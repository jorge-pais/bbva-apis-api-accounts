package uy.com.bbva.services.accounts.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;


/**
 * User\\&#39;s address associated to the account.
 **/



public class Address   {
  
  private String addressName = null;
  private String city = null;
  private String state = null;
  private Country country = null;
  private String zipCode = null;
  private String alias = null;

  /**
   * Complete address of the building.
   **/
  public Address addressName(String addressName) {
    this.addressName = addressName;
    return this;
  }

  

  @JsonProperty("addressName")
  public String getAddressName() {
    return addressName;
  }
  public void setAddressName(String addressName) {
    this.addressName = addressName;
  }

  /**
   * City where the building is placed.
   **/
  public Address city(String city) {
    this.city = city;
    return this;
  }

  

  @JsonProperty("city")
  public String getCity() {
    return city;
  }
  public void setCity(String city) {
    this.city = city;
  }

  /**
   * State, Province or Region where the building is placed.
   **/
  public Address state(String state) {
    this.state = state;
    return this;
  }

  

  @JsonProperty("state")
  public String getState() {
    return state;
  }
  public void setState(String state) {
    this.state = state;
  }

  /**
   * Country where the building is placed.
   **/
  public Address country(Country country) {
    this.country = country;
    return this;
  }

  

  @JsonProperty("country")
  public Country getCountry() {
    return country;
  }
  public void setCountry(Country country) {
    this.country = country;
  }

  /**
   * Address ZIP code.
   **/
  public Address zipCode(String zipCode) {
    this.zipCode = zipCode;
    return this;
  }

  

  @JsonProperty("zipCode")
  public String getZipCode() {
    return zipCode;
  }
  public void setZipCode(String zipCode) {
    this.zipCode = zipCode;
  }

  /**
   * Complete address of the building.
   **/
  public Address alias(String alias) {
    this.alias = alias;
    return this;
  }

  

  @JsonProperty("alias")
  public String getAlias() {
    return alias;
  }
  public void setAlias(String alias) {
    this.alias = alias;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Address address = (Address) o;
    return Objects.equals(addressName, address.addressName) &&
        Objects.equals(city, address.city) &&
        Objects.equals(state, address.state) &&
        Objects.equals(country, address.country) &&
        Objects.equals(zipCode, address.zipCode) &&
        Objects.equals(alias, address.alias);
  }

  @Override
  public int hashCode() {
    return Objects.hash(addressName, city, state, country, zipCode, alias);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Address {\n");
    
    sb.append("    addressName: ").append(toIndentedString(addressName)).append("\n");
    sb.append("    city: ").append(toIndentedString(city)).append("\n");
    sb.append("    state: ").append(toIndentedString(state)).append("\n");
    sb.append("    country: ").append(toIndentedString(country)).append("\n");
    sb.append("    zipCode: ").append(toIndentedString(zipCode)).append("\n");
    sb.append("    alias: ").append(toIndentedString(alias)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}