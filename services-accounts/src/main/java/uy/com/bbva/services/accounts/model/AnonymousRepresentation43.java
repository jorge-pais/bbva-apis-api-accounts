package uy.com.bbva.services.accounts.model;

import com.fasterxml.jackson.annotation.JsonProperty;


import java.util.ArrayList;
import java.util.List;
import java.util.Objects;



public class AnonymousRepresentation43   {
  
  private List<AnonymousRepresentation13> data = new ArrayList<AnonymousRepresentation13>();

  /**
   **/
  public AnonymousRepresentation43 data(List<AnonymousRepresentation13> data) {
    this.data = data;
    return this;
  }

  

  @JsonProperty("data")
  public List<AnonymousRepresentation13> getData() {
    return data;
  }
  public void setData(List<AnonymousRepresentation13> data) {
    this.data = data;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AnonymousRepresentation43 anonymousRepresentation43 = (AnonymousRepresentation43) o;
    return Objects.equals(data, anonymousRepresentation43.data);
  }

  @Override
  public int hashCode() {
    return Objects.hash(data);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AnonymousRepresentation43 {\n");
    
    sb.append("    data: ").append(toIndentedString(data)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}