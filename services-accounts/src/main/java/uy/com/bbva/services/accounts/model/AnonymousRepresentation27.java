package uy.com.bbva.services.accounts.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

public class AnonymousRepresentation27   {
  
  private Transaction data = null;

  /**
   **/
  public AnonymousRepresentation27 data(Transaction data) {
    this.data = data;
    return this;
  }

  

  @JsonProperty("data")
  public Transaction getData() {
    return data;
  }
  public void setData(Transaction data) {
    this.data = data;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AnonymousRepresentation27 anonymousRepresentation27 = (AnonymousRepresentation27) o;
    return Objects.equals(data, anonymousRepresentation27.data);
  }

  @Override
  public int hashCode() {
    return Objects.hash(data);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AnonymousRepresentation27 {\n");
    
    sb.append("    data: ").append(toIndentedString(data)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}