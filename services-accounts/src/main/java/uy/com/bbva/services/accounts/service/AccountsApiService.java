package uy.com.bbva.services.accounts.service;
import uy.com.bbva.services.accounts.response.AccountData;
import uy.com.bbva.services.accounts.response.AccountDataList;
import uy.com.bbva.services.accounts.response.TransactionDataList;
import uy.com.bbva.services.commons.exceptions.ServiceException;
import java.util.Date;
public abstract class AccountsApiService {
	public abstract AccountDataList getAccounts(String accountId, String numberTypeId, String number, String accountFamilyId,
			String accountTypeId, String jointId, String alias, String statusId, String currenciesCurrency, String orderBy,
			String order, String expand, Long pageSize, String paginationKey, String fields, String documentoHeader)
			throws ServiceException;
	public abstract AccountData getAccountsAccountId(String accountId, String expand, String fields) throws ServiceException;
	public abstract TransactionDataList getAccountsAccountIdTransactions(String accountId, Date operationDate,
			Date fromOperationDate, Date toOperationDate, String localAmountAmount, String fromLocalAmountAmount,
			String toLocalAmountAmount, String localAmountCurrency, String transactionTypeId,
			String transactionTypeInternalCodeId, String statusId, Long pageSize, String paginationKey, String orderBy,
			String order, String fields) throws ServiceException;
}