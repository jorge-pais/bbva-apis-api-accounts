package uy.com.bbva.services.accounts.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

public class AccountFormat {

    private String number = null;
    private AccountNumberType numberType = null;

    /**
     * Account number. This number consists in an alpha-numerical sequence to identify an account.
     **/
    public AccountFormat number(String number) {
        this.number = number;
        return this;
    }


    @JsonProperty("number")
    public String getNumber() {
        return number;
    }
    public void setNumber(String number) {
        this.number = number;
    }

    /**
     * Type of account number.
     **/
    public AccountFormat numberType(AccountNumberType numberType) {
        this.numberType = numberType;
        return this;
    }


    @JsonProperty("numberType")
    public AccountNumberType getNumberType() {
        return numberType;
    }
    public void setNumberType(AccountNumberType numberType) {
        this.numberType = numberType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AccountFormat accountFormat = (AccountFormat) o;
        return Objects.equals(number, accountFormat.number) &&
                Objects.equals(numberType, accountFormat.numberType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(number, numberType);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class AccountFormat {\n");

        sb.append("    number: ").append(toIndentedString(number)).append("\n");
        sb.append("    numberType: ").append(toIndentedString(numberType)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}