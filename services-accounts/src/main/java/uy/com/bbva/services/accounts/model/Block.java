package uy.com.bbva.services.accounts.model;

import com.fasterxml.jackson.annotation.JsonProperty;


import java.util.Date;
import java.util.Objects;



public class Block   {
  
  private String id = null;
  private BlockType blockType = null;
  private String description = null;
  private Date blockDate = null;
  private String status = null;

  /**
   * Block identifier associated to the account.
   **/
  public Block id(String id) {
    this.id = id;
    return this;
  }

  

  @JsonProperty("id")
  public String getId() {
    return id;
  }
  public void setId(String id) {
    this.id = id;
  }

  /**
   * Block type associated to the account.
   **/
  public Block blockType(BlockType blockType) {
    this.blockType = blockType;
    return this;
  }

  

  @JsonProperty("blockType")
  public BlockType getBlockType() {
    return blockType;
  }
  public void setBlockType(BlockType blockType) {
    this.blockType = blockType;
  }

  /**
   * Block description associated to the account.
   **/
  public Block description(String description) {
    this.description = description;
    return this;
  }

  

  @JsonProperty("description")
  public String getDescription() {
    return description;
  }
  public void setDescription(String description) {
    this.description = description;
  }

  /**
   * String based on ISO-8601 date format for specifying when the user requested to block the account.
   **/
  public Block blockDate(Date blockDate) {
    this.blockDate = blockDate;
    return this;
  }

  

  @JsonProperty("blockDate")
  public Date getBlockDate() {
    return blockDate;
  }
  public void setBlockDate(Date blockDate) {
    this.blockDate = blockDate;
  }

  /**
   * Block status.
   **/
  public Block status(String status) {
    this.status = status;
    return this;
  }

  

  @JsonProperty("status")
  public String getStatus() {
    return status;
  }
  public void setStatus(String status) {
    this.status = status;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Block block = (Block) o;
    return Objects.equals(id, block.id) &&
        Objects.equals(blockType, block.blockType) &&
        Objects.equals(description, block.description) &&
        Objects.equals(blockDate, block.blockDate) &&
        Objects.equals(status, block.status);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, blockType, description, blockDate, status);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Block {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    blockType: ").append(toIndentedString(blockType)).append("\n");
    sb.append("    description: ").append(toIndentedString(description)).append("\n");
    sb.append("    blockDate: ").append(toIndentedString(blockDate)).append("\n");
    sb.append("    status: ").append(toIndentedString(status)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
