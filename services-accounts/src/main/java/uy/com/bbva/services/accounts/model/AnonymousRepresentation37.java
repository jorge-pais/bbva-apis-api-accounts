package uy.com.bbva.services.accounts.model;

import com.fasterxml.jackson.annotation.JsonProperty;


import java.util.ArrayList;
import java.util.List;
import java.util.Objects;



public class AnonymousRepresentation37   {
  
  private List<AnonymousRepresentation11> data = new ArrayList<AnonymousRepresentation11>();

  /**
   **/
  public AnonymousRepresentation37 data(List<AnonymousRepresentation11> data) {
    this.data = data;
    return this;
  }

  

  @JsonProperty("data")
  public List<AnonymousRepresentation11> getData() {
    return data;
  }
  public void setData(List<AnonymousRepresentation11> data) {
    this.data = data;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AnonymousRepresentation37 anonymousRepresentation37 = (AnonymousRepresentation37) o;
    return Objects.equals(data, anonymousRepresentation37.data);
  }

  @Override
  public int hashCode() {
    return Objects.hash(data);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AnonymousRepresentation37 {\n");
    
    sb.append("    data: ").append(toIndentedString(data)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
