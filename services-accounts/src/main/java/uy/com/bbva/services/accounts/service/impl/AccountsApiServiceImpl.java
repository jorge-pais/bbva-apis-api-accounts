package uy.com.bbva.services.accounts.service.impl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uy.com.bbva.services.accounts.dao.DAO;
import uy.com.bbva.services.accounts.dao.TransactionDAO;
import uy.com.bbva.services.accounts.model.Account;
import uy.com.bbva.services.accounts.response.AccountData;
import uy.com.bbva.services.accounts.response.AccountDataList;
import uy.com.bbva.services.accounts.response.TransactionDataList;
import uy.com.bbva.services.accounts.service.AccountsApiService;
import uy.com.bbva.services.commons.exceptions.ServiceException;
import java.util.Date;
@Service
public class AccountsApiServiceImpl extends AccountsApiService {
	@Autowired
	private DAO dao;
	@Autowired
	private TransactionDAO transactionDAO;
	@Override
	public AccountDataList getAccounts(String accountId, String numberTypeId, String number, String accountFamilyId,
			String accountTypeId, String jointId, String alias, String statusId, String currenciesCurrency, String orderBy,
			String order, String expand, Long pageSize, String paginationKey, String fields, String documentoHeader)
			throws ServiceException {
		AccountDataList accounts = null;
		accounts = dao.accounts(documentoHeader, accountId);
		return accounts;
	}
	@Override
	public AccountData getAccountsAccountId(String accountId, String expand, String fields) throws ServiceException {
		Account account = dao.account(accountId);
		AccountData ad = new AccountData();
		ad.setData(account);
		return ad;
	}
	@Override
	public TransactionDataList getAccountsAccountIdTransactions(String accountId, Date operationDate, Date fromOperationDate,
			Date toOperationDate, String localAmountAmount, String fromLocalAmountAmount, String toLocalAmountAmount,
			String localAmountCurrency, String transactionTypeId, String transactionTypeInternalCodeId, String statusId,
			Long pageSize, String paginationKey, String orderBy, String order, String fields) throws ServiceException {
	    long pageIndex = StringUtils.isNotEmpty(paginationKey)? Long.parseLong(paginationKey):0;
		TransactionDataList tdl = transactionDAO.transactions(pageIndex, pageSize!= null? pageSize.longValue(): 0, accountId);
		return tdl;
	}
}