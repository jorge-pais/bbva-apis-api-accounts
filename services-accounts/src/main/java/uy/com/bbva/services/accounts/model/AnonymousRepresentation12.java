package uy.com.bbva.services.accounts.model;

import com.fasterxml.jackson.annotation.JsonProperty;


import java.util.Date;
import java.util.Objects;



public class AnonymousRepresentation12   {
  
  private String id = null;
  private HoldType holdType = null;
  private Boolean isActive = false;
  private Date registrationDate = null;
  private Date accountedDate = null;
  private Date endDate = null;
  private ModelImport originalAmount = null;
  private ModelImport currentAmount = null;
  private String comments = null;
  private Double checkNumber = null;

  /**
   * Unique hold identifier.
   **/
  public AnonymousRepresentation12 id(String id) {
    this.id = id;
    return this;
  }

  @JsonProperty("id")
  public String getId() {
    return id;
  }
  public void setId(String id) {
    this.id = id;
  }

  /**
   * Unique identifier of the hold type.
   **/
  public AnonymousRepresentation12 holdType(HoldType holdType) {
    this.holdType = holdType;
    return this;
  }

  

  @JsonProperty("holdType")
  public HoldType getHoldType() {
    return holdType;
  }
  public void setHoldType(HoldType holdType) {
    this.holdType = holdType;
  }

  /**
   * Flag indicating whether this hold is or not active.
   **/
  public AnonymousRepresentation12 isActive(Boolean isActive) {
    this.isActive = isActive;
    return this;
  }

  

  @JsonProperty("isActive")
  public Boolean getIsActive() {
    return isActive;
  }
  public void setIsActive(Boolean isActive) {
    this.isActive = isActive;
  }

  /**
   * Creation date of the hold.
   **/
  public AnonymousRepresentation12 registrationDate(Date registrationDate) {
    this.registrationDate = registrationDate;
    return this;
  }

  

  @JsonProperty("registrationDate")
  public Date getRegistrationDate() {
    return registrationDate;
  }
  public void setRegistrationDate(Date registrationDate) {
    this.registrationDate = registrationDate;
  }

  /**
   * The accounted date of a withholding is recorded when the external entity is checked against the bank and everything is correct. The hold changes state and is converted to a move of the account. String based on ISO-8601 date format.
   **/
  public AnonymousRepresentation12 accountedDate(Date accountedDate) {
    this.accountedDate = accountedDate;
    return this;
  }

  

  @JsonProperty("accountedDate")
  public Date getAccountedDate() {
    return accountedDate;
  }
  public void setAccountedDate(Date accountedDate) {
    this.accountedDate = accountedDate;
  }

  /**
   * End date of the hold.
   **/
  public AnonymousRepresentation12 endDate(Date endDate) {
    this.endDate = endDate;
    return this;
  }

  

  @JsonProperty("endDate")
  public Date getEndDate() {
    return endDate;
  }
  public void setEndDate(Date endDate) {
    this.endDate = endDate;
  }

  /**
   * Hold’s original amount.
   **/
  public AnonymousRepresentation12 originalAmount(ModelImport originalAmount) {
    this.originalAmount = originalAmount;
    return this;
  }

  

  @JsonProperty("originalAmount")
  public ModelImport getOriginalAmount() {
    return originalAmount;
  }
  public void setOriginalAmount(ModelImport originalAmount) {
    this.originalAmount = originalAmount;
  }

  /**
   * Hold’s remaining balance after an update.
   **/
  public AnonymousRepresentation12 currentAmount(ModelImport currentAmount) {
    this.currentAmount = currentAmount;
    return this;
  }

  

  @JsonProperty("currentAmount")
  public ModelImport getCurrentAmount() {
    return currentAmount;
  }
  public void setCurrentAmount(ModelImport currentAmount) {
    this.currentAmount = currentAmount;
  }

  /**
   * Comments about the hold.
   **/
  public AnonymousRepresentation12 comments(String comments) {
    this.comments = comments;
    return this;
  }

  

  @JsonProperty("comments")
  public String getComments() {
    return comments;
  }
  public void setComments(String comments) {
    this.comments = comments;
  }

  /**
   * Check number if the hold originated using a check.
   **/
  public AnonymousRepresentation12 checkNumber(Double checkNumber) {
    this.checkNumber = checkNumber;
    return this;
  }

  

  @JsonProperty("checkNumber")
  public Double getCheckNumber() {
    return checkNumber;
  }
  public void setCheckNumber(Double checkNumber) {
    this.checkNumber = checkNumber;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AnonymousRepresentation12 anonymousRepresentation12 = (AnonymousRepresentation12) o;
    return Objects.equals(id, anonymousRepresentation12.id) &&
        Objects.equals(holdType, anonymousRepresentation12.holdType) &&
        Objects.equals(isActive, anonymousRepresentation12.isActive) &&
        Objects.equals(registrationDate, anonymousRepresentation12.registrationDate) &&
        Objects.equals(accountedDate, anonymousRepresentation12.accountedDate) &&
        Objects.equals(endDate, anonymousRepresentation12.endDate) &&
        Objects.equals(originalAmount, anonymousRepresentation12.originalAmount) &&
        Objects.equals(currentAmount, anonymousRepresentation12.currentAmount) &&
        Objects.equals(comments, anonymousRepresentation12.comments) &&
        Objects.equals(checkNumber, anonymousRepresentation12.checkNumber);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, holdType, isActive, registrationDate, accountedDate, endDate, originalAmount, currentAmount, comments, checkNumber);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AnonymousRepresentation12 {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    holdType: ").append(toIndentedString(holdType)).append("\n");
    sb.append("    isActive: ").append(toIndentedString(isActive)).append("\n");
    sb.append("    registrationDate: ").append(toIndentedString(registrationDate)).append("\n");
    sb.append("    accountedDate: ").append(toIndentedString(accountedDate)).append("\n");
    sb.append("    endDate: ").append(toIndentedString(endDate)).append("\n");
    sb.append("    originalAmount: ").append(toIndentedString(originalAmount)).append("\n");
    sb.append("    currentAmount: ").append(toIndentedString(currentAmount)).append("\n");
    sb.append("    comments: ").append(toIndentedString(comments)).append("\n");
    sb.append("    checkNumber: ").append(toIndentedString(checkNumber)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}