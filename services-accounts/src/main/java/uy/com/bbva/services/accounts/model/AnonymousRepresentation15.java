package uy.com.bbva.services.accounts.model;

import com.fasterxml.jackson.annotation.JsonProperty;


import java.util.ArrayList;
import java.util.List;
import java.util.Objects;



public class AnonymousRepresentation15   {
  
  private AnonymousRepresentation15Groupings groupings = null;
  private List<AnonymousRepresentation15Breakdown> breakdown = new ArrayList<AnonymousRepresentation15Breakdown>();

  /**
   * Discriminant classification in which the balances will be segregated.
   **/
  public AnonymousRepresentation15 groupings(AnonymousRepresentation15Groupings groupings) {
    this.groupings = groupings;
    return this;
  }

  

  @JsonProperty("groupings")
  public AnonymousRepresentation15Groupings getGroupings() {
    return groupings;
  }
  public void setGroupings(AnonymousRepresentation15Groupings groupings) {
    this.groupings = groupings;
  }

  /**
   * Set of applicable balances, grouped by currency, that are present in the accounts that match the criteria defined by the grouping items. DISCLAIMER: At least one of balances must be provided.
   **/
  public AnonymousRepresentation15 breakdown(List<AnonymousRepresentation15Breakdown> breakdown) {
    this.breakdown = breakdown;
    return this;
  }

  

  @JsonProperty("breakdown")
  public List<AnonymousRepresentation15Breakdown> getBreakdown() {
    return breakdown;
  }
  public void setBreakdown(List<AnonymousRepresentation15Breakdown> breakdown) {
    this.breakdown = breakdown;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AnonymousRepresentation15 anonymousRepresentation15 = (AnonymousRepresentation15) o;
    return Objects.equals(groupings, anonymousRepresentation15.groupings) &&
        Objects.equals(breakdown, anonymousRepresentation15.breakdown);
  }

  @Override
  public int hashCode() {
    return Objects.hash(groupings, breakdown);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AnonymousRepresentation15 {\n");
    
    sb.append("    groupings: ").append(toIndentedString(groupings)).append("\n");
    sb.append("    breakdown: ").append(toIndentedString(breakdown)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
