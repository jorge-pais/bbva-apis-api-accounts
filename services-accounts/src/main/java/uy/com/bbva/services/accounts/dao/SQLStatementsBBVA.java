package uy.com.bbva.services.accounts.dao;
public class SQLStatementsBBVA {
    static String GET_CUENTAVISTA = "select * " +
            "from fsd011 " +
            "where Pgcod = 1 and" +
            " Scmod = ? and " +
            " Sctope = 0 and " +
            " Scmda = ? and " +
            " Scpap = ? and " +
            " Sccta = ? and " +
            " Scoper = 0 and " +
            " Scsbop = ? and " +
            " Scsuc = ? " +
            "order by Pgcod, Scmod, Sctope, Scmda, Scpap, Sccta, Scoper, Scsbop";
    static String GET_PRODUCTO = "select * " + "from fst003 " + "where modulo = ? ";
    static String GET_SALDO_ANTERIOR = "select * " + "from fsh014 " + "where pgcod = 1 and " + "Hasuc = ? and " +
            "Harub = ? and " + "Hamda = ? and " + "Hapap = ? and " + "Hacta = ? and " + "Haoper = ? and " +
            "Hasbop = ? and " + "Hatope = ? and " + "Haanio = ? ";
    static String GET_CUENTAS_BANTOTAL_ASOCIADAS_AL_DOCUMENTO = "select * " +
            "from fsr008 " +
            "where pgcod = 1 and " +
            "pepais = ? and " +
            "petdoc = ? and " +
            "pendoc = ? and " +
            "TTCOD = 1";
    static String GET_CUENTAS_BANTOTAL_ASOCIADAS_AL_DOCUMENTO_COUNT = "count (*) " +
            "from fsr008 " +
            "where pgcod = 1 and " +
            "pepais = ? and " +
            "petdoc = ? and " +
            "pendoc = ? and " +
            "TTCOD = 1";
    static String GET_CUENTAS_BANTOTAL_ASOCIADAS_AL_DOCUMENTO_PAGINATION = "select * " +
            "from fsr008 " +
            "where pgcod = 1 and " +
            "pepais = ? and " +
            "petdoc = ? and " +
            "pendoc = ? and " +
            "TTCOD = 1 and " +
            "ROWNUM BETWEEN ? AND ?";
    static String GET_LIST_CUENTASVISTAS_POR_CUENTA_BANTOTAL = "select * " +
            "from fsd011 " +
            "where pgcod = 1 and " +
            "sccta = ? and " +
            "scmod in (20, 21, 23) and " +
            "scstat <> 1 and scstat <> 99 " +
            "order by Pgcod, Sccta, Scmod, Scmda, Scpap ";
    static String GET_PERSONA = "select * " +
            "from fsd001 " +
            "where pendoc = ? " +
            "      and petdoc < 90 " +
            "order by pendoc ";
    static String GET_MOVIMIENTOS_DIARIOS = "select * from  " +
            "fsd016 " +
            "where pgcod = ? " +
            "and Itsucd = ? " +
            "and Rubro = ? " +
            "and Moneda = ? " +
            "and Papel = ? " +
            "and CTNRO = ? " +
            "and Itoper = ? " +
            "and Itsubo = ? " +
            "and Ittope = ? " +
            "order by Pgcod, Itsucd, Rubro, Moneda, Papel, CTNRO, Itoper, Itsubo, Ittope";
    static String GET_MOVIMIENTOS_DIARIOS_COUNT = "select count(*) from  " +
            "fsd016 " +
            "where pgcod = ? " +
            "and Itsucd = ? " +
            "and Rubro = ? " +
            "and Moneda = ? " +
            "and Papel = ? " +
            "and CTNRO = ? " +
            "and Itoper = ? " +
            "and Itsubo = ? " +
            "and Ittope = ? ";
    static String GET_MOVIMIENTOS_DIARIOS_PAGINATION = "select * from  " +
            "fsd016 " +
            "where pgcod = ? " +
            "and Itsucd = ? " +
            "and Rubro = ? " +
            "and Moneda = ? " +
            "and Papel = ? " +
            "and CTNRO = ? " +
            "and Itoper = ? " +
            "and Itsubo = ? " +
            "and Ittope = ? " +
            "order by Pgcod, Itsucd, Rubro, Moneda, Papel, CTNRO, Itoper, Itsubo, Ittope " +
            "LIMIT ? OFFSET ?";
    static String GET_MOVIMIENTOS_HISTORICOS = "select * " +
            "from  fsh016 " +
            "where pgcod = ? " +
            "and hfcon >= ? " +
            "and hfcon <= ? " +
            "and Hsucur = ? " +
            "and Hrubro = ? " +
            "and Hmda = ? " +
            "and Hpap = ? " +
            "and Hcta = ? " +
            "and Hoper = ? " +
            "and Hsubop = ? " +
            "order by Pgcod, Hfcon, Hsucur, Hrubro, Hmda, Hpap, Hcta, Hoper, Hsubop ";
    static String GET_MOVIMIENTOS_HISTORICOS_COUNT = "select count(*) " +
            "from  fsh016 " +
            "where pgcod = ? " +
            "and hfcon >= ? " +
            "and hfcon <= ? " +
            "and Hsucur = ? " +
            "and Hrubro = ? " +
            "and Hmda = ? " +
            "and Hpap = ? " +
            "and Hcta = ? " +
            "and Hoper = ? " +
            "and Hsubop = ?";
    static String GET_MOVIMIENTOS_HISTORICOS_PAGINATION = "select * " +
            "from  fsh016 " +
            "where pgcod = ? " +
            "and hfcon >= ? " +
            "and hfcon <= ? " +
            "and Hsucur = ? " +
            "and Hrubro = ? " +
            "and Hmda = ? " +
            "and Hpap = ? " +
            "and Hcta = ? " +
            "and Hoper = ? " +
            "and Hsubop = ? " +
            "order by Pgcod, Hfcon, Hsucur, Hrubro, Hmda, Hpap, Hcta, Hoper, Hsubop " +
            "LIMIT ? OFFSET ?";
    static String GET_CABEZAL_MOVIMIENTO_DIARIO = "select * " +
            "from fsd015 " +
            "where pgcod = ? " +
            "and itsuc = ? " +
            "and itmod = ? " +
            "and ittran = ? " +
            "and itnrel = ?";
    static String GET_CABEZAL_MOVIMIENTO_HISTORICO = "select * " +
            "from fsH015 " +
            "where pgcod = ? " +
            "and Hcmod = ? " +
            "and HSucor = ? " +
            "and Htran = ? " +
            "and Hnrel = ? " +
            "and HFcon = ? ";
    static String GET_NOMBRE_TRANSACCION = "select trnom " +
            "from fst034 " +
            "where Pgcod = ? " +
            "and trmod = ? " +
            "and trnro = ? ";
}