package uy.com.bbva.services.accounts.model;

import com.fasterxml.jackson.annotation.JsonProperty;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;



public class AnonymousRepresentation4   {
  
  private String id = null;
  private String name = null;
  private Period period = null;
  private List<AnonymousRepresentation4MonetaryPayments> monetaryPayments = new ArrayList<AnonymousRepresentation4MonetaryPayments>();
  private Double percentage = null;
  private Date endDate = null;
  private List<String> paymentAmounts = new ArrayList<String>();

  /**
   * Payment method type identifier.
   **/
  public AnonymousRepresentation4 id(String id) {
    this.id = id;
    return this;
  }

  

  @JsonProperty("id")
  public String getId() {
    return id;
  }
  public void setId(String id) {
    this.id = id;
  }

  /**
   * Payment method type name.
   **/
  public AnonymousRepresentation4 name(String name) {
    this.name = name;
    return this;
  }

  

  @JsonProperty("name")
  public String getName() {
    return name;
  }
  public void setName(String name) {
    this.name = name;
  }

  /**
   * Payment method period selected by the user for paying credit debt.
   **/
  public AnonymousRepresentation4 period(Period period) {
    this.period = period;
    return this;
  }

  

  @JsonProperty("period")
  public Period getPeriod() {
    return period;
  }
  public void setPeriod(Period period) {
    this.period = period;
  }

  /**
   * Monetary amount for the selected payment method. This attribute will not be provided if either percentage or free payment methods are selected. This amount may be provided in several currencies (depending on the country).
   **/
  public AnonymousRepresentation4 monetaryPayments(List<AnonymousRepresentation4MonetaryPayments> monetaryPayments) {
    this.monetaryPayments = monetaryPayments;
    return this;
  }

  

  @JsonProperty("monetaryPayments")
  public List<AnonymousRepresentation4MonetaryPayments> getMonetaryPayments() {
    return monetaryPayments;
  }
  public void setMonetaryPayments(List<AnonymousRepresentation4MonetaryPayments> monetaryPayments) {
    this.monetaryPayments = monetaryPayments;
  }

  /**
   * Percentage value used within the selected payment method. This attribute is mandatory for percentage payment method, and will not be provided if monetary payment method is selected.
   **/
  public AnonymousRepresentation4 percentage(Double percentage) {
    this.percentage = percentage;
    return this;
  }

  

  @JsonProperty("percentage")
  public Double getPercentage() {
    return percentage;
  }
  public void setPercentage(Double percentage) {
    this.percentage = percentage;
  }

  /**
   * String based on ISO-8601 date format for specifying the last date on which the current payment term must be made before being considered in arrears.
   **/
  public AnonymousRepresentation4 endDate(Date endDate) {
    this.endDate = endDate;
    return this;
  }

  

  @JsonProperty("endDate")
  public Date getEndDate() {
    return endDate;
  }
  public void setEndDate(Date endDate) {
    this.endDate = endDate;
  }

  /**
   * List of precalculated amounts for paying credit debt. These amounts are calculated based on the existing total debt amount at the moment the last period ended.
   **/
  public AnonymousRepresentation4 paymentAmounts(List<String> paymentAmounts) {
    this.paymentAmounts = paymentAmounts;
    return this;
  }

  

  @JsonProperty("paymentAmounts")
  public List<String> getPaymentAmounts() {
    return paymentAmounts;
  }
  public void setPaymentAmounts(List<String> paymentAmounts) {
    this.paymentAmounts = paymentAmounts;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AnonymousRepresentation4 anonymousRepresentation4 = (AnonymousRepresentation4) o;
    return Objects.equals(id, anonymousRepresentation4.id) &&
        Objects.equals(name, anonymousRepresentation4.name) &&
        Objects.equals(period, anonymousRepresentation4.period) &&
        Objects.equals(monetaryPayments, anonymousRepresentation4.monetaryPayments) &&
        Objects.equals(percentage, anonymousRepresentation4.percentage) &&
        Objects.equals(endDate, anonymousRepresentation4.endDate) &&
        Objects.equals(paymentAmounts, anonymousRepresentation4.paymentAmounts);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, name, period, monetaryPayments, percentage, endDate, paymentAmounts);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AnonymousRepresentation4 {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    period: ").append(toIndentedString(period)).append("\n");
    sb.append("    monetaryPayments: ").append(toIndentedString(monetaryPayments)).append("\n");
    sb.append("    percentage: ").append(toIndentedString(percentage)).append("\n");
    sb.append("    endDate: ").append(toIndentedString(endDate)).append("\n");
    sb.append("    paymentAmounts: ").append(toIndentedString(paymentAmounts)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
