package uy.com.bbva.services.accounts.response;
import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonProperty;
import uy.com.bbva.services.accounts.model.Pagination;
import uy.com.bbva.services.accounts.model.Transaction;
public class TransactionDataList {
	@JsonProperty("data")
	private List<Transaction> data = new ArrayList<>();
	@JsonProperty("pagination")
	private Pagination pagination = null;
	public List<Transaction> getData() {
		return data;
	}
	public void setData(List<Transaction> data) {
		this.data = data;
	}
	public Pagination getPagination() {
		return pagination;
	}
	public void setPagination(Pagination pagination) {
		this.pagination = pagination;
	}
}