package uy.com.bbva.services.accounts.model;

import com.fasterxml.jackson.annotation.JsonProperty;


import java.util.Objects;



public class Pagination   {
  
  private PaginationLinks links = null;
  private Double page = null;
  private Double totalPages = null;
  private Double totalElements = null;
  private Double pageSize = null;

  /**
   * Pagination links. These links provide relative URIs to reach different useful pages, such as first, last, next or previous pages.
   **/
  public Pagination links(PaginationLinks links) {
    this.links = links;
    return this;
  }

  

  @JsonProperty("links")
  public PaginationLinks getLinks() {
    return links;
  }
  public void setLinks(PaginationLinks links) {
    this.links = links;
  }

  /**
   * Current page number. This attribue value is 0 when referring to the first page.
   **/
  public Pagination page(Double page) {
    this.page = page;
    return this;
  }

  

  @JsonProperty("page")
  public Double getPage() {
    return page;
  }
  public void setPage(Double page) {
    this.page = page;
  }

  /**
   * Total number of pages for the provided pagination and filtering parameters.
   **/
  public Pagination totalPages(Double totalPages) {
    this.totalPages = totalPages;
    return this;
  }

  

  @JsonProperty("totalPages")
  public Double getTotalPages() {
    return totalPages;
  }
  public void setTotalPages(Double totalPages) {
    this.totalPages = totalPages;
  }

  /**
   * Total number of items to the requested list, taking into account possible filtering criteria if specified.
   **/
  public Pagination totalElements(Double totalElements) {
    this.totalElements = totalElements;
    return this;
  }

  

  @JsonProperty("totalElements")
  public Double getTotalElements() {
    return totalElements;
  }
  public void setTotalElements(Double totalElements) {
    this.totalElements = totalElements;
  }

  /**
   * Number of items per page. This attribute value matches pageSize query parameter if provided. Otherwise, this attribute may provide the default page size.
   **/
  public Pagination pageSize(Double pageSize) {
    this.pageSize = pageSize;
    return this;
  }

  

  @JsonProperty("pageSize")
  public Double getPageSize() {
    return pageSize;
  }
  public void setPageSize(Double pageSize) {
    this.pageSize = pageSize;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Pagination pagination = (Pagination) o;
    return Objects.equals(links, pagination.links) &&
        Objects.equals(page, pagination.page) &&
        Objects.equals(totalPages, pagination.totalPages) &&
        Objects.equals(totalElements, pagination.totalElements) &&
        Objects.equals(pageSize, pagination.pageSize);
  }

  @Override
  public int hashCode() {
    return Objects.hash(links, page, totalPages, totalElements, pageSize);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Pagination {\n");
    
    sb.append("    links: ").append(toIndentedString(links)).append("\n");
    sb.append("    page: ").append(toIndentedString(page)).append("\n");
    sb.append("    totalPages: ").append(toIndentedString(totalPages)).append("\n");
    sb.append("    totalElements: ").append(toIndentedString(totalElements)).append("\n");
    sb.append("    pageSize: ").append(toIndentedString(pageSize)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
