package uy.com.bbva.services.accounts.model;

import com.fasterxml.jackson.annotation.JsonProperty;


import java.util.Objects;



public class AnonymousRepresentation5Credits   {
  
  private Double totalTransactions = null;
  private AnonymousRepresentation5CreditsBalance balance = null;

  /**
   * Total number of credits transactions for the current day.
   **/
  public AnonymousRepresentation5Credits totalTransactions(Double totalTransactions) {
    this.totalTransactions = totalTransactions;
    return this;
  }

  

  @JsonProperty("totalTransactions")
  public Double getTotalTransactions() {
    return totalTransactions;
  }
  public void setTotalTransactions(Double totalTransactions) {
    this.totalTransactions = totalTransactions;
  }

  /**
   * Total monetary amount of the credit transactions posted along the current day.
   **/
  public AnonymousRepresentation5Credits balance(AnonymousRepresentation5CreditsBalance balance) {
    this.balance = balance;
    return this;
  }

  

  @JsonProperty("balance")
  public AnonymousRepresentation5CreditsBalance getBalance() {
    return balance;
  }
  public void setBalance(AnonymousRepresentation5CreditsBalance balance) {
    this.balance = balance;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AnonymousRepresentation5Credits anonymousRepresentation5Credits = (AnonymousRepresentation5Credits) o;
    return Objects.equals(totalTransactions, anonymousRepresentation5Credits.totalTransactions) &&
        Objects.equals(balance, anonymousRepresentation5Credits.balance);
  }

  @Override
  public int hashCode() {
    return Objects.hash(totalTransactions, balance);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AnonymousRepresentation5Credits {\n");
    
    sb.append("    totalTransactions: ").append(toIndentedString(totalTransactions)).append("\n");
    sb.append("    balance: ").append(toIndentedString(balance)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}