package uy.com.bbva.services.accounts.model;

import com.fasterxml.jackson.annotation.JsonProperty;


import java.util.Date;
import java.util.Objects;



public class AnonymousRepresentation6   {
  
  private String id = null;
  private BlockType blockType = null;
  private String description = null;
  private String status = null;
  private Date blockDate = null;
  private Date cancellationDate = null;

  /**
   * Block identifier associated to the account.
   **/
  public AnonymousRepresentation6 id(String id) {
    this.id = id;
    return this;
  }

  

  @JsonProperty("id")
  public String getId() {
    return id;
  }
  public void setId(String id) {
    this.id = id;
  }

  /**
   * Block type associated to the account.
   **/
  public AnonymousRepresentation6 blockType(BlockType blockType) {
    this.blockType = blockType;
    return this;
  }

  

  @JsonProperty("blockType")
  public BlockType getBlockType() {
    return blockType;
  }
  public void setBlockType(BlockType blockType) {
    this.blockType = blockType;
  }

  /**
   * Block description associated to the account.
   **/
  public AnonymousRepresentation6 description(String description) {
    this.description = description;
    return this;
  }

  

  @JsonProperty("description")
  public String getDescription() {
    return description;
  }
  public void setDescription(String description) {
    this.description = description;
  }

  /**
   * Block status.
   **/
  public AnonymousRepresentation6 status(String status) {
    this.status = status;
    return this;
  }

  

  @JsonProperty("status")
  public String getStatus() {
    return status;
  }
  public void setStatus(String status) {
    this.status = status;
  }

  /**
   * String based on ISO-8601 date format for specifying when the user requested to block the account.
   **/
  public AnonymousRepresentation6 blockDate(Date blockDate) {
    this.blockDate = blockDate;
    return this;
  }

  

  @JsonProperty("blockDate")
  public Date getBlockDate() {
    return blockDate;
  }
  public void setBlockDate(Date blockDate) {
    this.blockDate = blockDate;
  }

  /**
   * String based on ISO-8601 date format for specifying when the user requested to cancell the block on the account.
   **/
  public AnonymousRepresentation6 cancellationDate(Date cancellationDate) {
    this.cancellationDate = cancellationDate;
    return this;
  }

  

  @JsonProperty("cancellationDate")
  public Date getCancellationDate() {
    return cancellationDate;
  }
  public void setCancellationDate(Date cancellationDate) {
    this.cancellationDate = cancellationDate;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AnonymousRepresentation6 anonymousRepresentation6 = (AnonymousRepresentation6) o;
    return Objects.equals(id, anonymousRepresentation6.id) &&
        Objects.equals(blockType, anonymousRepresentation6.blockType) &&
        Objects.equals(description, anonymousRepresentation6.description) &&
        Objects.equals(status, anonymousRepresentation6.status) &&
        Objects.equals(blockDate, anonymousRepresentation6.blockDate) &&
        Objects.equals(cancellationDate, anonymousRepresentation6.cancellationDate);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, blockType, description, status, blockDate, cancellationDate);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AnonymousRepresentation6 {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    blockType: ").append(toIndentedString(blockType)).append("\n");
    sb.append("    description: ").append(toIndentedString(description)).append("\n");
    sb.append("    status: ").append(toIndentedString(status)).append("\n");
    sb.append("    blockDate: ").append(toIndentedString(blockDate)).append("\n");
    sb.append("    cancellationDate: ").append(toIndentedString(cancellationDate)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
