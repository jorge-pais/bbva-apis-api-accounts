package uy.com.bbva.services.accounts.model;

public class SaldoBantotal {

    private int pgcod = 1;
    private int sucursal;
    private int moneda;
    private int cuenta;
    private int papel;
    private int operacion;
    private int tipoOperacion;
    private int subOperacion;
    private int modulo;
    private long rubro;
    private int estado;
    private double saldo;
    private int scfunc;


    public int getPgcod() {
        return pgcod;
    }

    public void setPgcod(int pgcod) {
        this.pgcod = pgcod;
    }

    public int getSucursal() {
        return sucursal;
    }

    public void setSucursal(int sucursal) {
        this.sucursal = sucursal;
    }

    public int getMoneda() {
        return moneda;
    }

    public void setMoneda(int moneda) {
        this.moneda = moneda;
    }

    public int getCuenta() {
        return cuenta;
    }

    public void setCuenta(int cuenta) {
        this.cuenta = cuenta;
    }

    public int getPapel() {
        return papel;
    }

    public void setPapel(int papel) {
        this.papel = papel;
    }

    public int getTipoOperacion() {
        return tipoOperacion;
    }

    public void setTipoOperacion(int tipoOperacion) {
        this.tipoOperacion = tipoOperacion;
    }

    public int getSubOperacion() {
        return subOperacion;
    }

    public void setSubOperacion(int subOperacion) {
        this.subOperacion = subOperacion;
    }

    public int getModulo() {
        return modulo;
    }

    public void setModulo(int modulo) {
        this.modulo = modulo;
    }

    public long getRubro() {
        return rubro;
    }

    public void setRubro(long rubro) {
        this.rubro = rubro;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public int getOperacion() {
        return operacion;
    }

    public void setOperacion(int operacion) {
        this.operacion = operacion;
    }

    public int getScfunc() {
        return scfunc;
    }

    public void setScfunc(int scfunc) {
        this.scfunc = scfunc;
    }
}