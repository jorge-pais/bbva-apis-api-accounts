package uy.com.bbva.services.accounts.model;

import com.fasterxml.jackson.annotation.JsonProperty;


import java.util.Date;
import java.util.Objects;



public class Hold   {
  
  private String id = null;
  private HoldType holdType = null;
  private Boolean isActive = false;
  private Date registrationDate = null;
  private Date accountedDate = null;
  private ModelImport originalAmount = null;
  private ModelImport currentAmount = null;
  private String comments = null;
  private Double checkNumber = null;
  private String daysActive = null;
  private Date endDate = null;
  private Boolean authorization = false;

  /**
   * Unique hold identifier.
   **/
  public Hold id(String id) {
    this.id = id;
    return this;
  }

  

  @JsonProperty("id")
  public String getId() {
    return id;
  }
  public void setId(String id) {
    this.id = id;
  }

  /**
   * Unique identifier of the hold type.
   **/
  public Hold holdType(HoldType holdType) {
    this.holdType = holdType;
    return this;
  }

  

  @JsonProperty("holdType")
  public HoldType getHoldType() {
    return holdType;
  }
  public void setHoldType(HoldType holdType) {
    this.holdType = holdType;
  }

  /**
   * Flag indicating whether this hold is or not active.
   **/
  public Hold isActive(Boolean isActive) {
    this.isActive = isActive;
    return this;
  }

  

  @JsonProperty("isActive")
  public Boolean getIsActive() {
    return isActive;
  }
  public void setIsActive(Boolean isActive) {
    this.isActive = isActive;
  }

  /**
   * Creation date of the hold.
   **/
  public Hold registrationDate(Date registrationDate) {
    this.registrationDate = registrationDate;
    return this;
  }

  

  @JsonProperty("registrationDate")
  public Date getRegistrationDate() {
    return registrationDate;
  }
  public void setRegistrationDate(Date registrationDate) {
    this.registrationDate = registrationDate;
  }

  /**
   * The accounted date of a withholding is recorded when the external entity is checked against the bank and everything is correct. The hold changes state and is converted to a move of the account. String based on ISO-8601 date format.
   **/
  public Hold accountedDate(Date accountedDate) {
    this.accountedDate = accountedDate;
    return this;
  }

  

  @JsonProperty("accountedDate")
  public Date getAccountedDate() {
    return accountedDate;
  }
  public void setAccountedDate(Date accountedDate) {
    this.accountedDate = accountedDate;
  }

  /**
   * Hold’s original amount.
   **/
  public Hold originalAmount(ModelImport originalAmount) {
    this.originalAmount = originalAmount;
    return this;
  }

  

  @JsonProperty("originalAmount")
  public ModelImport getOriginalAmount() {
    return originalAmount;
  }
  public void setOriginalAmount(ModelImport originalAmount) {
    this.originalAmount = originalAmount;
  }

  /**
   * Hold’s remaining balance after an update.
   **/
  public Hold currentAmount(ModelImport currentAmount) {
    this.currentAmount = currentAmount;
    return this;
  }

  

  @JsonProperty("currentAmount")
  public ModelImport getCurrentAmount() {
    return currentAmount;
  }
  public void setCurrentAmount(ModelImport currentAmount) {
    this.currentAmount = currentAmount;
  }

  /**
   * Comments about the hold.
   **/
  public Hold comments(String comments) {
    this.comments = comments;
    return this;
  }

  

  @JsonProperty("comments")
  public String getComments() {
    return comments;
  }
  public void setComments(String comments) {
    this.comments = comments;
  }

  /**
   * Check number if the hold originated using a check.
   **/
  public Hold checkNumber(Double checkNumber) {
    this.checkNumber = checkNumber;
    return this;
  }

  

  @JsonProperty("checkNumber")
  public Double getCheckNumber() {
    return checkNumber;
  }
  public void setCheckNumber(Double checkNumber) {
    this.checkNumber = checkNumber;
  }

  /**
   * Number of business days the hold will remain active.
   **/
  public Hold daysActive(String daysActive) {
    this.daysActive = daysActive;
    return this;
  }

  

  @JsonProperty("daysActive")
  public String getDaysActive() {
    return daysActive;
  }
  public void setDaysActive(String daysActive) {
    this.daysActive = daysActive;
  }

  /**
   * End date of the hold.
   **/
  public Hold endDate(Date endDate) {
    this.endDate = endDate;
    return this;
  }

  

  @JsonProperty("endDate")
  public Date getEndDate() {
    return endDate;
  }
  public void setEndDate(Date endDate) {
    this.endDate = endDate;
  }

  /**
   * Decides if the customer needs enough balance in the account to perform the hold. Values: true (authorization to create a hold despite the balance of the account), false (holds can only be placed for amounts equal or less than the account’s available balance. Default: false.
   **/
  public Hold authorization(Boolean authorization) {
    this.authorization = authorization;
    return this;
  }

  

  @JsonProperty("authorization")
  public Boolean getAuthorization() {
    return authorization;
  }
  public void setAuthorization(Boolean authorization) {
    this.authorization = authorization;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Hold hold = (Hold) o;
    return Objects.equals(id, hold.id) &&
        Objects.equals(holdType, hold.holdType) &&
        Objects.equals(isActive, hold.isActive) &&
        Objects.equals(registrationDate, hold.registrationDate) &&
        Objects.equals(accountedDate, hold.accountedDate) &&
        Objects.equals(originalAmount, hold.originalAmount) &&
        Objects.equals(currentAmount, hold.currentAmount) &&
        Objects.equals(comments, hold.comments) &&
        Objects.equals(checkNumber, hold.checkNumber) &&
        Objects.equals(daysActive, hold.daysActive) &&
        Objects.equals(endDate, hold.endDate) &&
        Objects.equals(authorization, hold.authorization);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, holdType, isActive, registrationDate, accountedDate, originalAmount, currentAmount, comments, checkNumber, daysActive, endDate, authorization);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Hold {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    holdType: ").append(toIndentedString(holdType)).append("\n");
    sb.append("    isActive: ").append(toIndentedString(isActive)).append("\n");
    sb.append("    registrationDate: ").append(toIndentedString(registrationDate)).append("\n");
    sb.append("    accountedDate: ").append(toIndentedString(accountedDate)).append("\n");
    sb.append("    originalAmount: ").append(toIndentedString(originalAmount)).append("\n");
    sb.append("    currentAmount: ").append(toIndentedString(currentAmount)).append("\n");
    sb.append("    comments: ").append(toIndentedString(comments)).append("\n");
    sb.append("    checkNumber: ").append(toIndentedString(checkNumber)).append("\n");
    sb.append("    daysActive: ").append(toIndentedString(daysActive)).append("\n");
    sb.append("    endDate: ").append(toIndentedString(endDate)).append("\n");
    sb.append("    authorization: ").append(toIndentedString(authorization)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}