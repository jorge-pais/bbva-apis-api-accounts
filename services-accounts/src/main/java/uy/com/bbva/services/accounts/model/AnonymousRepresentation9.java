package uy.com.bbva.services.accounts.model;

import com.fasterxml.jackson.annotation.JsonProperty;


import java.util.Objects;



public class AnonymousRepresentation9   {
  
  private String indicatorId = null;
  private String name = null;
  private Boolean isActive = false;

  /**
   * Identifier associated to the behavior indicator.
   **/
  public AnonymousRepresentation9 indicatorId(String indicatorId) {
    this.indicatorId = indicatorId;
    return this;
  }

  

  @JsonProperty("indicatorId")
  public String getIndicatorId() {
    return indicatorId;
  }
  public void setIndicatorId(String indicatorId) {
    this.indicatorId = indicatorId;
  }

  /**
   * Description of the behavior indicator.
   **/
  public AnonymousRepresentation9 name(String name) {
    this.name = name;
    return this;
  }

  

  @JsonProperty("name")
  public String getName() {
    return name;
  }
  public void setName(String name) {
    this.name = name;
  }

  /**
   * Indicates whether the indicator is enabled.
   **/
  public AnonymousRepresentation9 isActive(Boolean isActive) {
    this.isActive = isActive;
    return this;
  }

  

  @JsonProperty("isActive")
  public Boolean getIsActive() {
    return isActive;
  }
  public void setIsActive(Boolean isActive) {
    this.isActive = isActive;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AnonymousRepresentation9 anonymousRepresentation9 = (AnonymousRepresentation9) o;
    return Objects.equals(indicatorId, anonymousRepresentation9.indicatorId) &&
        Objects.equals(name, anonymousRepresentation9.name) &&
        Objects.equals(isActive, anonymousRepresentation9.isActive);
  }

  @Override
  public int hashCode() {
    return Objects.hash(indicatorId, name, isActive);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AnonymousRepresentation9 {\n");
    
    sb.append("    indicatorId: ").append(toIndentedString(indicatorId)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    isActive: ").append(toIndentedString(isActive)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
