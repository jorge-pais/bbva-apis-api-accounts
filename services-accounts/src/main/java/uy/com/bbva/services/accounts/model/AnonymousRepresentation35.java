package uy.com.bbva.services.accounts.model;

import com.fasterxml.jackson.annotation.JsonProperty;


import java.util.Objects;



public class AnonymousRepresentation35   {
  
  private RelatedContract data = null;

  /**
   **/
  public AnonymousRepresentation35 data(RelatedContract data) {
    this.data = data;
    return this;
  }

  

  @JsonProperty("data")
  public RelatedContract getData() {
    return data;
  }
  public void setData(RelatedContract data) {
    this.data = data;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AnonymousRepresentation35 anonymousRepresentation35 = (AnonymousRepresentation35) o;
    return Objects.equals(data, anonymousRepresentation35.data);
  }

  @Override
  public int hashCode() {
    return Objects.hash(data);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AnonymousRepresentation35 {\n");
    
    sb.append("    data: ").append(toIndentedString(data)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}