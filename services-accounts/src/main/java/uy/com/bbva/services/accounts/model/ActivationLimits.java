
package uy.com.bbva.services.accounts.model;

import com.fasterxml.jackson.annotation.JsonProperty;


import java.util.Objects;


public class ActivationLimits   {
  
  private String id = null;
  private String name = null;
  private ActivationLimitsAmountLimit amountLimit = null;

  /**
   * Limit identifier.
   **/
  public ActivationLimits id(String id) {
    this.id = id;
    return this;
  }

  

  @JsonProperty("id")
  public String getId() {
    return id;
  }
  public void setId(String id) {
    this.id = id;
  }

  /**
   * Limit description.
   **/
  public ActivationLimits name(String name) {
    this.name = name;
    return this;
  }

  

  @JsonProperty("name")
  public String getName() {
    return name;
  }
  public void setName(String name) {
    this.name = name;
  }

  /**
   * Monetary restriction related to the operation.
   **/
  public ActivationLimits amountLimit(ActivationLimitsAmountLimit amountLimit) {
    this.amountLimit = amountLimit;
    return this;
  }

  

  @JsonProperty("amountLimit")
  public ActivationLimitsAmountLimit getAmountLimit() {
    return amountLimit;
  }
  public void setAmountLimit(ActivationLimitsAmountLimit amountLimit) {
    this.amountLimit = amountLimit;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ActivationLimits activationLimits = (ActivationLimits) o;
    return Objects.equals(id, activationLimits.id) &&
        Objects.equals(name, activationLimits.name) &&
        Objects.equals(amountLimit, activationLimits.amountLimit);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, name, amountLimit);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ActivationLimits {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    amountLimit: ").append(toIndentedString(amountLimit)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}