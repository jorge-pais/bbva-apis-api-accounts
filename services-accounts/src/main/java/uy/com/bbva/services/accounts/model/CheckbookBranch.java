package uy.com.bbva.services.accounts.model;

import com.fasterxml.jackson.annotation.JsonProperty;



import java.util.Objects;


/**
 * Office of the bank where the customer will pick up the checkbook.
 **/



public class CheckbookBranch   {
  
  private String id = null;
  private String name = null;
  private String costCenter = null;

  /**
   * Unique branch identifier.
   **/
  public CheckbookBranch id(String id) {
    this.id = id;
    return this;
  }

  

  @JsonProperty("id")
  public String getId() {
    return id;
  }
  public void setId(String id) {
    this.id = id;
  }

  /**
   * Branch name.
   **/
  public CheckbookBranch name(String name) {
    this.name = name;
    return this;
  }

  

  @JsonProperty("name")
  public String getName() {
    return name;
  }
  public void setName(String name) {
    this.name = name;
  }

  /**
   * A department or unit within a bank to which costs may be charged for accounting purposes.
   **/
  public CheckbookBranch costCenter(String costCenter) {
    this.costCenter = costCenter;
    return this;
  }

  

  @JsonProperty("costCenter")
  public String getCostCenter() {
    return costCenter;
  }
  public void setCostCenter(String costCenter) {
    this.costCenter = costCenter;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CheckbookBranch checkbookBranch = (CheckbookBranch) o;
    return Objects.equals(id, checkbookBranch.id) &&
        Objects.equals(name, checkbookBranch.name) &&
        Objects.equals(costCenter, checkbookBranch.costCenter);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, name, costCenter);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class CheckbookBranch {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    costCenter: ").append(toIndentedString(costCenter)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
