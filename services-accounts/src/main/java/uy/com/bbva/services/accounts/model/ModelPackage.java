package uy.com.bbva.services.accounts.model;

import com.fasterxml.jackson.annotation.JsonProperty;



import java.util.Objects;


/**
 * Identifies the package to which the account belongs. A package brings together different products that customers contract together. For example: \&quot;Libreton Full\&quot;, that groups credit card, savings account and checking account
 **/



public class ModelPackage   {
  
  private String id = null;
  private String name = null;

  /**
   * The package identifier.
   **/
  public ModelPackage id(String id) {
    this.id = id;
    return this;
  }

  

  @JsonProperty("id")
  public String getId() {
    return id;
  }
  public void setId(String id) {
    this.id = id;
  }

  /**
   * Package name.
   **/
  public ModelPackage name(String name) {
    this.name = name;
    return this;
  }

  

  @JsonProperty("name")
  public String getName() {
    return name;
  }
  public void setName(String name) {
    this.name = name;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ModelPackage _package = (ModelPackage) o;
    return Objects.equals(id, _package.id) &&
        Objects.equals(name, _package.name);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, name);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ModelPackage {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
