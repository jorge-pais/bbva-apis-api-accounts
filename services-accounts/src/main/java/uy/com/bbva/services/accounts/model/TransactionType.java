

package uy.com.bbva.services.accounts.model;

import com.fasterxml.jackson.annotation.JsonProperty;



import java.util.Objects;


/**
 * Transaction type. It talks about which kind of operative has created this transaction. DISCLAIMER: Nowsdays some geographies have no possibility of identifying all the types of transaction that are detailed below. This should not become an impediment that in the near future this inconvenience be fixed by the corresponding backends and allow the customers to know and filter those transactions according to their criteria.
 **/



public class TransactionType   {
  
  private String id = null;
  private String name = null;
  private TransactionTypeInternalCode internalCode = null;

  /**
   * Transaction type identifier. DISCLAIMER: UNCATEGORIZED type will be accompanied by information that must be fulfilled on the attribute internalCode that belogs to transactionType.
   **/
  public TransactionType id(String id) {
    this.id = id;
    return this;
  }

  

  @JsonProperty("id")
  public String getId() {
    return id;
  }
  public void setId(String id) {
    this.id = id;
  }

  /**
   * Name associated to the transaction type.
   **/
  public TransactionType name(String name) {
    this.name = name;
    return this;
  }

  

  @JsonProperty("name")
  public String getName() {
    return name;
  }
  public void setName(String name) {
    this.name = name;
  }

  /**
   * Associated to the transaction type that can\\`t be categorized and must be considered. DISCLAIMER: In case of using UNCATEGORIZED type, this attribute must be forcible informed with the informations that talks about which kind of transaction is being shown.
   **/
  public TransactionType internalCode(TransactionTypeInternalCode internalCode) {
    this.internalCode = internalCode;
    return this;
  }

  

  @JsonProperty("internalCode")
  public TransactionTypeInternalCode getInternalCode() {
    return internalCode;
  }
  public void setInternalCode(TransactionTypeInternalCode internalCode) {
    this.internalCode = internalCode;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TransactionType transactionType = (TransactionType) o;
    return Objects.equals(id, transactionType.id) &&
        Objects.equals(name, transactionType.name) &&
        Objects.equals(internalCode, transactionType.internalCode);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, name, internalCode);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class TransactionType {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    internalCode: ").append(toIndentedString(internalCode)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
