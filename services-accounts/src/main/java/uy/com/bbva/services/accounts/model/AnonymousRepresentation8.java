package uy.com.bbva.services.accounts.model;

import com.fasterxml.jackson.annotation.JsonProperty;


import java.util.Date;
import java.util.Objects;



public class AnonymousRepresentation8   {
  
  private String id = null;
  private CheckbookType checkbookType = null;
  private CheckbookFormat format = null;
  private Date requestDate = null;
  private Date deliveryDate = null;
  private CheckbookStatus status = null;
  private String firstCheckId = null;
  private String lastCheckId = null;
  private CheckbookBranch branch = null;

  /**
   * Checkbook identifier.
   **/
  public AnonymousRepresentation8 id(String id) {
    this.id = id;
    return this;
  }

  

  @JsonProperty("id")
  public String getId() {
    return id;
  }
  public void setId(String id) {
    this.id = id;
  }

  /**
   * Checkbook type.
   **/
  public AnonymousRepresentation8 checkbookType(CheckbookType checkbookType) {
    this.checkbookType = checkbookType;
    return this;
  }

  

  @JsonProperty("checkbookType")
  public CheckbookType getCheckbookType() {
    return checkbookType;
  }
  public void setCheckbookType(CheckbookType checkbookType) {
    this.checkbookType = checkbookType;
  }

  /**
   * Printed format.
   **/
  public AnonymousRepresentation8 format(CheckbookFormat format) {
    this.format = format;
    return this;
  }

  

  @JsonProperty("format")
  public CheckbookFormat getFormat() {
    return format;
  }
  public void setFormat(CheckbookFormat format) {
    this.format = format;
  }

  /**
   * String based on ISO-8601 date format for specifying when the user requested the checkbook.
   **/
  public AnonymousRepresentation8 requestDate(Date requestDate) {
    this.requestDate = requestDate;
    return this;
  }

  

  @JsonProperty("requestDate")
  public Date getRequestDate() {
    return requestDate;
  }
  public void setRequestDate(Date requestDate) {
    this.requestDate = requestDate;
  }

  /**
   * String based on ISO-8601 date format for specifying when the user picked up the checkbook.
   **/
  public AnonymousRepresentation8 deliveryDate(Date deliveryDate) {
    this.deliveryDate = deliveryDate;
    return this;
  }

  

  @JsonProperty("deliveryDate")
  public Date getDeliveryDate() {
    return deliveryDate;
  }
  public void setDeliveryDate(Date deliveryDate) {
    this.deliveryDate = deliveryDate;
  }

  /**
   * Checkbook status.
   **/
  public AnonymousRepresentation8 status(CheckbookStatus status) {
    this.status = status;
    return this;
  }

  

  @JsonProperty("status")
  public CheckbookStatus getStatus() {
    return status;
  }
  public void setStatus(CheckbookStatus status) {
    this.status = status;
  }

  /**
   * Numeration of the checkbook\\'s first check.
   **/
  public AnonymousRepresentation8 firstCheckId(String firstCheckId) {
    this.firstCheckId = firstCheckId;
    return this;
  }

  

  @JsonProperty("firstCheckId")
  public String getFirstCheckId() {
    return firstCheckId;
  }
  public void setFirstCheckId(String firstCheckId) {
    this.firstCheckId = firstCheckId;
  }

  /**
   * Numeration of the checkbook\\'s last check.
   **/
  public AnonymousRepresentation8 lastCheckId(String lastCheckId) {
    this.lastCheckId = lastCheckId;
    return this;
  }

  

  @JsonProperty("lastCheckId")
  public String getLastCheckId() {
    return lastCheckId;
  }
  public void setLastCheckId(String lastCheckId) {
    this.lastCheckId = lastCheckId;
  }

  /**
   * Office of the bank where the customer will pick up the checkbook.
   **/
  public AnonymousRepresentation8 branch(CheckbookBranch branch) {
    this.branch = branch;
    return this;
  }

  

  @JsonProperty("branch")
  public CheckbookBranch getBranch() {
    return branch;
  }
  public void setBranch(CheckbookBranch branch) {
    this.branch = branch;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AnonymousRepresentation8 anonymousRepresentation8 = (AnonymousRepresentation8) o;
    return Objects.equals(id, anonymousRepresentation8.id) &&
        Objects.equals(checkbookType, anonymousRepresentation8.checkbookType) &&
        Objects.equals(format, anonymousRepresentation8.format) &&
        Objects.equals(requestDate, anonymousRepresentation8.requestDate) &&
        Objects.equals(deliveryDate, anonymousRepresentation8.deliveryDate) &&
        Objects.equals(status, anonymousRepresentation8.status) &&
        Objects.equals(firstCheckId, anonymousRepresentation8.firstCheckId) &&
        Objects.equals(lastCheckId, anonymousRepresentation8.lastCheckId) &&
        Objects.equals(branch, anonymousRepresentation8.branch);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, checkbookType, format, requestDate, deliveryDate, status, firstCheckId, lastCheckId, branch);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AnonymousRepresentation8 {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    checkbookType: ").append(toIndentedString(checkbookType)).append("\n");
    sb.append("    format: ").append(toIndentedString(format)).append("\n");
    sb.append("    requestDate: ").append(toIndentedString(requestDate)).append("\n");
    sb.append("    deliveryDate: ").append(toIndentedString(deliveryDate)).append("\n");
    sb.append("    status: ").append(toIndentedString(status)).append("\n");
    sb.append("    firstCheckId: ").append(toIndentedString(firstCheckId)).append("\n");
    sb.append("    lastCheckId: ").append(toIndentedString(lastCheckId)).append("\n");
    sb.append("    branch: ").append(toIndentedString(branch)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
