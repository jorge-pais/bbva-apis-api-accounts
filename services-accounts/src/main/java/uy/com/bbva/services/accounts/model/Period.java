package uy.com.bbva.services.accounts.model;

import com.fasterxml.jackson.annotation.JsonProperty;



import java.util.Date;
import java.util.Objects;


/**
 * Payment method period selected by the user for paying credit debt.
 **/



public class Period {

    private String id = null;
    private String name = null;
    private Date checkDate = null;

    /**
     * Period identifier.
     **/
    public Period id(String id) {
        this.id = id;
        return this;
    }



    @JsonProperty("id")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    /**
     * Period name.
     **/
    public Period name(String name) {
        this.name = name;
        return this;
    }



    @JsonProperty("name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * String based on ISO-8601 date format for specifying the next date when the Facts will be evaluated.
     **/
    public Period checkDate(Date checkDate) {
        this.checkDate = checkDate;
        return this;
    }



    @JsonProperty("checkDate")
    public Date getCheckDate() {
        return checkDate;
    }

    public void setCheckDate(Date checkDate) {
        this.checkDate = checkDate;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Period period = (Period) o;
        return Objects.equals(id, period.id) &&
                Objects.equals(name, period.name) &&
                Objects.equals(checkDate, period.checkDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, checkDate);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class Period {\n");

        sb.append("    id: ").append(toIndentedString(id)).append("\n");
        sb.append("    name: ").append(toIndentedString(name)).append("\n");
        sb.append("    checkDate: ").append(toIndentedString(checkDate)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
