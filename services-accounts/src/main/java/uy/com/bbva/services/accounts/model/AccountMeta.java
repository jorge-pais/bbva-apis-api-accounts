package uy.com.bbva.services.accounts.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;


public class AccountMeta   {
  
  private String accountLevel = null;

  /**
   * The level of the account allows defining the requirements for opening an account, the operations and commissions that will be applied.
   **/
  public AccountMeta accountLevel(String accountLevel) {
    this.accountLevel = accountLevel;
    return this;
  }

  
  @JsonProperty("accountLevel")
  public String getAccountLevel() {
    return accountLevel;
  }
  public void setAccountLevel(String accountLevel) {
    this.accountLevel = accountLevel;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AccountMeta accountMeta = (AccountMeta) o;
    return Objects.equals(accountLevel, accountMeta.accountLevel);
  }

  @Override
  public int hashCode() {
    return Objects.hash(accountLevel);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AccountMeta {\n");
    
    sb.append("    accountLevel: ").append(toIndentedString(accountLevel)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
