package uy.com.bbva.services.accounts.model;

import com.fasterxml.jackson.annotation.JsonProperty;


import java.util.Objects;



public class AvailableBalancePendingBalances   {
  
  private Double amount = null;
  private String currency = null;

  /**
   * Pending available balance monetary amount.
   **/
  public AvailableBalancePendingBalances amount(Double amount) {
    this.amount = amount;
    return this;
  }

  

  @JsonProperty("amount")
  public Double getAmount() {
    return amount;
  }
  public void setAmount(Double amount) {
    this.amount = amount;
  }

  /**
   * String based on ISO-4217 for specifying the currency related to the pending available balance.
   **/
  public AvailableBalancePendingBalances currency(String currency) {
    this.currency = currency;
    return this;
  }

  

  @JsonProperty("currency")
  public String getCurrency() {
    return currency;
  }
  public void setCurrency(String currency) {
    this.currency = currency;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AvailableBalancePendingBalances availableBalancePendingBalances = (AvailableBalancePendingBalances) o;
    return Objects.equals(amount, availableBalancePendingBalances.amount) &&
        Objects.equals(currency, availableBalancePendingBalances.currency);
  }

  @Override
  public int hashCode() {
    return Objects.hash(amount, currency);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AvailableBalancePendingBalances {\n");
    
    sb.append("    amount: ").append(toIndentedString(amount)).append("\n");
    sb.append("    currency: ").append(toIndentedString(currency)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
