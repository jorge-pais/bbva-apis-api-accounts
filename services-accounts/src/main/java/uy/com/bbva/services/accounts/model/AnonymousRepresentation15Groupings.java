package uy.com.bbva.services.accounts.model;

import com.fasterxml.jackson.annotation.JsonProperty;


import java.util.ArrayList;
import java.util.List;
import java.util.Objects;



public class AnonymousRepresentation15Groupings   {
  
  private String id = null;
  private String name = null;
  private List<AnonymousRepresentation15GroupingsItems> items = new ArrayList<AnonymousRepresentation15GroupingsItems>();

  /**
   * Grouping identifier, it can be to increased by adding new ways and variables for grouping balances.
   **/
  public AnonymousRepresentation15Groupings id(String id) {
    this.id = id;
    return this;
  }

  

  @JsonProperty("id")
  public String getId() {
    return id;
  }
  public void setId(String id) {
    this.id = id;
  }

  /**
   * Description or name of the identifier for show to the customer the meaning of this grouping.
   **/
  public AnonymousRepresentation15Groupings name(String name) {
    this.name = name;
    return this;
  }

  

  @JsonProperty("name")
  public String getName() {
    return name;
  }
  public void setName(String name) {
    this.name = name;
  }

  /**
   * Values used to filter customer accounts according to the discriminator defined by the parent groupings.id attribute.
   **/
  public AnonymousRepresentation15Groupings items(List<AnonymousRepresentation15GroupingsItems> items) {
    this.items = items;
    return this;
  }

  

  @JsonProperty("items")
  public List<AnonymousRepresentation15GroupingsItems> getItems() {
    return items;
  }
  public void setItems(List<AnonymousRepresentation15GroupingsItems> items) {
    this.items = items;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AnonymousRepresentation15Groupings anonymousRepresentation15Groupings = (AnonymousRepresentation15Groupings) o;
    return Objects.equals(id, anonymousRepresentation15Groupings.id) &&
        Objects.equals(name, anonymousRepresentation15Groupings.name) &&
        Objects.equals(items, anonymousRepresentation15Groupings.items);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, name, items);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AnonymousRepresentation15Groupings {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    items: ").append(toIndentedString(items)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
