package uy.com.bbva.services.accounts.model;

import com.fasterxml.jackson.annotation.JsonProperty;


import java.util.Date;
import java.util.Objects;



public class ConditionOutcomes {
  
  private OutcomeType outcomeType = null;
  private Double feePercentage = null;
  private Amount feeAmount = null;
  private Apply apply = null;
  private Date dueDate = null;

  /**
   * Outcome type.
   **/
  public ConditionOutcomes outcomeType(OutcomeType outcomeType) {
    this.outcomeType = outcomeType;
    return this;
  }

  

  @JsonProperty("outcomeType")
  public OutcomeType getOutcomeType() {
    return outcomeType;
  }
  public void setOutcomeType(OutcomeType outcomeType) {
    this.outcomeType = outcomeType;
  }

  /**
   * Outcome percental value.
   **/
  public ConditionOutcomes feePercentage(Double feePercentage) {
    this.feePercentage = feePercentage;
    return this;
  }

  

  @JsonProperty("feePercentage")
  public Double getFeePercentage() {
    return feePercentage;
  }
  public void setFeePercentage(Double feePercentage) {
    this.feePercentage = feePercentage;
  }

  /**
   * Outcome amount.
   **/
  public ConditionOutcomes feeAmount(Amount feeAmount) {
    this.feeAmount = feeAmount;
    return this;
  }

  

  @JsonProperty("feeAmount")
  public Amount getFeeAmount() {
    return feeAmount;
  }
  public void setFeeAmount(Amount feeAmount) {
    this.feeAmount = feeAmount;
  }

  /**
   * Property or value over which Outcome fee applies.
   **/
  public ConditionOutcomes apply(Apply apply) {
    this.apply = apply;
    return this;
  }

  

  @JsonProperty("apply")
  public Apply getApply() {
    return apply;
  }
  public void setApply(Apply apply) {
    this.apply = apply;
  }

  /**
   * String based on ISO-8601 date format for specifying the next date when the Outcome will be generated.
   **/
  public ConditionOutcomes dueDate(Date dueDate) {
    this.dueDate = dueDate;
    return this;
  }

  

  @JsonProperty("dueDate")
  public Date getDueDate() {
    return dueDate;
  }
  public void setDueDate(Date dueDate) {
    this.dueDate = dueDate;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ConditionOutcomes conditionOutcomes = (ConditionOutcomes) o;
    return Objects.equals(outcomeType, conditionOutcomes.outcomeType) &&
        Objects.equals(feePercentage, conditionOutcomes.feePercentage) &&
        Objects.equals(feeAmount, conditionOutcomes.feeAmount) &&
        Objects.equals(apply, conditionOutcomes.apply) &&
        Objects.equals(dueDate, conditionOutcomes.dueDate);
  }

  @Override
  public int hashCode() {
    return Objects.hash(outcomeType, feePercentage, feeAmount, apply, dueDate);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ConditionOutcomes {\n");
    
    sb.append("    outcomeType: ").append(toIndentedString(outcomeType)).append("\n");
    sb.append("    feePercentage: ").append(toIndentedString(feePercentage)).append("\n");
    sb.append("    feeAmount: ").append(toIndentedString(feeAmount)).append("\n");
    sb.append("    apply: ").append(toIndentedString(apply)).append("\n");
    sb.append("    dueDate: ").append(toIndentedString(dueDate)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}