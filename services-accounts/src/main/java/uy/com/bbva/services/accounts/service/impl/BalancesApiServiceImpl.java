package uy.com.bbva.services.accounts.service.impl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uy.com.bbva.services.accounts.dao.DAO;
import uy.com.bbva.services.accounts.response.BalanceDataList;
import uy.com.bbva.services.accounts.service.BalancesApiService;
import uy.com.bbva.services.commons.exceptions.ServiceException;
import java.util.Date;
@Service
public class BalancesApiServiceImpl extends BalancesApiService {
	
	@Autowired
	private DAO dao;
	
    @Override
    public BalanceDataList getBalances(String accountFamilyId, String accountTypeId, String currenciesCurrency, Date periodStartDate, Date periodEndDate, String documento) throws
			ServiceException{
        return dao.balances(documento, accountFamilyId, accountTypeId, currenciesCurrency, periodStartDate, periodEndDate);
    }
}