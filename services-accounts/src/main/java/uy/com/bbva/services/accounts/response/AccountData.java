package uy.com.bbva.services.accounts.response;
import com.fasterxml.jackson.annotation.JsonProperty;
import uy.com.bbva.services.accounts.model.Account;
import java.util.Objects;
public class AccountData{
  
  private Account data = null;
  /**
   **/
  public AccountData data(Account data) {
    this.data = data;
    return this;
  }
  
  @JsonProperty("data")
  public Account getData() {
    return data;
  }
  public void setData(Account data) {
    this.data = data;
  }
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AccountData accountData = (AccountData) o;
    return Objects.equals(data, accountData.data);
  }
  @Override
  public int hashCode() {
    return Objects.hash(data);
  }
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AccountData {\n");
    
    sb.append("    data: ").append(toIndentedString(data)).append("\n");
    sb.append("}");
    return sb.toString();
  }
  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}