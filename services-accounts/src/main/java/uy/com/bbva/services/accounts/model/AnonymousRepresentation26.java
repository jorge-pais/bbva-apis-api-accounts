package uy.com.bbva.services.accounts.model;

import com.fasterxml.jackson.annotation.JsonProperty;


import java.util.ArrayList;
import java.util.List;
import java.util.Objects;



public class AnonymousRepresentation26   {
  
  private List<AnonymousRepresentation7> data = new ArrayList<AnonymousRepresentation7>();
  private Pagination pagination = null;

  /**
   **/
  public AnonymousRepresentation26 data(List<AnonymousRepresentation7> data) {
    this.data = data;
    return this;
  }

  

  @JsonProperty("data")
  public List<AnonymousRepresentation7> getData() {
    return data;
  }
  public void setData(List<AnonymousRepresentation7> data) {
    this.data = data;
  }

  /**
   **/
  public AnonymousRepresentation26 pagination(Pagination pagination) {
    this.pagination = pagination;
    return this;
  }

  

  @JsonProperty("pagination")
  public Pagination getPagination() {
    return pagination;
  }
  public void setPagination(Pagination pagination) {
    this.pagination = pagination;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AnonymousRepresentation26 anonymousRepresentation26 = (AnonymousRepresentation26) o;
    return Objects.equals(data, anonymousRepresentation26.data) &&
        Objects.equals(pagination, anonymousRepresentation26.pagination);
  }

  @Override
  public int hashCode() {
    return Objects.hash(data, pagination);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AnonymousRepresentation26 {\n");
    
    sb.append("    data: ").append(toIndentedString(data)).append("\n");
    sb.append("    pagination: ").append(toIndentedString(pagination)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
