package uy.com.bbva.services.accounts.dao;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import uy.com.bbva.services.accounts.model.AccountFamily;
import uy.com.bbva.services.accounts.model.AccountNumberType;
import uy.com.bbva.services.accounts.model.AccountType;
import uy.com.bbva.services.accounts.model.AvailableBalance;
import uy.com.bbva.services.accounts.model.AvailableBalanceCurrentBalances;
import uy.com.bbva.services.accounts.model.AvailableBalancePendingBalances;
import uy.com.bbva.services.accounts.model.AvailableBalancePostedBalances;
import uy.com.bbva.services.accounts.model.Branch;
import uy.com.bbva.services.accounts.model.Contract;
import uy.com.bbva.services.accounts.model.ContractNumberType;
import uy.com.bbva.services.accounts.model.ContractProduct;
import uy.com.bbva.services.accounts.model.MovimientoBantotal;
import uy.com.bbva.services.accounts.model.RelatedContract;
import uy.com.bbva.services.accounts.model.RelatedContractNumberType;
import uy.com.bbva.services.accounts.model.RelatedContractProduct;
import uy.com.bbva.services.accounts.model.SaldoBantotal;
import uy.com.bbva.services.accounts.model.Status;
import uy.com.bbva.services.accounts.model.Title;
import uy.com.bbva.services.accounts.model.TransactionFinancingType;
import uy.com.bbva.services.accounts.model.TransactionMoneyFlow;
import uy.com.bbva.services.accounts.model.TransactionStatus;
import uy.com.bbva.services.accounts.model.TransactionType;
public class AccountsUtil {
	
	public static AccountFamily getAccountFamily(int modulo) {
		AccountFamily af = new AccountFamily();
		if (modulo == 21 || modulo == 23) {
			af.setId("SAVINGS");
			af.setName("Saving Account");
		} else {
			af.setId("COMMON");
			af.setName("Common Account");
		}
		return af;
	}
	public static AccountType getAccountType(int modulo) {
		AccountType af = new AccountType();
		if (modulo == 21) {
			af.setId("21");
			af.setName("Caja de Ahorro");
		} else if (modulo == 23) {
			af.setId("23");
			af.setName("Cuenta Cajero");
		} else if (modulo == 20) {
			af.setId("20");
			af.setName("Cuenta Corriente");
		}
		return af;
	}
	public static String getId(int suc, int cuenta, int moneda, int papel, int subcuenta, int modulo) {
		/*
		 * El saldo origen se identifica con un string con el siguiente formato:
		 * TTMMMMCCCCCCCCCBBBSSSPPPP TT - Tipo de Cuenta (20-Cuenta Corriente,
		 * 21-Caja de Ahorro, 23-Cuenta Cajero MMMM - Moneda (0000, 2222, 11111)
		 * CCCCCCCCC - Cuenta BBB - Sucursal SSS - Subcuenta PPPP - Papel
		 */
		return StringUtils.leftPad(String.valueOf(modulo), 2, "0") + StringUtils.leftPad(String.valueOf(moneda), 4, "0")
				+ StringUtils.leftPad(String.valueOf(cuenta), 9, "0") + StringUtils.leftPad(String.valueOf(suc), 3, "0")
				+ StringUtils.leftPad(String.valueOf(subcuenta), 3, "0")
				+ StringUtils.leftPad(String.valueOf(papel), 4, "0");
	}
	public static  String getSimboloMoneda(int moneda, int papel) {
		if(papel == 20 && moneda == 0000){
			return "UYI";
		} else if (moneda == 2222) {
			return "USD";
		} else if (moneda == 1111) {
			return "EUR";
		} else {
			return "UYU";
		}
	}
	public static String getReminderCode(int modulo, int moneda, int papel ,int suc, int subcuenta) {
		if (modulo == 20) {
			return "CC-" + getSimboloMoneda(moneda, papel) + " - " + suc + " - " + subcuenta;
		} else if (modulo == 21) {
			return "CA-" + getSimboloMoneda(moneda, papel) + " - " + suc + " - " + subcuenta;
		} else if (modulo == 23) {
			return "CAJ-" + getSimboloMoneda(moneda, papel) + " - " + suc + " - " + subcuenta;
		} else {
			return "Otro tipo de cuenta";
		}
	}
	public static String getTitle(int modulo, int moneda, int suc, int subcuenta, HashMap<Integer,Branch> sucursales) {
		if (modulo == 20) {
			return "C.CORRIENTE" + 
					 "-" + sucursales.get(suc).getName().trim() + 
					 "(" + subcuenta + ")";
		} else if (modulo == 21) {
			return "C/AHORRO" + 
					"-" + sucursales.get(suc).getName().trim() + 
					"(" + subcuenta + ")";
		} else if (modulo == 23) {
			return "Cta.CAJERO" +
					"-" + sucursales.get(suc).getName().trim() + 
					"(" + subcuenta + ")";
		} else {
			return "Otro tipo de cuenta";
		}
	}
	public static String getAlias(int suc, int subcuenta, HashMap<Integer,Branch> sucursales) {
			return sucursales.get(suc).getName().trim() +" (" + subcuenta + ")";
	}
	public static AvailableBalance getAvailableBalance(double saldo, double saldoAnterior, int moneda, int papel) {
		AvailableBalance ab = new AvailableBalance();
		AvailableBalanceCurrentBalances cb = new AvailableBalanceCurrentBalances();
		cb.setAmount(saldo);
		cb.setCurrency(getSimboloMoneda(moneda, papel));
		ab.getCurrentBalances().add(cb);
		AvailableBalancePendingBalances pb = new AvailableBalancePendingBalances();
		pb.setAmount(0d);
		pb.setCurrency(getSimboloMoneda(moneda, papel));
		ab.getPendingBalances().add(pb);
		AvailableBalancePostedBalances pob = new AvailableBalancePostedBalances();
		pob.setAmount(saldoAnterior);
		pob.setCurrency(getSimboloMoneda(moneda, papel));
		ab.getPostedBalances().add(pob);
		return ab;
	}
	public static AccountNumberType getNumberType() {
		AccountNumberType nt = new AccountNumberType();
		nt.setId("LIC");
		nt.setName("Local Internal Code");
		return nt;
	}
	public static Status getStatus(int estado, int scfunc) {
		Status status = new Status();
		if (estado == 0) {
			status.setId("ACTIVATED");
			status.setName("The account is fully operative.");
		} else if (estado == 81) {
			status.setId("DORMANT");
			status.setName("The account has been unused for 180 days.");
		} else if (estado == 1 && scfunc==99) {
			status.setId("CANCELED");
			status.setName("The account is canceled and the user can't operate with it.");
		} else {
			status.setId("BLOCKED");
			status.setName("The account has been blocked by a justified reason.");
		}
		return status;
	}
	public static Title getTitle(int modulo, String nombre) {
		Title title = new Title();
		title.setId(String.valueOf(modulo));
		title.setName(nombre);
		return title;
	}
	public static List<RelatedContract> getRelatedContracts(int cuenta){
		RelatedContract relContrac = new RelatedContract();
		relContrac.setContractId(String.valueOf(cuenta));
		relContrac.setNumber(String.valueOf(cuenta));
		RelatedContractNumberType rcnt = new RelatedContractNumberType();
		rcnt.setId("LIC");
		rcnt.setName("Local Internal Code");
		relContrac.setNumberType(rcnt);
		RelatedContractProduct rcp = new RelatedContractProduct();
		rcp.setId("ACCOUNTS");
		rcp.setName("Accounts");
		relContrac.setProduct(rcp);
		ArrayList<RelatedContract> resp = new ArrayList<>();
		resp.add(relContrac);
		return resp;
	}
	public static SaldoBantotal getSaldo(String accountId){
		
		SaldoBantotal saldo = new SaldoBantotal();
		
		saldo.setModulo(Integer.parseInt(accountId.substring(0, 2)));
		saldo.setMoneda(Integer.parseInt(accountId.substring(2, 6)));
		saldo.setCuenta(Integer.parseInt(accountId.substring(6, 15)));
		saldo.setSucursal(Integer.parseInt(accountId.substring(15, 18)));		
		saldo.setSubOperacion(Integer.parseInt(accountId.substring(18, 21)));	
		saldo.setPapel(Integer.parseInt(accountId.substring(21, 25)));	
		
		return saldo;
	}
	public static String getIdTransacction(MovimientoBantotal mb){
		return StringUtils.leftPad(String.valueOf(mb.getModulo()), 3, "0") 
				+ StringUtils.leftPad(String.valueOf(mb.getTrnasaccion()), 3, "0")
				+ StringUtils.leftPad(String.valueOf(mb.getRelacion()), 4, "0") 
				+ StringUtils.leftPad(String.valueOf(mb.getSucursal()), 3, "0")
				+ mb.getFechaContabilizado();
	}
	
	public static TransactionMoneyFlow getMoneyFlow(int debOCre){
		
		TransactionMoneyFlow tmf = new TransactionMoneyFlow();
		if(debOCre==1){
			tmf.setId("EXPENSE");
			tmf.setName("Débito");
		}else{
			tmf.setId("INCOME");
			tmf.setName("Crédito");			
		}
		return tmf;
		
	}
    public static TransactionType getTransactionType(int mod, int trn){
    	TransactionType tt = new TransactionType();
    	tt.setId("UNCATEGORIZED");
    	tt.setName("UNCATEGORIZED");
    	return tt;
    }
    public static TransactionFinancingType getFinancingType(int mod, int trn){
    	TransactionFinancingType ft = new TransactionFinancingType();
    	ft.setId("NON_FINANCING");
    	ft.setName("NON_FINANCING");
    	return ft;
    }
    
    public static TransactionStatus getStatus(String status){
    	TransactionStatus ts = new TransactionStatus();
    	if ("S".equals(status)){
    		ts.setId("SETTLED");
    		ts.setName("SETTLED");
    	}else if("B".equals("PENDING")){
    		ts.setId("PENDING");
    		ts.setName("PENDING");    		
    	}else{
    		ts.setId("RETURNED");
    		ts.setName("RETURNED");      		
    	}
    	return ts;
    }
    
    public static Contract getContract(int cuenta){
    	Contract contract = new Contract();
		contract.setId( String.valueOf(cuenta)); 
		contract.setNumber(String.valueOf(cuenta));
		ContractNumberType cnt = new ContractNumberType();
		cnt.setId("LIC");
		cnt.setName("Local Internal Code");
		contract.setNumberType(cnt);
		ContractProduct cp = new ContractProduct();
		cp.setId("ACCOUNTS");
		cp.setName("Accounts");
		contract.setProduct(cp);		
		return contract;
    }
}