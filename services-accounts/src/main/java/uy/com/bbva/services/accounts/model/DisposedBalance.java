package uy.com.bbva.services.accounts.model;

import com.fasterxml.jackson.annotation.JsonProperty;



import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


/**
 * This balance is the total spent amount from granted credit. This attribute is mandatory for credit accounts.
 **/

public class DisposedBalance   {
  
  private List<DisposedBalanceCurrentBalances> currentBalances = new ArrayList<DisposedBalanceCurrentBalances>();
  private List<DisposedBalancePostedBalances> postedBalances = new ArrayList<DisposedBalancePostedBalances>();
  private List<DisposedBalancePendingBalances> pendingBalances = new ArrayList<DisposedBalancePendingBalances>();

  /**
   * This balance provides the total spent amount from granted credit. This balance may be provided in several currencies (depending on the country). It\\'s represented by the current disposed balance monetary amount and the currency related to the current disposed balance.
   **/
  public DisposedBalance currentBalances(List<DisposedBalanceCurrentBalances> currentBalances) {
    this.currentBalances = currentBalances;
    return this;
  }

  

  @JsonProperty("currentBalances")
  public List<DisposedBalanceCurrentBalances> getCurrentBalances() {
    return currentBalances;
  }
  public void setCurrentBalances(List<DisposedBalanceCurrentBalances> currentBalances) {
    this.currentBalances = currentBalances;
  }

  /**
   * This balance is the spent amount from granted credit calculated at the end of the last business day. This balance may be provided in several currencies (depending on the country). It\\'s represented by the posted disposed balance monetary amount and the currency related to the posted disposed balance.
   **/
  public DisposedBalance postedBalances(List<DisposedBalancePostedBalances> postedBalances) {
    this.postedBalances = postedBalances;
    return this;
  }

  

  @JsonProperty("postedBalances")
  public List<DisposedBalancePostedBalances> getPostedBalances() {
    return postedBalances;
  }
  public void setPostedBalances(List<DisposedBalancePostedBalances> postedBalances) {
    this.postedBalances = postedBalances;
  }

  /**
   * This balance is the aggregated amount of all pending transactions over granted credit. This amount is the result of substracting the postedBalances from the currentBalances. This balance may be provided in several currencies (depending on the country). It\\'s represented by the pending disposed balance monetary amount and the currency related to the pending disposed balance.
   **/
  public DisposedBalance pendingBalances(List<DisposedBalancePendingBalances> pendingBalances) {
    this.pendingBalances = pendingBalances;
    return this;
  }

  

  @JsonProperty("pendingBalances")
  public List<DisposedBalancePendingBalances> getPendingBalances() {
    return pendingBalances;
  }
  public void setPendingBalances(List<DisposedBalancePendingBalances> pendingBalances) {
    this.pendingBalances = pendingBalances;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DisposedBalance disposedBalance = (DisposedBalance) o;
    return Objects.equals(currentBalances, disposedBalance.currentBalances) &&
        Objects.equals(postedBalances, disposedBalance.postedBalances) &&
        Objects.equals(pendingBalances, disposedBalance.pendingBalances);
  }

  @Override
  public int hashCode() {
    return Objects.hash(currentBalances, postedBalances, pendingBalances);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DisposedBalance {\n");
    
    sb.append("    currentBalances: ").append(toIndentedString(currentBalances)).append("\n");
    sb.append("    postedBalances: ").append(toIndentedString(postedBalances)).append("\n");
    sb.append("    pendingBalances: ").append(toIndentedString(pendingBalances)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
