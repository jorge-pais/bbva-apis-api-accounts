
package uy.com.bbva.services.accounts.model;

import com.fasterxml.jackson.annotation.JsonProperty;


import java.util.Objects;

public class CustomizedFormat   {
  
  private String customizedFormatId = null;
  private String value = null;

  /**
   * Identifier customized format associated to the value of account number.
   **/
  public CustomizedFormat customizedFormatId(String customizedFormatId) {
    this.customizedFormatId = customizedFormatId;
    return this;
  }

  

  @JsonProperty("customizedFormatId")
  public String getCustomizedFormatId() {
    return customizedFormatId;
  }
  public void setCustomizedFormatId(String customizedFormatId) {
    this.customizedFormatId = customizedFormatId;
  }

  /**
   * Value customized of account number.
   **/
  public CustomizedFormat value(String value) {
    this.value = value;
    return this;
  }

  

  @JsonProperty("value")
  public String getValue() {
    return value;
  }
  public void setValue(String value) {
    this.value = value;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CustomizedFormat customizedFormat = (CustomizedFormat) o;
    return Objects.equals(customizedFormatId, customizedFormat.customizedFormatId) &&
        Objects.equals(value, customizedFormat.value);
  }

  @Override
  public int hashCode() {
    return Objects.hash(customizedFormatId, value);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class CustomizedFormat {\n");
    
    sb.append("    customizedFormatId: ").append(toIndentedString(customizedFormatId)).append("\n");
    sb.append("    value: ").append(toIndentedString(value)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
