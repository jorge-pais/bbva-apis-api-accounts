package uy.com.bbva.services.accounts.model;

import com.fasterxml.jackson.annotation.JsonProperty;



import java.util.Objects;


/**
 * Office of the bank associated with the customer.
 **/



public class Branch   {
  
  private String branchId = null;
  private String name = null;
  private String costCenter = null;

  /**
   * Unique branch identifier.
   **/
  public Branch branchId(String branchId) {
    this.branchId = branchId;
    return this;
  }

  

  @JsonProperty("branchId")
  public String getBranchId() {
    return branchId;
  }
  public void setBranchId(String branchId) {
    this.branchId = branchId;
  }

  /**
   * Branch name.
   **/
  public Branch name(String name) {
    this.name = name;
    return this;
  }

  

  @JsonProperty("name")
  public String getName() {
    return name;
  }
  public void setName(String name) {
    this.name = name;
  }

  /**
   * A department or unit within a bank to which costs may be charged for accounting purposes.
   **/
  public Branch costCenter(String costCenter) {
    this.costCenter = costCenter;
    return this;
  }

  

  @JsonProperty("costCenter")
  public String getCostCenter() {
    return costCenter;
  }
  public void setCostCenter(String costCenter) {
    this.costCenter = costCenter;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Branch branch = (Branch) o;
    return Objects.equals(branchId, branch.branchId) &&
        Objects.equals(name, branch.name) &&
        Objects.equals(costCenter, branch.costCenter);
  }

  @Override
  public int hashCode() {
    return Objects.hash(branchId, name, costCenter);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Branch {\n");
    
    sb.append("    branchId: ").append(toIndentedString(branchId)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    costCenter: ").append(toIndentedString(costCenter)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
