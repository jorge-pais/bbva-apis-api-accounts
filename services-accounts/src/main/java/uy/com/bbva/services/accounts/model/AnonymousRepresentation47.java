package uy.com.bbva.services.accounts.model;

import com.fasterxml.jackson.annotation.JsonProperty;


import java.util.ArrayList;
import java.util.List;
import java.util.Objects;



public class AnonymousRepresentation47   {
  
  private List<AnonymousRepresentation16> data = new ArrayList<AnonymousRepresentation16>();

  /**
   **/
  public AnonymousRepresentation47 data(List<AnonymousRepresentation16> data) {
    this.data = data;
    return this;
  }

  

  @JsonProperty("data")
  public List<AnonymousRepresentation16> getData() {
    return data;
  }
  public void setData(List<AnonymousRepresentation16> data) {
    this.data = data;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AnonymousRepresentation47 anonymousRepresentation47 = (AnonymousRepresentation47) o;
    return Objects.equals(data, anonymousRepresentation47.data);
  }

  @Override
  public int hashCode() {
    return Objects.hash(data);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AnonymousRepresentation47 {\n");
    
    sb.append("    data: ").append(toIndentedString(data)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
