package uy.com.bbva.services.accounts.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

public class AnonymousRepresentation13   {
  
  private String customizedFormatId = null;
  private String value = null;

  /**
   * Identifier customized format associated to the value of account number.
   **/
  public AnonymousRepresentation13 customizedFormatId(String customizedFormatId) {
    this.customizedFormatId = customizedFormatId;
    return this;
  }

  

  @JsonProperty("customizedFormatId")
  public String getCustomizedFormatId() {
    return customizedFormatId;
  }
  public void setCustomizedFormatId(String customizedFormatId) {
    this.customizedFormatId = customizedFormatId;
  }

  /**
   * Value customized of account number.
   **/
  public AnonymousRepresentation13 value(String value) {
    this.value = value;
    return this;
  }

  

  @JsonProperty("value")
  public String getValue() {
    return value;
  }
  public void setValue(String value) {
    this.value = value;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AnonymousRepresentation13 anonymousRepresentation13 = (AnonymousRepresentation13) o;
    return Objects.equals(customizedFormatId, anonymousRepresentation13.customizedFormatId) &&
        Objects.equals(value, anonymousRepresentation13.value);
  }

  @Override
  public int hashCode() {
    return Objects.hash(customizedFormatId, value);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AnonymousRepresentation13 {\n");
    
    sb.append("    customizedFormatId: ").append(toIndentedString(customizedFormatId)).append("\n");
    sb.append("    value: ").append(toIndentedString(value)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}