package uy.com.bbva.services.accounts.model;

import com.fasterxml.jackson.annotation.JsonProperty;


import java.util.Objects;



public class RelatedContract   {
  
  private String relatedContractId = null;
  private String contractId = null;
  private String number = null;
  private RelatedContractNumberType numberType = null;
  private RelatedContractProduct product = null;
  private RelationType relationType = null;

  /**
   * Identifier associated to the relationship between the card and the contract.
   **/
  public RelatedContract relatedContractId(String relatedContractId) {
    this.relatedContractId = relatedContractId;
    return this;
  }

  

  @JsonProperty("relatedContractId")
  public String getRelatedContractId() {
    return relatedContractId;
  }
  public void setRelatedContractId(String relatedContractId) {
    this.relatedContractId = relatedContractId;
  }

  /**
   * Identifier associated to the contract.
   **/
  public RelatedContract contractId(String contractId) {
    this.contractId = contractId;
    return this;
  }

  

  @JsonProperty("contractId")
  public String getContractId() {
    return contractId;
  }
  public void setContractId(String contractId) {
    this.contractId = contractId;
  }

  /**
   * Contract number.
   **/
  public RelatedContract number(String number) {
    this.number = number;
    return this;
  }

  

  @JsonProperty("number")
  public String getNumber() {
    return number;
  }
  public void setNumber(String number) {
    this.number = number;
  }

  /**
   * Contract number type based on the financial product type.
   **/
  public RelatedContract numberType(RelatedContractNumberType numberType) {
    this.numberType = numberType;
    return this;
  }

  

  @JsonProperty("numberType")
  public RelatedContractNumberType getNumberType() {
    return numberType;
  }
  public void setNumberType(RelatedContractNumberType numberType) {
    this.numberType = numberType;
  }

  /**
   * Financial product associated to the contract.
   **/
  public RelatedContract product(RelatedContractProduct product) {
    this.product = product;
    return this;
  }

  

  @JsonProperty("product")
  public RelatedContractProduct getProduct() {
    return product;
  }
  public void setProduct(RelatedContractProduct product) {
    this.product = product;
  }

  /**
   * Type of relation between the related contract and the account.
   **/
  public RelatedContract relationType(RelationType relationType) {
    this.relationType = relationType;
    return this;
  }

  

  @JsonProperty("relationType")
  public RelationType getRelationType() {
    return relationType;
  }
  public void setRelationType(RelationType relationType) {
    this.relationType = relationType;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    RelatedContract relatedContract = (RelatedContract) o;
    return Objects.equals(relatedContractId, relatedContract.relatedContractId) &&
        Objects.equals(contractId, relatedContract.contractId) &&
        Objects.equals(number, relatedContract.number) &&
        Objects.equals(numberType, relatedContract.numberType) &&
        Objects.equals(product, relatedContract.product) &&
        Objects.equals(relationType, relatedContract.relationType);
  }

  @Override
  public int hashCode() {
    return Objects.hash(relatedContractId, contractId, number, numberType, product, relationType);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class RelatedContract {\n");
    
    sb.append("    relatedContractId: ").append(toIndentedString(relatedContractId)).append("\n");
    sb.append("    contractId: ").append(toIndentedString(contractId)).append("\n");
    sb.append("    number: ").append(toIndentedString(number)).append("\n");
    sb.append("    numberType: ").append(toIndentedString(numberType)).append("\n");
    sb.append("    product: ").append(toIndentedString(product)).append("\n");
    sb.append("    relationType: ").append(toIndentedString(relationType)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
