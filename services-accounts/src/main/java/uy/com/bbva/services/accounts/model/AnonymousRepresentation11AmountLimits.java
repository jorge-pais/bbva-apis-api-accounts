package uy.com.bbva.services.accounts.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

public class AnonymousRepresentation11AmountLimits   {
  
  private Double amount = null;
  private String currency = null;

  /**
   * Monetary amount related to the limit.
   **/
  public AnonymousRepresentation11AmountLimits amount(Double amount) {
    this.amount = amount;
    return this;
  }

  

  @JsonProperty("amount")
  public Double getAmount() {
    return amount;
  }
  public void setAmount(Double amount) {
    this.amount = amount;
  }

  /**
   * String based on ISO-4217 for specifying the currency related to the amount limit.
   **/
  public AnonymousRepresentation11AmountLimits currency(String currency) {
    this.currency = currency;
    return this;
  }

  

  @JsonProperty("currency")
  public String getCurrency() {
    return currency;
  }
  public void setCurrency(String currency) {
    this.currency = currency;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AnonymousRepresentation11AmountLimits anonymousRepresentation11AmountLimits = (AnonymousRepresentation11AmountLimits) o;
    return Objects.equals(amount, anonymousRepresentation11AmountLimits.amount) &&
        Objects.equals(currency, anonymousRepresentation11AmountLimits.currency);
  }

  @Override
  public int hashCode() {
    return Objects.hash(amount, currency);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AnonymousRepresentation11AmountLimits {\n");
    
    sb.append("    amount: ").append(toIndentedString(amount)).append("\n");
    sb.append("    currency: ").append(toIndentedString(currency)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}