package uy.com.bbva.services.accounts.model;

import com.fasterxml.jackson.annotation.JsonProperty;


import java.util.Objects;


public class AnonymousRepresentation5Debits   {
  
  private Double totalTransactions = null;
  private AnonymousRepresentation5DebitsBalance balance = null;

  /**
   * Total number of debits transactions for the current day.
   **/
  public AnonymousRepresentation5Debits totalTransactions(Double totalTransactions) {
    this.totalTransactions = totalTransactions;
    return this;
  }

  

  @JsonProperty("totalTransactions")
  public Double getTotalTransactions() {
    return totalTransactions;
  }
  public void setTotalTransactions(Double totalTransactions) {
    this.totalTransactions = totalTransactions;
  }

  /**
   * Total monetary amount of the credit transactions posted along the current day.
   **/
  public AnonymousRepresentation5Debits balance(AnonymousRepresentation5DebitsBalance balance) {
    this.balance = balance;
    return this;
  }

  

  @JsonProperty("balance")
  public AnonymousRepresentation5DebitsBalance getBalance() {
    return balance;
  }
  public void setBalance(AnonymousRepresentation5DebitsBalance balance) {
    this.balance = balance;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AnonymousRepresentation5Debits anonymousRepresentation5Debits = (AnonymousRepresentation5Debits) o;
    return Objects.equals(totalTransactions, anonymousRepresentation5Debits.totalTransactions) &&
        Objects.equals(balance, anonymousRepresentation5Debits.balance);
  }

  @Override
  public int hashCode() {
    return Objects.hash(totalTransactions, balance);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AnonymousRepresentation5Debits {\n");
    
    sb.append("    totalTransactions: ").append(toIndentedString(totalTransactions)).append("\n");
    sb.append("    balance: ").append(toIndentedString(balance)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
