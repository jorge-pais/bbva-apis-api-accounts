package uy.com.bbva.services.accounts.model;

import com.fasterxml.jackson.annotation.JsonProperty;



import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


/**
 * This attribute provides the account balance. This balance may be provided in several currencies (depending on the country).
 **/



public class AvailableBalance   {
  
  private List<AvailableBalanceCurrentBalances> currentBalances = new ArrayList<AvailableBalanceCurrentBalances>();
  private List<AvailableBalancePostedBalances> postedBalances = new ArrayList<AvailableBalancePostedBalances>();
  private List<AvailableBalancePendingBalances> pendingBalances = new ArrayList<AvailableBalancePendingBalances>();

  /**
   * This balance means the maximum expendable amount at the moment of retrieval. It\\'s represented by the current available balance monetary amount and the urrency related to the current available balance.
   **/
  public AvailableBalance currentBalances(List<AvailableBalanceCurrentBalances> currentBalances) {
    this.currentBalances = currentBalances;
    return this;
  }

  

  @JsonProperty("currentBalances")
  public List<AvailableBalanceCurrentBalances> getCurrentBalances() {
    return currentBalances;
  }
  public void setCurrentBalances(List<AvailableBalanceCurrentBalances> currentBalances) {
    this.currentBalances = currentBalances;
  }

  /**
   * This balance is the maximum expendable amount calculated at the end of the last business day. It\\'s represented by the posted available balance monetary amount and the currency related to the posted available balance.
   **/
  public AvailableBalance postedBalances(List<AvailableBalancePostedBalances> postedBalances) {
    this.postedBalances = postedBalances;
    return this;
  }

  

  @JsonProperty("postedBalances")
  public List<AvailableBalancePostedBalances> getPostedBalances() {
    return postedBalances;
  }
  public void setPostedBalances(List<AvailableBalancePostedBalances> postedBalances) {
    this.postedBalances = postedBalances;
  }

  /**
   * This balance is the aggregated amount of all pending transactions. This amount is the result of substracting the postedBalance from the currentBalance. It\\'s represented by the pending available balance monetary amount and the currency related to the pending available balance.
   **/
  public AvailableBalance pendingBalances(List<AvailableBalancePendingBalances> pendingBalances) {
    this.pendingBalances = pendingBalances;
    return this;
  }

  

  @JsonProperty("pendingBalances")
  public List<AvailableBalancePendingBalances> getPendingBalances() {
    return pendingBalances;
  }
  public void setPendingBalances(List<AvailableBalancePendingBalances> pendingBalances) {
    this.pendingBalances = pendingBalances;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AvailableBalance availableBalance = (AvailableBalance) o;
    return Objects.equals(currentBalances, availableBalance.currentBalances) &&
        Objects.equals(postedBalances, availableBalance.postedBalances) &&
        Objects.equals(pendingBalances, availableBalance.pendingBalances);
  }

  @Override
  public int hashCode() {
    return Objects.hash(currentBalances, postedBalances, pendingBalances);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AvailableBalance {\n");
    
    sb.append("    currentBalances: ").append(toIndentedString(currentBalances)).append("\n");
    sb.append("    postedBalances: ").append(toIndentedString(postedBalances)).append("\n");
    sb.append("    pendingBalances: ").append(toIndentedString(pendingBalances)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
