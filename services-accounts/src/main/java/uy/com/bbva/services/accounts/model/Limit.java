package uy.com.bbva.services.accounts.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

public class Limit   {
  
  private String limitId = null;
  private String name = null;
  private List<LimitAmountLimits> amountLimits = new ArrayList<LimitAmountLimits>();
  private String offerId = null;
  private Date operationDate = null;
  private String operationNumber = null;

  /**
   * Limit identifier.
   **/
  public Limit limitId(String limitId) {
    this.limitId = limitId;
    return this;
  }

  

  @JsonProperty("limitId")
  public String getLimitId() {
    return limitId;
  }
  public void setLimitId(String limitId) {
    this.limitId = limitId;
  }

  /**
   * Limit localized description.
   **/
  public Limit name(String name) {
    this.name = name;
    return this;
  }

  

  @JsonProperty("name")
  public String getName() {
    return name;
  }
  public void setName(String name) {
    this.name = name;
  }

  /**
   * Monetary restriction related to the current limit. This amount may be provided in several currencies (depending on the country).
   **/
  public Limit amountLimits(List<LimitAmountLimits> amountLimits) {
    this.amountLimits = amountLimits;
    return this;
  }

  

  @JsonProperty("amountLimits")
  public List<LimitAmountLimits> getAmountLimits() {
    return amountLimits;
  }
  public void setAmountLimits(List<LimitAmountLimits> amountLimits) {
    this.amountLimits = amountLimits;
  }

  /**
   * Offer identifier.
   **/
  public Limit offerId(String offerId) {
    this.offerId = offerId;
    return this;
  }

  

  @JsonProperty("offerId")
  public String getOfferId() {
    return offerId;
  }
  public void setOfferId(String offerId) {
    this.offerId = offerId;
  }

  /**
   * String based on ISO-8601 date format for specifying the operation date.
   **/
  public Limit operationDate(Date operationDate) {
    this.operationDate = operationDate;
    return this;
  }

  

  @JsonProperty("operationDate")
  public Date getOperationDate() {
    return operationDate;
  }
  public void setOperationDate(Date operationDate) {
    this.operationDate = operationDate;
  }

  /**
   * Reference number to the operation performed.
   **/
  public Limit operationNumber(String operationNumber) {
    this.operationNumber = operationNumber;
    return this;
  }

  

  @JsonProperty("operationNumber")
  public String getOperationNumber() {
    return operationNumber;
  }
  public void setOperationNumber(String operationNumber) {
    this.operationNumber = operationNumber;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Limit limit = (Limit) o;
    return Objects.equals(limitId, limit.limitId) &&
        Objects.equals(name, limit.name) &&
        Objects.equals(amountLimits, limit.amountLimits) &&
        Objects.equals(offerId, limit.offerId) &&
        Objects.equals(operationDate, limit.operationDate) &&
        Objects.equals(operationNumber, limit.operationNumber);
  }

  @Override
  public int hashCode() {
    return Objects.hash(limitId, name, amountLimits, offerId, operationDate, operationNumber);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Limit {\n");
    
    sb.append("    limitId: ").append(toIndentedString(limitId)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    amountLimits: ").append(toIndentedString(amountLimits)).append("\n");
    sb.append("    offerId: ").append(toIndentedString(offerId)).append("\n");
    sb.append("    operationDate: ").append(toIndentedString(operationDate)).append("\n");
    sb.append("    operationNumber: ").append(toIndentedString(operationNumber)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
