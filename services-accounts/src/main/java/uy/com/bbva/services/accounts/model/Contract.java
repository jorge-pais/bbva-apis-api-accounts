package uy.com.bbva.services.accounts.model;

import com.fasterxml.jackson.annotation.JsonProperty;



import java.util.Objects;


/**
 * Customer\\&#x60;s contract that is being queried. When customer is performing an account\\&#x60;s transactions query in this attribute must be setted the information about that account. It occurs the same for other products which transaction can be queried (i.e. cards, loans, etc...)
 **/



public class Contract   {
  
  private String id = null;
  private String number = null;
  private ContractNumberType numberType = null;
  private ContractProduct product = null;
  private String alias = null;

  /**
   * Identifier associated to the contract.
   **/
  public Contract id(String id) {
    this.id = id;
    return this;
  }

  

  @JsonProperty("id")
  public String getId() {
    return id;
  }
  public void setId(String id) {
    this.id = id;
  }

  /**
   * Contract number.
   **/
  public Contract number(String number) {
    this.number = number;
    return this;
  }

  

  @JsonProperty("number")
  public String getNumber() {
    return number;
  }
  public void setNumber(String number) {
    this.number = number;
  }

  /**
   * Contract number type based on the financial product type.
   **/
  public Contract numberType(ContractNumberType numberType) {
    this.numberType = numberType;
    return this;
  }

  

  @JsonProperty("numberType")
  public ContractNumberType getNumberType() {
    return numberType;
  }
  public void setNumberType(ContractNumberType numberType) {
    this.numberType = numberType;
  }

  /**
   * Financial product associated to the contract.
   **/
  public Contract product(ContractProduct product) {
    this.product = product;
    return this;
  }

  

  @JsonProperty("product")
  public ContractProduct getProduct() {
    return product;
  }
  public void setProduct(ContractProduct product) {
    this.product = product;
  }

  /**
   * Alias assigned to the contract customized by the user.
   **/
  public Contract alias(String alias) {
    this.alias = alias;
    return this;
  }

  

  @JsonProperty("alias")
  public String getAlias() {
    return alias;
  }
  public void setAlias(String alias) {
    this.alias = alias;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Contract contract = (Contract) o;
    return Objects.equals(id, contract.id) &&
        Objects.equals(number, contract.number) &&
        Objects.equals(numberType, contract.numberType) &&
        Objects.equals(product, contract.product) &&
        Objects.equals(alias, contract.alias);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, number, numberType, product, alias);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Contract {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    number: ").append(toIndentedString(number)).append("\n");
    sb.append("    numberType: ").append(toIndentedString(numberType)).append("\n");
    sb.append("    product: ").append(toIndentedString(product)).append("\n");
    sb.append("    alias: ").append(toIndentedString(alias)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
