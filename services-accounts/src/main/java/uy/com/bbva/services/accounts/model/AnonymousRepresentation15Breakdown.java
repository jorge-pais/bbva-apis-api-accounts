package uy.com.bbva.services.accounts.model;

import com.fasterxml.jackson.annotation.JsonProperty;


import java.util.Objects;



public class AnonymousRepresentation15Breakdown   {
  
  private String currency = null;
  private Double available = null;
  private Double posted = null;
  private Double pendingTransactions = null;
  private Double availableCredit = null;

  /**
   * Currency in which the balances obtained are expressed.
   **/
  public AnonymousRepresentation15Breakdown currency(String currency) {
    this.currency = currency;
    return this;
  }

  

  @JsonProperty("currency")
  public String getCurrency() {
    return currency;
  }
  public void setCurrency(String currency) {
    this.currency = currency;
  }

  /**
   * Amount in a checking or savings account that is immediately available for use. Also called available funds.
   **/
  public AnonymousRepresentation15Breakdown available(Double available) {
    this.available = available;
    return this;
  }

  

  @JsonProperty("available")
  public Double getAvailable() {
    return available;
  }
  public void setAvailable(Double available) {
    this.available = available;
  }

  /**
   * The Posted Balance shows the balance in the account after all transactions have “posted” during the account’s last processing date (normally through the previous business day).
   **/
  public AnonymousRepresentation15Breakdown posted(Double posted) {
    this.posted = posted;
    return this;
  }

  

  @JsonProperty("posted")
  public Double getPosted() {
    return posted;
  }
  public void setPosted(Double posted) {
    this.posted = posted;
  }

  /**
   * This balance is the aggregated amount of all pending transactions. This amount is the result of subtracting posted to available balance. This balance may be provided in several currencies (depending on the country). It\\'s represented by the pending disposed balance monetary amount and the currency related to the pending disposed balance.
   **/
  public AnonymousRepresentation15Breakdown pendingTransactions(Double pendingTransactions) {
    this.pendingTransactions = pendingTransactions;
    return this;
  }

  

  @JsonProperty("pendingTransactions")
  public Double getPendingTransactions() {
    return pendingTransactions;
  }
  public void setPendingTransactions(Double pendingTransactions) {
    this.pendingTransactions = pendingTransactions;
  }

  /**
   * The unused portion of an open line of credit, such as a credit account. Available credit is the difference between the amount of the credit line or limit, and the amount that has already been borrowed.
   **/
  public AnonymousRepresentation15Breakdown availableCredit(Double availableCredit) {
    this.availableCredit = availableCredit;
    return this;
  }

  

  @JsonProperty("availableCredit")
  public Double getAvailableCredit() {
    return availableCredit;
  }
  public void setAvailableCredit(Double availableCredit) {
    this.availableCredit = availableCredit;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AnonymousRepresentation15Breakdown anonymousRepresentation15Breakdown = (AnonymousRepresentation15Breakdown) o;
    return Objects.equals(currency, anonymousRepresentation15Breakdown.currency) &&
        Objects.equals(available, anonymousRepresentation15Breakdown.available) &&
        Objects.equals(posted, anonymousRepresentation15Breakdown.posted) &&
        Objects.equals(pendingTransactions, anonymousRepresentation15Breakdown.pendingTransactions) &&
        Objects.equals(availableCredit, anonymousRepresentation15Breakdown.availableCredit);
  }

  @Override
  public int hashCode() {
    return Objects.hash(currency, available, posted, pendingTransactions, availableCredit);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AnonymousRepresentation15Breakdown {\n");
    
    sb.append("    currency: ").append(toIndentedString(currency)).append("\n");
    sb.append("    available: ").append(toIndentedString(available)).append("\n");
    sb.append("    posted: ").append(toIndentedString(posted)).append("\n");
    sb.append("    pendingTransactions: ").append(toIndentedString(pendingTransactions)).append("\n");
    sb.append("    availableCredit: ").append(toIndentedString(availableCredit)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
