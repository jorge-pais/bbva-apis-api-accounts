package uy.com.bbva.services.accounts.model;

import com.fasterxml.jackson.annotation.JsonProperty;


import java.util.Date;
import java.util.Objects;



public class Checkbook   {
  
  private String id = null;
  private CheckbookType checkbookType = null;
  private CheckbookFormat format = null;
  private Date requestDate = null;
  private CheckbookStatus status = null;
  private String firstCheckId = null;
  private String lastCheckId = null;
  private CheckbookBranch branch = null;
  private String verificationCode = null;

  /**
   * Checkbook identifier.
   **/
  public Checkbook id(String id) {
    this.id = id;
    return this;
  }

  

  @JsonProperty("id")
  public String getId() {
    return id;
  }
  public void setId(String id) {
    this.id = id;
  }

  /**
   * Checkbook type.
   **/
  public Checkbook checkbookType(CheckbookType checkbookType) {
    this.checkbookType = checkbookType;
    return this;
  }

  

  @JsonProperty("checkbookType")
  public CheckbookType getCheckbookType() {
    return checkbookType;
  }
  public void setCheckbookType(CheckbookType checkbookType) {
    this.checkbookType = checkbookType;
  }

  /**
   * Printed format.
   **/
  public Checkbook format(CheckbookFormat format) {
    this.format = format;
    return this;
  }

  

  @JsonProperty("format")
  public CheckbookFormat getFormat() {
    return format;
  }
  public void setFormat(CheckbookFormat format) {
    this.format = format;
  }

  /**
   * String based on ISO-8601 date format for specifying when the user requested the checkbook.
   **/
  public Checkbook requestDate(Date requestDate) {
    this.requestDate = requestDate;
    return this;
  }

  

  @JsonProperty("requestDate")
  public Date getRequestDate() {
    return requestDate;
  }
  public void setRequestDate(Date requestDate) {
    this.requestDate = requestDate;
  }

  /**
   * Checkbook status.
   **/
  public Checkbook status(CheckbookStatus status) {
    this.status = status;
    return this;
  }

  

  @JsonProperty("status")
  public CheckbookStatus getStatus() {
    return status;
  }
  public void setStatus(CheckbookStatus status) {
    this.status = status;
  }

  /**
   * Numeration of the checkbook\\'s first check.
   **/
  public Checkbook firstCheckId(String firstCheckId) {
    this.firstCheckId = firstCheckId;
    return this;
  }

  

  @JsonProperty("firstCheckId")
  public String getFirstCheckId() {
    return firstCheckId;
  }
  public void setFirstCheckId(String firstCheckId) {
    this.firstCheckId = firstCheckId;
  }

  /**
   * Numeration of the checkbook\\'s last check.
   **/
  public Checkbook lastCheckId(String lastCheckId) {
    this.lastCheckId = lastCheckId;
    return this;
  }

  

  @JsonProperty("lastCheckId")
  public String getLastCheckId() {
    return lastCheckId;
  }
  public void setLastCheckId(String lastCheckId) {
    this.lastCheckId = lastCheckId;
  }

  /**
   * Office of the bank where the customer will pick up the checkbook.
   **/
  public Checkbook branch(CheckbookBranch branch) {
    this.branch = branch;
    return this;
  }

  

  @JsonProperty("branch")
  public CheckbookBranch getBranch() {
    return branch;
  }
  public void setBranch(CheckbookBranch branch) {
    this.branch = branch;
  }

  /**
   * Special  digit of checkbook.
   **/
  public Checkbook verificationCode(String verificationCode) {
    this.verificationCode = verificationCode;
    return this;
  }

  

  @JsonProperty("verificationCode")
  public String getVerificationCode() {
    return verificationCode;
  }
  public void setVerificationCode(String verificationCode) {
    this.verificationCode = verificationCode;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Checkbook checkbook = (Checkbook) o;
    return Objects.equals(id, checkbook.id) &&
        Objects.equals(checkbookType, checkbook.checkbookType) &&
        Objects.equals(format, checkbook.format) &&
        Objects.equals(requestDate, checkbook.requestDate) &&
        Objects.equals(status, checkbook.status) &&
        Objects.equals(firstCheckId, checkbook.firstCheckId) &&
        Objects.equals(lastCheckId, checkbook.lastCheckId) &&
        Objects.equals(branch, checkbook.branch) &&
        Objects.equals(verificationCode, checkbook.verificationCode);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, checkbookType, format, requestDate, status, firstCheckId, lastCheckId, branch, verificationCode);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Checkbook {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    checkbookType: ").append(toIndentedString(checkbookType)).append("\n");
    sb.append("    format: ").append(toIndentedString(format)).append("\n");
    sb.append("    requestDate: ").append(toIndentedString(requestDate)).append("\n");
    sb.append("    status: ").append(toIndentedString(status)).append("\n");
    sb.append("    firstCheckId: ").append(toIndentedString(firstCheckId)).append("\n");
    sb.append("    lastCheckId: ").append(toIndentedString(lastCheckId)).append("\n");
    sb.append("    branch: ").append(toIndentedString(branch)).append("\n");
    sb.append("    verificationCode: ").append(toIndentedString(verificationCode)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
