package uy.com.bbva.services.accounts.model;

import com.fasterxml.jackson.annotation.JsonProperty;


import java.util.Objects;



public class PaginationLinks   {
  
  private String first = null;
  private String last = null;
  private String previous = null;
  private String next = null;

  /**
   * URI to the first page. It will be the URI to the list without paging element.
   **/
  public PaginationLinks first(String first) {
    this.first = first;
    return this;
  }

  

  @JsonProperty("first")
  public String getFirst() {
    return first;
  }
  public void setFirst(String first) {
    this.first = first;
  }

  /**
   * URI to the last page based on the current pagination configuration.
   **/
  public PaginationLinks last(String last) {
    this.last = last;
    return this;
  }

  

  @JsonProperty("last")
  public String getLast() {
    return last;
  }
  public void setLast(String last) {
    this.last = last;
  }

  /**
   * URI to the previous page.
   **/
  public PaginationLinks previous(String previous) {
    this.previous = previous;
    return this;
  }

  

  @JsonProperty("previous")
  public String getPrevious() {
    return previous;
  }
  public void setPrevious(String previous) {
    this.previous = previous;
  }

  /**
   * URI to the next page.
   **/
  public PaginationLinks next(String next) {
    this.next = next;
    return this;
  }

  

  @JsonProperty("next")
  public String getNext() {
    return next;
  }
  public void setNext(String next) {
    this.next = next;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PaginationLinks paginationLinks = (PaginationLinks) o;
    return Objects.equals(first, paginationLinks.first) &&
        Objects.equals(last, paginationLinks.last) &&
        Objects.equals(previous, paginationLinks.previous) &&
        Objects.equals(next, paginationLinks.next);
  }

  @Override
  public int hashCode() {
    return Objects.hash(first, last, previous, next);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PaginationLinks {\n");
    
    sb.append("    first: ").append(toIndentedString(first)).append("\n");
    sb.append("    last: ").append(toIndentedString(last)).append("\n");
    sb.append("    previous: ").append(toIndentedString(previous)).append("\n");
    sb.append("    next: ").append(toIndentedString(next)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
