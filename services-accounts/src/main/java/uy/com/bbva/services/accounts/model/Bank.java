package uy.com.bbva.services.accounts.model;

import com.fasterxml.jackson.annotation.JsonProperty;



import java.util.Objects;


/**
 * Entity associated to the account.
 **/



public class Bank   {
  
  private String bankId = null;
  private String name = null;
  private Branch branch = null;

  /**
   * Unique bank identifier.
   **/
  public Bank bankId(String bankId) {
    this.bankId = bankId;
    return this;
  }

  

  @JsonProperty("bankId")
  public String getBankId() {
    return bankId;
  }
  public void setBankId(String bankId) {
    this.bankId = bankId;
  }

  /**
   * Bank name.
   **/
  public Bank name(String name) {
    this.name = name;
    return this;
  }

  

  @JsonProperty("name")
  public String getName() {
    return name;
  }
  public void setName(String name) {
    this.name = name;
  }

  /**
   * Office of the bank associated with the customer.
   **/
  public Bank branch(Branch branch) {
    this.branch = branch;
    return this;
  }

  

  @JsonProperty("branch")
  public Branch getBranch() {
    return branch;
  }
  public void setBranch(Branch branch) {
    this.branch = branch;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Bank bank = (Bank) o;
    return Objects.equals(bankId, bank.bankId) &&
        Objects.equals(name, bank.name) &&
        Objects.equals(branch, bank.branch);
  }

  @Override
  public int hashCode() {
    return Objects.hash(bankId, name, branch);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Bank {\n");
    
    sb.append("    bankId: ").append(toIndentedString(bankId)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    branch: ").append(toIndentedString(branch)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
