
package uy.com.bbva.services.accounts.model;

import com.fasterxml.jackson.annotation.JsonProperty;



import java.util.Objects;


/**
 * Associated image to the account.
 **/



public class Image   {
  
  private String id = null;
  private String name = null;
  private String url = null;

  /**
   * Identifier of the image.
   **/
  public Image id(String id) {
    this.id = id;
    return this;
  }

  

  @JsonProperty("id")
  public String getId() {
    return id;
  }
  public void setId(String id) {
    this.id = id;
  }

  /**
   * Name of the image.
   **/
  public Image name(String name) {
    this.name = name;
    return this;
  }

  

  @JsonProperty("name")
  public String getName() {
    return name;
  }
  public void setName(String name) {
    this.name = name;
  }

  /**
   * URL to reach the image.
   **/
  public Image url(String url) {
    this.url = url;
    return this;
  }

  

  @JsonProperty("url")
  public String getUrl() {
    return url;
  }
  public void setUrl(String url) {
    this.url = url;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Image image = (Image) o;
    return Objects.equals(id, image.id) &&
        Objects.equals(name, image.name) &&
        Objects.equals(url, image.url);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, name, url);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Image {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    url: ").append(toIndentedString(url)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}