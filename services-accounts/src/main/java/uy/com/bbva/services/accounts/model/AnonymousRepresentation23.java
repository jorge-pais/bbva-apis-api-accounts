package uy.com.bbva.services.accounts.model;

import com.fasterxml.jackson.annotation.JsonProperty;


import java.util.ArrayList;
import java.util.List;
import java.util.Objects;



public class AnonymousRepresentation23   {
  
  private List<AnonymousRepresentation6> data = new ArrayList<AnonymousRepresentation6>();

  /**
   **/
  public AnonymousRepresentation23 data(List<AnonymousRepresentation6> data) {
    this.data = data;
    return this;
  }

  

  @JsonProperty("data")
  public List<AnonymousRepresentation6> getData() {
    return data;
  }
  public void setData(List<AnonymousRepresentation6> data) {
    this.data = data;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AnonymousRepresentation23 anonymousRepresentation23 = (AnonymousRepresentation23) o;
    return Objects.equals(data, anonymousRepresentation23.data);
  }

  @Override
  public int hashCode() {
    return Objects.hash(data);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AnonymousRepresentation23 {\n");
    
    sb.append("    data: ").append(toIndentedString(data)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
