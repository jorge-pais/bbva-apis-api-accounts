package uy.com.bbva.services.accounts.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;


public class Account   {
  
  private String accountId = null;
  private String number = null;
  private AccountNumberType numberType = null;
  private List<AccountFormat> formats = null;
  private List<CustomizedFormat> customizedFormats = null;
  private AccountFamily accountFamily = null;
  private AccountType accountType = null;
  private ModelPackage _package = null;
  private Joint joint = null;
  private Title title = null;
  private Bank bank = null;
  private String alias = null;
  private Address address = null;
  private Date openingDate = null;
  private Date cutOffDate = null;
  private Status status = null;
  private Image image = null;
  private List<AccountCurrencies> currencies = new ArrayList<AccountCurrencies>() ;
  private List<AccountGrantedCredits> grantedCredits ;
  private AvailableBalance availableBalance = null;
  private DisposedBalance disposedBalance = null;
  private List<Participant> participants ;
  private List<Condition> conditions ;
  private String offerId = null;
  private List<RelatedContract> relatedContracts ;
  private AccountMeta meta = null;

  /**
   * Account identifier.
   **/
  public Account accountId(String accountId) {
    this.accountId = accountId;
    return this;
  }

  
  @JsonProperty("accountId")
  public String getAccountId() {
    return accountId;
  }
  public void setAccountId(String accountId) {
    this.accountId = accountId;
  }

  /**
   * Account number. This number consists in an alpha-numerical sequence to identify an account.
   **/
  public Account number(String number) {
    this.number = number;
    return this;
  }

  
  @JsonProperty("number")
  public String getNumber() {
    return number;
  }
  public void setNumber(String number) {
    this.number = number;
  }

  /**
   * Type of account number. This type identifies the universal standard of an account number.
   **/
  public Account numberType(AccountNumberType numberType) {
    this.numberType = numberType;
    return this;
  }

  
  @JsonProperty("numberType")
  public AccountNumberType getNumberType() {
    return numberType;
  }
  public void setNumberType(AccountNumberType numberType) {
    this.numberType = numberType;
  }

  /**
   * Other formats associated to the account used to identify it.
   **/
  public Account formats(List<AccountFormat> formats) {
    this.formats = formats;
    return this;
  }

  
  @JsonProperty("formats")
  public List<AccountFormat> getFormats() {
    return formats;
  }
  public void setFormats(List<AccountFormat> formats) {
    this.formats = formats;
  }

  /**
   * User custom formats associated to the account used to identify it.
   **/
  public Account customizedFormats(List<CustomizedFormat> customizedFormats) {
    this.customizedFormats = customizedFormats;
    return this;
  }

  
  @JsonProperty("customizedFormats")
  public List<CustomizedFormat> getCustomizedFormats() {
    return customizedFormats;
  }
  public void setCustomizedFormats(List<CustomizedFormat> customizedFormats) {
    this.customizedFormats = customizedFormats;
  }

  /**
   * An account family is a classification of an account based on certain distinctive attributes of that financial product
   **/
  public Account accountFamily(AccountFamily accountFamily) {
    this.accountFamily = accountFamily;
    return this;
  }

  
  @JsonProperty("accountFamily")
  public AccountFamily getAccountFamily() {
    return accountFamily;
  }
  public void setAccountFamily(AccountFamily accountFamily) {
    this.accountFamily = accountFamily;
  }

  /**
   * Unique identifier of the product in open alphanumerical format. Each geography define that identifier freely
   **/
  public Account accountType(AccountType accountType) {
    this.accountType = accountType;
    return this;
  }

  
  @JsonProperty("accountType")
  public AccountType getAccountType() {
    return accountType;
  }
  public void setAccountType(AccountType accountType) {
    this.accountType = accountType;
  }

  /**
   * It identifies the package the card belongs to. A package groups various products the client has. For example: \"Libreton Full\", that groups credit card, savings account and checking account.
   **/
  public Account _package(ModelPackage _package) {
    this._package = _package;
    return this;
  }

  
  @JsonProperty("package")
  public ModelPackage getPackage() {
    return _package;
  }
  public void setPackage(ModelPackage _package) {
    this._package = _package;
  }

  /**
   * Types of joint account.
   **/
  public Account joint(Joint joint) {
    this.joint = joint;
    return this;
  }

  
  @JsonProperty("joint")
  public Joint getJoint() {
    return joint;
  }
  public void setJoint(Joint joint) {
    this.joint = joint;
  }

  /**
   * Title of the financial product. This is the commercial name by which the user knows this product.
   **/
  public Account title(Title title) {
    this.title = title;
    return this;
  }

  
  @JsonProperty("title")
  public Title getTitle() {
    return title;
  }
  public void setTitle(Title title) {
    this.title = title;
  }

  /**
   * Entity associated to the account.
   **/
  public Account bank(Bank bank) {
    this.bank = bank;
    return this;
  }

  
  @JsonProperty("bank")
  public Bank getBank() {
    return bank;
  }
  public void setBank(Bank bank) {
    this.bank = bank;
  }

  /**
   * User-customizable alias assigned to the account to name the account the way the user likes.
   **/
  public Account alias(String alias) {
    this.alias = alias;
    return this;
  }

  
  @JsonProperty("alias")
  public String getAlias() {
    return alias;
  }
  public void setAlias(String alias) {
    this.alias = alias;
  }

  /**
   * User\\'s address associated to the account.
   **/
  public Account address(Address address) {
    this.address = address;
    return this;
  }

  
  @JsonProperty("address")
  public Address getAddress() {
    return address;
  }
  public void setAddress(Address address) {
    this.address = address;
  }

  /**
   * String based on ISO-8601 date format for specifying the account creation date.
   **/
  public Account openingDate(Date openingDate) {
    this.openingDate = openingDate;
    return this;
  }

  
  @JsonProperty("openingDate")
  public Date getOpeningDate() {
    return openingDate;
  }
  public void setOpeningDate(Date openingDate) {
    this.openingDate = openingDate;
  }

  /**
   * String based on ISO-8601 date format for specifying the date when the current credit account period ends and the next one starts. All the transactions performed until this date will be added to the next payment term.
   **/
  public Account cutOffDate(Date cutOffDate) {
    this.cutOffDate = cutOffDate;
    return this;
  }

  
  @JsonProperty("cutOffDate")
  public Date getCutOffDate() {
    return cutOffDate;
  }
  public void setCutOffDate(Date cutOffDate) {
    this.cutOffDate = cutOffDate;
  }

  /**
   * Current status of the account.
   **/
  public Account status(Status status) {
    this.status = status;
    return this;
  }

  
  @JsonProperty("status")
  public Status getStatus() {
    return status;
  }
  public void setStatus(Status status) {
    this.status = status;
  }

  /**
   * Image associated to the account.
   **/
  public Account image(Image image) {
    this.image = image;
    return this;
  }

  
  @JsonProperty("image")
  public Image getImage() {
    return image;
  }
  public void setImage(Image image) {
    this.image = image;
  }

  /**
   * Currencies related to the account. Monetary amounts related to the account may be provided in one or many of these currencies.
   **/
  public Account currencies(List<AccountCurrencies> currencies) {
    this.currencies = currencies;
    return this;
  }

  
  @JsonProperty("currencies")
  public List<AccountCurrencies> getCurrencies() {
    return currencies;
  }
  public void setCurrencies(List<AccountCurrencies> currencies) {
    this.currencies = currencies;
  }

  /**
   * Granted credit. This amount may be provided in several currencies (depending on the country). This attribute is mandatory for credit accounts. It\\'s represented by the granted credit monetary amount and the currency related to the granted credit amount.
   **/
  public Account grantedCredits(List<AccountGrantedCredits> grantedCredits) {
    this.grantedCredits = grantedCredits;
    return this;
  }

  
  @JsonProperty("grantedCredits")
  public List<AccountGrantedCredits> getGrantedCredits() {
    return grantedCredits;
  }
  public void setGrantedCredits(List<AccountGrantedCredits> grantedCredits) {
    this.grantedCredits = grantedCredits;
  }

  /**
   * This attribute provides the account balance. This balance may be provided in several currencies (depending on the country).
   **/
  public Account availableBalance(AvailableBalance availableBalance) {
    this.availableBalance = availableBalance;
    return this;
  }

  
  @JsonProperty("availableBalance")
  public AvailableBalance getAvailableBalance() {
    return availableBalance;
  }
  public void setAvailableBalance(AvailableBalance availableBalance) {
    this.availableBalance = availableBalance;
  }

  /**
   * This balance is the total spent amount from granted credit. This attribute is mandatory for credit accounts.
   **/
  public Account disposedBalance(DisposedBalance disposedBalance) {
    this.disposedBalance = disposedBalance;
    return this;
  }

  
  @JsonProperty("disposedBalance")
  public DisposedBalance getDisposedBalance() {
    return disposedBalance;
  }
  public void setDisposedBalance(DisposedBalance disposedBalance) {
    this.disposedBalance = disposedBalance;
  }

  /**
   * Account participants. An account participant is any allowed person by the account holder who has permissions over an account to make operations.
   **/
  public Account participants(List<Participant> participants) {
    this.participants = participants;
    return this;
  }

  
  @JsonProperty("participants")
  public List<Participant> getParticipants() {
    return participants;
  }
  public void setParticipants(List<Participant> participants) {
    this.participants = participants;
  }

  /**
   * Account conditions.
   **/
  public Account conditions(List<Condition> conditions) {
    this.conditions = conditions;
    return this;
  }

  
  @JsonProperty("conditions")
  public List<Condition> getConditions() {
    return conditions;
  }
  public void setConditions(List<Condition> conditions) {
    this.conditions = conditions;
  }

  /**
   * This attribute refers to the identifier of an offer within the Offers API, this way it\\'ll be possible select a campaign to offer new products or upgrade the existing.
   **/
  public Account offerId(String offerId) {
    this.offerId = offerId;
    return this;
  }

  
  @JsonProperty("offerId")
  public String getOfferId() {
    return offerId;
  }
  public void setOfferId(String offerId) {
    this.offerId = offerId;
  }

  /**
   * An account can have several related contracts, each one associated with a different type of relationship.
   **/
  public Account relatedContracts(List<RelatedContract> relatedContracts) {
    this.relatedContracts = relatedContracts;
    return this;
  }

  
  @JsonProperty("relatedContracts")
  public List<RelatedContract> getRelatedContracts() {
    return relatedContracts;
  }
  public void setRelatedContracts(List<RelatedContract> relatedContracts) {
    this.relatedContracts = relatedContracts;
  }

  /**
   * Placeholder for countries specific account attributes.
   **/
  public Account meta(AccountMeta meta) {
    this.meta = meta;
    return this;
  }

  
  @JsonProperty("meta")
  public AccountMeta getMeta() {
    return meta;
  }
  public void setMeta(AccountMeta meta) {
    this.meta = meta;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Account account = (Account) o;
    return Objects.equals(accountId, account.accountId) &&
        Objects.equals(number, account.number) &&
        Objects.equals(numberType, account.numberType) &&
        Objects.equals(formats, account.formats) &&
        Objects.equals(customizedFormats, account.customizedFormats) &&
        Objects.equals(accountFamily, account.accountFamily) &&
        Objects.equals(accountType, account.accountType) &&
        Objects.equals(_package, account._package) &&
        Objects.equals(joint, account.joint) &&
        Objects.equals(title, account.title) &&
        Objects.equals(bank, account.bank) &&
        Objects.equals(alias, account.alias) &&
        Objects.equals(address, account.address) &&
        Objects.equals(openingDate, account.openingDate) &&
        Objects.equals(cutOffDate, account.cutOffDate) &&
        Objects.equals(status, account.status) &&
        Objects.equals(image, account.image) &&
        Objects.equals(currencies, account.currencies) &&
        Objects.equals(grantedCredits, account.grantedCredits) &&
        Objects.equals(availableBalance, account.availableBalance) &&
        Objects.equals(disposedBalance, account.disposedBalance) &&
        Objects.equals(participants, account.participants) &&
        Objects.equals(conditions, account.conditions) &&
        Objects.equals(offerId, account.offerId) &&
        Objects.equals(relatedContracts, account.relatedContracts) &&
        Objects.equals(meta, account.meta);
  }

  @Override
  public int hashCode() {
    return Objects.hash(accountId, number, numberType, formats, customizedFormats, accountFamily, accountType, _package, joint, title, bank, alias, address, openingDate, cutOffDate, status, image, currencies, grantedCredits, availableBalance, disposedBalance, participants, conditions, offerId, relatedContracts, meta);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Account {\n");
    
    sb.append("    accountId: ").append(toIndentedString(accountId)).append("\n");
    sb.append("    number: ").append(toIndentedString(number)).append("\n");
    sb.append("    numberType: ").append(toIndentedString(numberType)).append("\n");
    sb.append("    formats: ").append(toIndentedString(formats)).append("\n");
    sb.append("    customizedFormats: ").append(toIndentedString(customizedFormats)).append("\n");
    sb.append("    accountFamily: ").append(toIndentedString(accountFamily)).append("\n");
    sb.append("    accountType: ").append(toIndentedString(accountType)).append("\n");
    sb.append("    _package: ").append(toIndentedString(_package)).append("\n");
    sb.append("    joint: ").append(toIndentedString(joint)).append("\n");
    sb.append("    title: ").append(toIndentedString(title)).append("\n");
    sb.append("    bank: ").append(toIndentedString(bank)).append("\n");
    sb.append("    alias: ").append(toIndentedString(alias)).append("\n");
    sb.append("    address: ").append(toIndentedString(address)).append("\n");
    sb.append("    openingDate: ").append(toIndentedString(openingDate)).append("\n");
    sb.append("    cutOffDate: ").append(toIndentedString(cutOffDate)).append("\n");
    sb.append("    status: ").append(toIndentedString(status)).append("\n");
    sb.append("    image: ").append(toIndentedString(image)).append("\n");
    sb.append("    currencies: ").append(toIndentedString(currencies)).append("\n");
    sb.append("    grantedCredits: ").append(toIndentedString(grantedCredits)).append("\n");
    sb.append("    availableBalance: ").append(toIndentedString(availableBalance)).append("\n");
    sb.append("    disposedBalance: ").append(toIndentedString(disposedBalance)).append("\n");
    sb.append("    participants: ").append(toIndentedString(participants)).append("\n");
    sb.append("    conditions: ").append(toIndentedString(conditions)).append("\n");
    sb.append("    offerId: ").append(toIndentedString(offerId)).append("\n");
    sb.append("    relatedContracts: ").append(toIndentedString(relatedContracts)).append("\n");
    sb.append("    meta: ").append(toIndentedString(meta)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
