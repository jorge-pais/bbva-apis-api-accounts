package uy.com.bbva.services.accounts.model;

import com.fasterxml.jackson.annotation.JsonProperty;


import java.util.Date;
import java.util.List;
import java.util.Objects;



public class Transaction   {
  
  private String transactionId = null;
  private ModelImport localAmount = null;
  private ModelImport originAmount = null;
  private TransactionMoneyFlow moneyFlow = null;
  private String concept = null;
  private TransactionType transactionType = null;
  private Date operationDate = null;
  private Date accountedDate = null;
  private Date valuationDate = null;
  private TransactionFinancingType financingType = null;
  private TransactionStatus status = null;
  private Contract contract = null;
  private List<String> tags = null;
  private TransactionCategory category = null;
  private TransactionAdditionalInformation additionalInformation = null;
  private TransactionDetail detail = null;

  /**
   * Unique identifier of the transaction.
   **/
  public Transaction transactionId(String transactionId) {
    this.transactionId = transactionId;
    return this;
  }

  

  @JsonProperty("transactionId")
  public String getTransactionId() {
    return transactionId;
  }
  public void setTransactionId(String transactionId) {
    this.transactionId = transactionId;
  }

  /**
   * Amount that is associated to the transaction on the local currency. This amount may not include any charges that the bank can carry with the operation.
   **/
  public Transaction localAmount(ModelImport localAmount) {
    this.localAmount = localAmount;
    return this;
  }

  

  @JsonProperty("localAmount")
  public ModelImport getLocalAmount() {
    return localAmount;
  }
  public void setLocalAmount(ModelImport localAmount) {
    this.localAmount = localAmount;
  }

  /**
   * Origin amount that is associated to the transaction. This attribute can only be informed when the transactions has been performed on a currency that is different from the contract\\`s original currency. For example, when a customer performs a purchase in USD using his card that is associated to an account opened in Euros, originAmount attribute will be fulfilled with the original price of the item purchased in UK. This amount may not include any charges that the bank can carry with the operation.
   **/
  public Transaction originAmount(ModelImport originAmount) {
    this.originAmount = originAmount;
    return this;
  }

  

  @JsonProperty("originAmount")
  public ModelImport getOriginAmount() {
    return originAmount;
  }
  public void setOriginAmount(ModelImport originAmount) {
    this.originAmount = originAmount;
  }

  /**
   * Indicates whether the operation is an addition or a substraction from the total balance that customer has in the cotract that is being querying.
   **/
  public Transaction moneyFlow(TransactionMoneyFlow moneyFlow) {
    this.moneyFlow = moneyFlow;
    return this;
  }

  

  @JsonProperty("moneyFlow")
  public TransactionMoneyFlow getMoneyFlow() {
    return moneyFlow;
  }
  public void setMoneyFlow(TransactionMoneyFlow moneyFlow) {
    this.moneyFlow = moneyFlow;
  }

  /**
   * Brief description of the operative that create the transaction. Backend must provide this information depending on the transaction\\`s type.
   **/
  public Transaction concept(String concept) {
    this.concept = concept;
    return this;
  }

  

  @JsonProperty("concept")
  public String getConcept() {
    return concept;
  }
  public void setConcept(String concept) {
    this.concept = concept;
  }

  /**
   * Transaction type. It talks about which kind of operative has created this transaction. DISCLAIMER: Nowsdays some geographies have no possibility of identifying all the types of transaction that are detailed below. This should not become an impediment that in the near future this inconvenience be fixed by the corresponding backends and allow the customers to know and filter those transactions according to their criteria.
   **/
  public Transaction transactionType(TransactionType transactionType) {
    this.transactionType = transactionType;
    return this;
  }

  

  @JsonProperty("transactionType")
  public TransactionType getTransactionType() {
    return transactionType;
  }
  public void setTransactionType(TransactionType transactionType) {
    this.transactionType = transactionType;
  }

  /**
   * String based on ISO-8601 date format for providing the last date when the operation was performed by the client.
   **/
  public Transaction operationDate(Date operationDate) {
    this.operationDate = operationDate;
    return this;
  }

  

  @JsonProperty("operationDate")
  public Date getOperationDate() {
    return operationDate;
  }
  public void setOperationDate(Date operationDate) {
    this.operationDate = operationDate;
  }

  /**
   * String based on ISO-8601 date format for providing the last date when the transaction was accounted.
   **/
  public Transaction accountedDate(Date accountedDate) {
    this.accountedDate = accountedDate;
    return this;
  }

  

  @JsonProperty("accountedDate")
  public Date getAccountedDate() {
    return accountedDate;
  }
  public void setAccountedDate(Date accountedDate) {
    this.accountedDate = accountedDate;
  }

  /**
   * String based on ISO-8601 date format, this value is used when a debit or credit is fully effective and begins to accrue interest. <br/> For example: <br/>  - In check income, to take into account the value date, it will be necessary to wait for the secure payment in the account of the entity. <br/>  - In transfer income, entities can not apply a date value higher than the day after the credit has been posted.
   **/
  public Transaction valuationDate(Date valuationDate) {
    this.valuationDate = valuationDate;
    return this;
  }

  

  @JsonProperty("valuationDate")
  public Date getValuationDate() {
    return valuationDate;
  }
  public void setValuationDate(Date valuationDate) {
    this.valuationDate = valuationDate;
  }

  /**
   * It represents the information about the possibility that certain operations to be financed within the conditions of the product to which they belong. For example, we can perform a purchase in an store that can be financed if the amount of the purchase is greater than 50,00 euros. The product and the transaction type can define which business rules are applied to know whether the operation can be financed or not.
   **/
  public Transaction financingType(TransactionFinancingType financingType) {
    this.financingType = financingType;
    return this;
  }

  

  @JsonProperty("financingType")
  public TransactionFinancingType getFinancingType() {
    return financingType;
  }
  public void setFinancingType(TransactionFinancingType financingType) {
    this.financingType = financingType;
  }

  /**
   * Transaction status.
   **/
  public Transaction status(TransactionStatus status) {
    this.status = status;
    return this;
  }

  

  @JsonProperty("status")
  public TransactionStatus getStatus() {
    return status;
  }
  public void setStatus(TransactionStatus status) {
    this.status = status;
  }

  /**
   * Customer\\`s contract that is being queried. When customer is performing an account\\`s transactions query in this attribute must be setted the information about that account. It occurs the same for other products which transaction can be queried (i.e. cards, loans, etc...)
   **/
  public Transaction contract(Contract contract) {
    this.contract = contract;
    return this;
  }

  

  @JsonProperty("contract")
  public Contract getContract() {
    return contract;
  }
  public void setContract(Contract contract) {
    this.contract = contract;
  }

  /**
   * List of tags that can label a transaction. This tags are customizable by the customers and can be used to help them when they try to locate a transaction.
   **/
  public Transaction tags(List<String> tags) {
    this.tags = tags;
    return this;
  }

  

  @JsonProperty("tags")
  public List<String> getTags() {
    return tags;
  }
  public void setTags(List<String> tags) {
    this.tags = tags;
  }

  /**
   * Represents the category that a transaction can fit.
   **/
  public Transaction category(TransactionCategory category) {
    this.category = category;
    return this;
  }

  

  @JsonProperty("category")
  public TransactionCategory getCategory() {
    return category;
  }
  public void setCategory(TransactionCategory category) {
    this.category = category;
  }

  /**
   * Additional information related to this transaction.
   **/
  public Transaction additionalInformation(TransactionAdditionalInformation additionalInformation) {
    this.additionalInformation = additionalInformation;
    return this;
  }

  

  @JsonProperty("additionalInformation")
  public TransactionAdditionalInformation getAdditionalInformation() {
    return additionalInformation;
  }
  public void setAdditionalInformation(TransactionAdditionalInformation additionalInformation) {
    this.additionalInformation = additionalInformation;
  }

  /**
   * Detailed information according to the transaction type.
   **/
  public Transaction detail(TransactionDetail detail) {
    this.detail = detail;
    return this;
  }

  

  @JsonProperty("detail")
  public TransactionDetail getDetail() {
    return detail;
  }
  public void setDetail(TransactionDetail detail) {
    this.detail = detail;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Transaction transaction = (Transaction) o;
    return Objects.equals(transactionId, transaction.transactionId) &&
        Objects.equals(localAmount, transaction.localAmount) &&
        Objects.equals(originAmount, transaction.originAmount) &&
        Objects.equals(moneyFlow, transaction.moneyFlow) &&
        Objects.equals(concept, transaction.concept) &&
        Objects.equals(transactionType, transaction.transactionType) &&
        Objects.equals(operationDate, transaction.operationDate) &&
        Objects.equals(accountedDate, transaction.accountedDate) &&
        Objects.equals(valuationDate, transaction.valuationDate) &&
        Objects.equals(financingType, transaction.financingType) &&
        Objects.equals(status, transaction.status) &&
        Objects.equals(contract, transaction.contract) &&
        Objects.equals(tags, transaction.tags) &&
        Objects.equals(category, transaction.category) &&
        Objects.equals(additionalInformation, transaction.additionalInformation) &&
        Objects.equals(detail, transaction.detail);
  }

  @Override
  public int hashCode() {
    return Objects.hash(transactionId, localAmount, originAmount, moneyFlow, concept, transactionType, operationDate, accountedDate, valuationDate, financingType, status, contract, tags, category, additionalInformation, detail);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Transaction {\n");
    
    sb.append("    transactionId: ").append(toIndentedString(transactionId)).append("\n");
    sb.append("    localAmount: ").append(toIndentedString(localAmount)).append("\n");
    sb.append("    originAmount: ").append(toIndentedString(originAmount)).append("\n");
    sb.append("    moneyFlow: ").append(toIndentedString(moneyFlow)).append("\n");
    sb.append("    concept: ").append(toIndentedString(concept)).append("\n");
    sb.append("    transactionType: ").append(toIndentedString(transactionType)).append("\n");
    sb.append("    operationDate: ").append(toIndentedString(operationDate)).append("\n");
    sb.append("    accountedDate: ").append(toIndentedString(accountedDate)).append("\n");
    sb.append("    valuationDate: ").append(toIndentedString(valuationDate)).append("\n");
    sb.append("    financingType: ").append(toIndentedString(financingType)).append("\n");
    sb.append("    status: ").append(toIndentedString(status)).append("\n");
    sb.append("    contract: ").append(toIndentedString(contract)).append("\n");
    sb.append("    tags: ").append(toIndentedString(tags)).append("\n");
    sb.append("    category: ").append(toIndentedString(category)).append("\n");
    sb.append("    additionalInformation: ").append(toIndentedString(additionalInformation)).append("\n");
    sb.append("    detail: ").append(toIndentedString(detail)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
