package uy.com.bbva.services.accounts.model;

import com.fasterxml.jackson.annotation.JsonProperty;


import java.util.ArrayList;
import java.util.List;
import java.util.Objects;



public class AnonymousRepresentation11   {
  
  private String limitId = null;
  private String name = null;
  private List<AnonymousRepresentation11AmountLimits> amountLimits = new ArrayList<AnonymousRepresentation11AmountLimits>();

  /**
   * Limit identifier.
   **/
  public AnonymousRepresentation11 limitId(String limitId) {
    this.limitId = limitId;
    return this;
  }

  

  @JsonProperty("limitId")
  public String getLimitId() {
    return limitId;
  }
  public void setLimitId(String limitId) {
    this.limitId = limitId;
  }

  /**
   * Limit localized description.
   **/
  public AnonymousRepresentation11 name(String name) {
    this.name = name;
    return this;
  }

  

  @JsonProperty("name")
  public String getName() {
    return name;
  }
  public void setName(String name) {
    this.name = name;
  }

  /**
   * Monetary restriction related to the current limit. This amount may be provided in several currencies (depending on the country).
   **/
  public AnonymousRepresentation11 amountLimits(List<AnonymousRepresentation11AmountLimits> amountLimits) {
    this.amountLimits = amountLimits;
    return this;
  }

  

  @JsonProperty("amountLimits")
  public List<AnonymousRepresentation11AmountLimits> getAmountLimits() {
    return amountLimits;
  }
  public void setAmountLimits(List<AnonymousRepresentation11AmountLimits> amountLimits) {
    this.amountLimits = amountLimits;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AnonymousRepresentation11 anonymousRepresentation11 = (AnonymousRepresentation11) o;
    return Objects.equals(limitId, anonymousRepresentation11.limitId) &&
        Objects.equals(name, anonymousRepresentation11.name) &&
        Objects.equals(amountLimits, anonymousRepresentation11.amountLimits);
  }

  @Override
  public int hashCode() {
    return Objects.hash(limitId, name, amountLimits);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AnonymousRepresentation11 {\n");
    
    sb.append("    limitId: ").append(toIndentedString(limitId)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    amountLimits: ").append(toIndentedString(amountLimits)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
