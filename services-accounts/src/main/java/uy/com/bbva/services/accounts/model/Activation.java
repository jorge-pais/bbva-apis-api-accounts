package uy.com.bbva.services.accounts.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;


public class Activation   {
  
  private String activationId = null;
  private String name = null;
  private Boolean isActive = false;
  private Date startDate = null;
  private Date endDate = null;
  private List<ActivationLimits> limits = new ArrayList<ActivationLimits>();

  /**
   * Operational activation identifier.
   **/
  public Activation activationId(String activationId) {
    this.activationId = activationId;
    return this;
  }

  
  @JsonProperty("activationId")
  public String getActivationId() {
    return activationId;
  }
  public void setActivationId(String activationId) {
    this.activationId = activationId;
  }

  /**
   * Operational activation description.
   **/
  public Activation name(String name) {
    this.name = name;
    return this;
  }

  
  @JsonProperty("name")
  public String getName() {
    return name;
  }
  public void setName(String name) {
    this.name = name;
  }

  /**
   * Indicates whether the activation is enabled.
   **/
  public Activation isActive(Boolean isActive) {
    this.isActive = isActive;
    return this;
  }

  
  @JsonProperty("isActive")
  public Boolean getIsActive() {
    return isActive;
  }
  public void setIsActive(Boolean isActive) {
    this.isActive = isActive;
  }

  /**
   * String based on ISO-8601 timestamp format for specifying when the activation became temporarily disabled.
   **/
  public Activation startDate(Date startDate) {
    this.startDate = startDate;
    return this;
  }

  
  @JsonProperty("startDate")
  public Date getStartDate() {
    return startDate;
  }
  public void setStartDate(Date startDate) {
    this.startDate = startDate;
  }

  /**
   * String based on ISO-8601 timestamp format for specifying when the activation becomes enabled after temporarily disabled.
   **/
  public Activation endDate(Date endDate) {
    this.endDate = endDate;
    return this;
  }

  
  @JsonProperty("endDate")
  public Date getEndDate() {
    return endDate;
  }
  public void setEndDate(Date endDate) {
    this.endDate = endDate;
  }

  /**
   * List of limits associated to the operational activation.
   **/
  public Activation limits(List<ActivationLimits> limits) {
    this.limits = limits;
    return this;
  }

  
  @JsonProperty("limits")
  public List<ActivationLimits> getLimits() {
    return limits;
  }
  public void setLimits(List<ActivationLimits> limits) {
    this.limits = limits;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Activation activation = (Activation) o;
    return Objects.equals(activationId, activation.activationId) &&
        Objects.equals(name, activation.name) &&
        Objects.equals(isActive, activation.isActive) &&
        Objects.equals(startDate, activation.startDate) &&
        Objects.equals(endDate, activation.endDate) &&
        Objects.equals(limits, activation.limits);
  }

  @Override
  public int hashCode() {
    return Objects.hash(activationId, name, isActive, startDate, endDate, limits);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Activation {\n");
    
    sb.append("    activationId: ").append(toIndentedString(activationId)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    isActive: ").append(toIndentedString(isActive)).append("\n");
    sb.append("    startDate: ").append(toIndentedString(startDate)).append("\n");
    sb.append("    endDate: ").append(toIndentedString(endDate)).append("\n");
    sb.append("    limits: ").append(toIndentedString(limits)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}