package uy.com.bbva.services.accounts.model.bantotal;
public class CabezeraDiario {
    String fechaContabilizado;
    String fechaValor;
    String estado;
    public String getFechaContabilizado() {
        return fechaContabilizado;
    }
    public void setFechaContabilizado(String fechaContabilizado) {
        this.fechaContabilizado = fechaContabilizado;
    }
    public String getFechaValor() {
        return fechaValor;
    }
    public void setFechaValor(String fechaValor) {
        this.fechaValor = fechaValor;
    }
    public String getEstado() {
        return estado;
    }
    public void setEstado(String estado) {
        this.estado = estado;
    }
}