package uy.com.bbva.services.accounts.service;
import uy.com.bbva.services.accounts.response.AccountTypeDataList;
import uy.com.bbva.services.commons.exceptions.ServiceException;
public abstract class AccountTypesApiService {
    public abstract AccountTypeDataList getAccountTypes() throws ServiceException;
}