package uy.com.bbva.services.accounts.model;

import com.fasterxml.jackson.annotation.JsonProperty;


import java.util.Objects;



public class AnonymousRepresentation5CreditsBalance   {
  
  private Double amount = null;
  private String currency = null;

  /**
   * Total amount of credits transactions for the current day.
   **/
  public AnonymousRepresentation5CreditsBalance amount(Double amount) {
    this.amount = amount;
    return this;
  }

  

  @JsonProperty("amount")
  public Double getAmount() {
    return amount;
  }
  public void setAmount(Double amount) {
    this.amount = amount;
  }

  /**
   * String based on ISO-4217 for specifying the currency related to the total credits amount for the current day.
   **/
  public AnonymousRepresentation5CreditsBalance currency(String currency) {
    this.currency = currency;
    return this;
  }

  

  @JsonProperty("currency")
  public String getCurrency() {
    return currency;
  }
  public void setCurrency(String currency) {
    this.currency = currency;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AnonymousRepresentation5CreditsBalance anonymousRepresentation5CreditsBalance = (AnonymousRepresentation5CreditsBalance) o;
    return Objects.equals(amount, anonymousRepresentation5CreditsBalance.amount) &&
        Objects.equals(currency, anonymousRepresentation5CreditsBalance.currency);
  }

  @Override
  public int hashCode() {
    return Objects.hash(amount, currency);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AnonymousRepresentation5CreditsBalance {\n");
    
    sb.append("    amount: ").append(toIndentedString(amount)).append("\n");
    sb.append("    currency: ").append(toIndentedString(currency)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}