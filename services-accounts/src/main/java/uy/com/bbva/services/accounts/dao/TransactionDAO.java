package uy.com.bbva.services.accounts.dao;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import uy.com.bbva.services.accounts.model.*;
import uy.com.bbva.services.accounts.model.bantotal.CabezalHistorico;
import uy.com.bbva.services.accounts.model.bantotal.CabezeraDiario;
import uy.com.bbva.services.accounts.response.TransactionDataList;
import uy.com.bbva.services.commons.dao.ManagerDataAccessAs400;
import uy.com.bbva.services.commons.exceptions.ServiceException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
@Component
public class TransactionDAO {
    @Autowired
    private ManagerDataAccessAs400 managerDataAccessAs400;
    private SimpleDateFormat sdfBantotal = new SimpleDateFormat("yyyyMMdd");
    public TransactionDataList transactions(long pageIndex, long pageSize, String accountId) throws ServiceException {
        TransactionDataList trans = new TransactionDataList();
        SaldoBantotal saldo = AccountsUtil.getSaldo(accountId);
        try {
            //-- Obtengo el rubro
            saldo.setRubro(obtenerRubro(saldo));
            Pagination pagination = new Pagination();
            //Obtengo la cantidad de movimientos Diarios
            int movDiariosCount = 0;
            PreparedStatement psMovDiariosCount = managerDataAccessAs400.prepareStatement(SQLStatementsBBVA.GET_MOVIMIENTOS_DIARIOS_COUNT);
            psMovDiariosCount.setInt(1, saldo.getPgcod());
            psMovDiariosCount.setInt(2, saldo.getSucursal());
            psMovDiariosCount.setLong(3, saldo.getRubro());
            psMovDiariosCount.setInt(4, saldo.getMoneda());
            psMovDiariosCount.setInt(5, saldo.getPapel());
            psMovDiariosCount.setInt(6, saldo.getCuenta());
            psMovDiariosCount.setInt(7, saldo.getOperacion());
            psMovDiariosCount.setInt(8, saldo.getSubOperacion());
            psMovDiariosCount.setInt(9, saldo.getTipoOperacion());
            ResultSet rsMovDiariosCount = managerDataAccessAs400.executeQuery(psMovDiariosCount);
            while (rsMovDiariosCount.next()) {
                movDiariosCount = rsMovDiariosCount.getInt(1);
            }
            rsMovDiariosCount.close();
            managerDataAccessAs400.close(psMovDiariosCount);
            //Obtengo la cantidad de movimientos istoricos
            int movHistoriciosCount = 0;
            PreparedStatement psMovHistoriciosCount = managerDataAccessAs400.prepareStatement(SQLStatementsBBVA.GET_MOVIMIENTOS_HISTORICOS_COUNT);
            psMovHistoriciosCount.setInt(1, saldo.getPgcod());
            psMovHistoriciosCount.setString(2, "20170101");
            psMovHistoriciosCount.setString(3, sdfBantotal.format(new Date()));
            psMovHistoriciosCount.setInt(4, saldo.getSucursal());
            psMovHistoriciosCount.setLong(5, saldo.getRubro());
            psMovHistoriciosCount.setInt(6, saldo.getMoneda());
            psMovHistoriciosCount.setInt(7, saldo.getPapel());
            psMovHistoriciosCount.setInt(8, saldo.getCuenta());
            psMovHistoriciosCount.setInt(9, saldo.getOperacion());
            psMovHistoriciosCount.setInt(10, saldo.getSubOperacion());
            ResultSet rsMovHistoriciosCount = managerDataAccessAs400.executeQuery(psMovHistoriciosCount);
            while (rsMovHistoriciosCount.next()) {
                movHistoriciosCount = rsMovHistoriciosCount.getInt(1);
            }
            rsMovHistoriciosCount.close();
            managerDataAccessAs400.close(psMovHistoriciosCount);
            long pageKeyValue = pageIndex>0? pageIndex-1 : 0;
            long pageSizeValue = pageSize>0? pageSize-1: movDiariosCount+movHistoriciosCount;
            //Calculo la primera fila de movimientos diarios
            long firstMovDiarios = ((pageKeyValue * pageSizeValue) <= movDiariosCount) ?
                    (pageKeyValue * pageSizeValue) : -1;
            //Calculo la primera fila de movimientos historico
            long firstMovHistoricos = (pageKeyValue * pageSizeValue)<1? 0 :
                    (((pageKeyValue * pageSizeValue)-movDiariosCount)<=movHistoriciosCount)?
                    ((pageKeyValue * pageSizeValue)-movDiariosCount) : -1;
            //Calculo la ultima fila de movimientos historico
            long lastMovHistoricos = movDiariosCount-(pageKeyValue*pageSizeValue) < 0?
                    (movDiariosCount-(pageKeyValue*pageSizeValue))-pageSizeValue : pageSizeValue ;
            //Obtenengo la cantidad de movimientos diaros + movimientos historicos
            double totalElements = movDiariosCount + movHistoriciosCount;
            pagination.setPage(Double.valueOf(pageIndex));
            pagination.pageSize(Double.valueOf(pageSize));
            pagination.setTotalElements(totalElements);
            pagination.totalPages(pageSize>0? Math.ceil(totalElements/pageSize):0);
            if(firstMovDiarios>-1 && movDiariosCount>(pageKeyValue * pageSizeValue)) {
                //-- Movimientos diarios
                PreparedStatement psMovDiarios = managerDataAccessAs400.prepareStatement(SQLStatementsBBVA.GET_MOVIMIENTOS_DIARIOS_PAGINATION);
                psMovDiarios.setInt(1, saldo.getPgcod());
                psMovDiarios.setInt(2, saldo.getSucursal());
                psMovDiarios.setLong(3, saldo.getRubro());
                psMovDiarios.setInt(4, saldo.getMoneda());
                psMovDiarios.setInt(5, saldo.getPapel());
                psMovDiarios.setInt(6, saldo.getCuenta());
                psMovDiarios.setInt(7, saldo.getOperacion());
                psMovDiarios.setInt(8, saldo.getSubOperacion());
                psMovDiarios.setInt(9, saldo.getTipoOperacion());
                psMovDiarios.setLong(10, pageSizeValue);
                psMovDiarios.setLong(11, firstMovDiarios);
                ResultSet rsMovDiarios = managerDataAccessAs400.executeQuery(psMovDiarios);
                while (rsMovDiarios.next()) {
                    MovimientoBantotal mb = new MovimientoBantotal();
                    mb.setPgcod(rsMovDiarios.getInt("pgcod"));
                    mb.setSucursal(rsMovDiarios.getInt("itsuc"));
                    mb.setModulo(rsMovDiarios.getInt("itmod"));
                    mb.setTrnasaccion(rsMovDiarios.getInt("ittran"));
                    mb.setRelacion(rsMovDiarios.getInt("itnrel"));
                    mb.setOrdinal(rsMovDiarios.getInt("itord"));
                    mb.setMonto(rsMovDiarios.getDouble("itimp1"));
                    mb.setMoneda(rsMovDiarios.getInt("Moneda"));
                    mb.setDebOCred(rsMovDiarios.getInt("itdbha"));
                    mb.setCuenta(rsMovDiarios.getInt("Ctnro"));
                    mb.setHistorico(false);
                    Transaction trn = new Transaction();
                    ModelImport mi = new ModelImport();
                    mi.setAmount(mb.getMonto());
                    mi.setCurrency(AccountsUtil.getSimboloMoneda(mb.getMoneda(),saldo.getPapel()));
                    trn.setLocalAmount(mi);
                    trn.setMoneyFlow(AccountsUtil.getMoneyFlow(mb.getDebOCred()));
                    trn.setConcept(obtenerConcepto(mb));
                    trn.setTransactionType(AccountsUtil.getTransactionType(mb.getModulo(), mb.getTrnasaccion()));
                    //-Datos del cabezal
                    CabezeraDiario cabezeraDiario = obtenerCabezalDiario(mb);
                    if (cabezeraDiario != null) {
                        mb.setFechaContabilizado(cabezeraDiario.getFechaContabilizado());
                        mb.setFechaValor(cabezeraDiario.getFechaValor());
                        mb.setEstado(cabezeraDiario.getEstado());
                        trn.setStatus(AccountsUtil.getStatus(mb.getEstado()));
                        try {
                            trn.setOperationDate(sdfBantotal.parse(mb.getFechaContabilizado()));
                            trn.setAccountedDate(sdfBantotal.parse(mb.getFechaContabilizado()));
                            trn.setValuationDate(sdfBantotal.parse(mb.getFechaValor()));
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                    trn.setTransactionId(AccountsUtil.getIdTransacction(mb));
                    trn.setFinancingType(AccountsUtil.getFinancingType(mb.getModulo(), mb.getTrnasaccion()));
                    trn.setContract(AccountsUtil.getContract(mb.getCuenta()));
                    if ("S".equals(mb.getEstado()) || "B".equals(mb.getEstado())) {
                        trans.getData().add(trn);
                    }
                }
                rsMovDiarios.close();
                managerDataAccessAs400.close(psMovDiarios);
            }
            if(firstMovHistoricos>-1 && lastMovHistoricos>0) {
                //-- Movimientos Históricos
                PreparedStatement psMovHistoricios = managerDataAccessAs400.prepareStatement(SQLStatementsBBVA.GET_MOVIMIENTOS_HISTORICOS_PAGINATION);
                psMovHistoricios.setInt(1, saldo.getPgcod());
                psMovHistoricios.setString(2, "20170101");
                psMovHistoricios.setString(3, sdfBantotal.format(new Date()));
                psMovHistoricios.setInt(4, saldo.getSucursal());
                psMovHistoricios.setLong(5, saldo.getRubro());
                psMovHistoricios.setInt(6, saldo.getMoneda());
                psMovHistoricios.setInt(7, saldo.getPapel());
                psMovHistoricios.setInt(8, saldo.getCuenta());
                psMovHistoricios.setInt(9, saldo.getOperacion());
                psMovHistoricios.setInt(10, saldo.getSubOperacion());
                psMovHistoricios.setLong(11, lastMovHistoricos);
                psMovHistoricios.setLong(12, firstMovHistoricos);
                //Pgcod, Hfcon, Hsucur, Hrubro, Hmda, Hpap, Hcta, Hoper, Hsubop
                ResultSet rsMovHistoricos = managerDataAccessAs400.executeQuery(psMovHistoricios);
                while (rsMovHistoricos.next()) {
                    MovimientoBantotal mb = new MovimientoBantotal();
                    mb.setPgcod(rsMovHistoricos.getInt("pgcod"));
                    mb.setFechaContabilizado(rsMovHistoricos.getString("Hfcon"));
                    mb.setSucursal(rsMovHistoricos.getInt("Hsucor"));
                    mb.setModulo(rsMovHistoricos.getInt("Hcmod"));
                    mb.setTrnasaccion(rsMovHistoricos.getInt("Htran"));
                    mb.setRelacion(rsMovHistoricos.getInt("Hnrel"));
                    mb.setOrdinal(rsMovHistoricos.getInt("Hcord"));
                    mb.setMonto(rsMovHistoricos.getDouble("Hcimp1"));
                    mb.setMoneda(rsMovHistoricos.getInt("Hmda"));
                    mb.setDebOCred(rsMovHistoricos.getInt("Hcodmo"));
                    mb.setCuenta(rsMovHistoricos.getInt("Hcta"));
                    mb.setHistorico(false);
                    Transaction trn = new Transaction();
                    ModelImport mi = new ModelImport();
                    mi.setAmount(mb.getMonto());
                    mi.setCurrency(AccountsUtil.getSimboloMoneda(mb.getMoneda(),saldo.getPapel()));
                    trn.setLocalAmount(mi);
                    trn.setMoneyFlow(AccountsUtil.getMoneyFlow(mb.getDebOCred()));
                    trn.setConcept(obtenerConcepto(mb));
                    trn.setTransactionType(AccountsUtil.getTransactionType(mb.getModulo(), mb.getTrnasaccion()));
                    mb.setEstado("S");
                    trn.setStatus(AccountsUtil.getStatus(mb.getEstado()));
                    //-Datos del cabezal
                    CabezalHistorico cabezalHistorico = obtenerCabezalHistorico(mb);
                    if (cabezalHistorico != null) {
                        mb.setFechaValor(cabezalHistorico.getFechaValor());
                        try {
                            trn.setAccountedDate(sdfBantotal.parse(mb.getFechaContabilizado()));
                            trn.setValuationDate(sdfBantotal.parse(mb.getFechaValor()));
                            trn.setOperationDate(sdfBantotal.parse(mb.getFechaContabilizado()));
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                    trn.setTransactionId(AccountsUtil.getIdTransacction(mb));
                    trn.setFinancingType(AccountsUtil.getFinancingType(mb.getModulo(), mb.getTrnasaccion()));
                    trn.setContract(AccountsUtil.getContract(mb.getCuenta()));
                    trans.getData().add(trn);
                }
                rsMovHistoricos.close();
                managerDataAccessAs400.close(psMovHistoricios);
            }
            trans.setPagination(pagination);
        } catch (SQLException e1) {
            throw new ServiceException("Error en transactions", "Error en transactions", e1);
        } finally {
            try {
                managerDataAccessAs400.closeConnection();
            } catch (SQLException e) {
                throw new ServiceException("Error al cerrar la conexion", "Error en transactions", e);
            }
        }
        return trans;
    }
    private String obtenerConcepto(MovimientoBantotal mb) throws SQLException {
        PreparedStatement psTransaccion = managerDataAccessAs400.prepareStatement(SQLStatementsBBVA.GET_NOMBRE_TRANSACCION);
        psTransaccion.setInt(1, mb.getPgcod());
        psTransaccion.setInt(2, mb.getModulo());
        psTransaccion.setInt(3, mb.getTrnasaccion());
        ResultSet rsNombreTransaccion = managerDataAccessAs400.executeQuery(psTransaccion);
        String concepto = "";
        while (rsNombreTransaccion.next()) {
            concepto = rsNombreTransaccion.getString("trnom").trim();
        }
        rsNombreTransaccion.close();
        managerDataAccessAs400.close(psTransaccion);
        return concepto;
    }
    private long obtenerRubro(SaldoBantotal saldo) throws SQLException {
        PreparedStatement psCuenta = managerDataAccessAs400.prepareStatement(SQLStatementsBBVA.GET_CUENTAVISTA);
        psCuenta.setInt(1, saldo.getModulo());
        psCuenta.setInt(2, saldo.getMoneda());
        psCuenta.setInt(3, saldo.getPapel());
        psCuenta.setInt(4, saldo.getCuenta());
        psCuenta.setInt(5, saldo.getSubOperacion());
        psCuenta.setInt(6, saldo.getSucursal());
        ResultSet rsCuenta = managerDataAccessAs400.executeQuery(psCuenta);
        long rubro = 0;
        while (rsCuenta.next()) {
            rubro = rsCuenta.getLong("scrub");
        }
        rsCuenta.close();
        managerDataAccessAs400.close(psCuenta);
        return rubro;
    }
    private CabezalHistorico obtenerCabezalHistorico(MovimientoBantotal mb) throws SQLException {
        PreparedStatement psCabezal = managerDataAccessAs400.prepareStatement(SQLStatementsBBVA.GET_CABEZAL_MOVIMIENTO_HISTORICO);
        psCabezal.setInt(1, mb.getPgcod());
        psCabezal.setInt(2, mb.getModulo());
        psCabezal.setInt(3, mb.getSucursal());
        psCabezal.setInt(4, mb.getTrnasaccion());
        psCabezal.setInt(5, mb.getRelacion());
        psCabezal.setString(6, mb.getFechaContabilizado());
        ResultSet rsCabezal = managerDataAccessAs400.executeQuery(psCabezal);
        CabezalHistorico cabezalHistorico = null;
        while (rsCabezal.next()) {
            cabezalHistorico = new CabezalHistorico();
            cabezalHistorico.setFechaValor(rsCabezal.getString("HFvc"));
        }
        rsCabezal.close();
        managerDataAccessAs400.close(psCabezal);
        return cabezalHistorico;
    }
    private CabezeraDiario obtenerCabezalDiario(MovimientoBantotal mb) throws SQLException {
        PreparedStatement psCabezalDiario = managerDataAccessAs400.prepareStatement(SQLStatementsBBVA.GET_CABEZAL_MOVIMIENTO_DIARIO);
        psCabezalDiario.setInt(1, mb.getPgcod());
        psCabezalDiario.setInt(2, mb.getSucursal());
        psCabezalDiario.setInt(3, mb.getModulo());
        psCabezalDiario.setInt(4, mb.getTrnasaccion());
        psCabezalDiario.setInt(5, mb.getRelacion());
        CabezeraDiario cabezeraDiario = null;
        ResultSet rsCabDiario = managerDataAccessAs400.executeQuery(psCabezalDiario);
        while (rsCabDiario.next()) {
            cabezeraDiario = new CabezeraDiario();
            cabezeraDiario.setFechaContabilizado(rsCabDiario.getString("itfcon"));
            cabezeraDiario.setFechaValor(rsCabDiario.getString("itfvc"));
            cabezeraDiario.setEstado(rsCabDiario.getString("itcont"));
        }
        rsCabDiario.close();
        managerDataAccessAs400.close(psCabezalDiario);
        return cabezeraDiario;
    }
}