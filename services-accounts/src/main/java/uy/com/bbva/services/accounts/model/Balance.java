package uy.com.bbva.services.accounts.model;

import com.fasterxml.jackson.annotation.JsonProperty;


import java.util.ArrayList;
import java.util.List;
import java.util.Objects;



public class Balance {
  
  private Grouping groupings = null;
  private List<Breakdown> breakdown = new ArrayList<Breakdown>();

  /**
   * Discriminant classification in which the balances will be segregated.
   **/
  public Balance groupings(Grouping groupings) {
    this.groupings = groupings;
    return this;
  }

  

  @JsonProperty("groupings")
  public Grouping getGroupings() {
    return groupings;
  }
  public void setGroupings(Grouping groupings) {
    this.groupings = groupings;
  }

  /**
   * Set of applicable balances, grouped by currency, that are present in the accounts that match the criteria defined by the grouping items. DISCLAIMER: At least one of balances must be provided.
   **/
  public Balance breakdown(List<Breakdown> breakdown) {
    this.breakdown = breakdown;
    return this;
  }

  

  @JsonProperty("breakdown")
  public List<Breakdown> getBreakdown() {
    return breakdown;
  }
  public void setBreakdown(List<Breakdown> breakdown) {
    this.breakdown = breakdown;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Balance balance = (Balance) o;
    return Objects.equals(groupings, balance.groupings) &&
        Objects.equals(breakdown, balance.breakdown);
  }

  @Override
  public int hashCode() {
    return Objects.hash(groupings, breakdown);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Balance {\n");
    
    sb.append("    groupings: ").append(toIndentedString(groupings)).append("\n");
    sb.append("    breakdown: ").append(toIndentedString(breakdown)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
