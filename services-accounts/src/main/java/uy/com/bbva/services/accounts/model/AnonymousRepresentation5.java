package uy.com.bbva.services.accounts.model;

import com.fasterxml.jackson.annotation.JsonProperty;


import java.util.ArrayList;
import java.util.List;
import java.util.Objects;



public class AnonymousRepresentation5   {
  
  private List<AnonymousRepresentation5Credits> credits = new ArrayList<AnonymousRepresentation5Credits>();
  private List<AnonymousRepresentation5Debits> debits = new ArrayList<AnonymousRepresentation5Debits>();

  /**
   * Total credits made in the account on the current day. There will be an element in the array for each type of currency with which the account operates (in the case of multicurrency accounts, there will be two or more, for accounts in one currency, one element).
   **/
  public AnonymousRepresentation5 credits(List<AnonymousRepresentation5Credits> credits) {
    this.credits = credits;
    return this;
  }

  

  @JsonProperty("credits")
  public List<AnonymousRepresentation5Credits> getCredits() {
    return credits;
  }
  public void setCredits(List<AnonymousRepresentation5Credits> credits) {
    this.credits = credits;
  }

  /**
   * Total debits made in the account on the current day. There will be an element in the array for each type of currency with which the account operates (in the case of multicurrency accounts, there will be two or more, for accounts in one currency, one element).
   **/
  public AnonymousRepresentation5 debits(List<AnonymousRepresentation5Debits> debits) {
    this.debits = debits;
    return this;
  }

  

  @JsonProperty("debits")
  public List<AnonymousRepresentation5Debits> getDebits() {
    return debits;
  }
  public void setDebits(List<AnonymousRepresentation5Debits> debits) {
    this.debits = debits;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AnonymousRepresentation5 anonymousRepresentation5 = (AnonymousRepresentation5) o;
    return Objects.equals(credits, anonymousRepresentation5.credits) &&
        Objects.equals(debits, anonymousRepresentation5.debits);
  }

  @Override
  public int hashCode() {
    return Objects.hash(credits, debits);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AnonymousRepresentation5 {\n");
    
    sb.append("    credits: ").append(toIndentedString(credits)).append("\n");
    sb.append("    debits: ").append(toIndentedString(debits)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

