package uy.com.bbva.services.accounts.response;
import com.fasterxml.jackson.annotation.JsonProperty;
import uy.com.bbva.services.accounts.model.Account;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
public class AccountDataList{
  
  private List<Account> data = new ArrayList<>();
  /**
   **/
  public AccountDataList data(List<Account> data) {
    this.data = data;
    return this;
  }
  
  @JsonProperty("data")
  public List<Account> getData() {
    return data;
  }
  public void setData(List<Account> data) {
    this.data = data;
  }
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AccountDataList accountDataList = (AccountDataList) o;
    return Objects.equals(data, accountDataList.data);
  }
  @Override
  public int hashCode() {
    return Objects.hash(data);
  }
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AccountDataList {\n");
    
    sb.append("    data: ").append(toIndentedString(data)).append("\n");
    sb.append("}");
    return sb.toString();
  }
  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}