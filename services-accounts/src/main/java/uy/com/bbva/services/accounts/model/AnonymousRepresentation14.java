package uy.com.bbva.services.accounts.model;

import com.fasterxml.jackson.annotation.JsonProperty;


import java.util.Objects;



public class AnonymousRepresentation14   {
  
  private String activationId = null;
  private String name = null;
  private Boolean isActive = false;

  /**
   * Operational activation identifier.
   **/
  public AnonymousRepresentation14 activationId(String activationId) {
    this.activationId = activationId;
    return this;
  }

  

  @JsonProperty("activationId")
  public String getActivationId() {
    return activationId;
  }
  public void setActivationId(String activationId) {
    this.activationId = activationId;
  }

  /**
   * Operational activation description.
   **/
  public AnonymousRepresentation14 name(String name) {
    this.name = name;
    return this;
  }

  

  @JsonProperty("name")
  public String getName() {
    return name;
  }
  public void setName(String name) {
    this.name = name;
  }

  /**
   * Indicates whether the activation is enabled.
   **/
  public AnonymousRepresentation14 isActive(Boolean isActive) {
    this.isActive = isActive;
    return this;
  }

  

  @JsonProperty("isActive")
  public Boolean getIsActive() {
    return isActive;
  }
  public void setIsActive(Boolean isActive) {
    this.isActive = isActive;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AnonymousRepresentation14 anonymousRepresentation14 = (AnonymousRepresentation14) o;
    return Objects.equals(activationId, anonymousRepresentation14.activationId) &&
        Objects.equals(name, anonymousRepresentation14.name) &&
        Objects.equals(isActive, anonymousRepresentation14.isActive);
  }

  @Override
  public int hashCode() {
    return Objects.hash(activationId, name, isActive);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AnonymousRepresentation14 {\n");
    
    sb.append("    activationId: ").append(toIndentedString(activationId)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    isActive: ").append(toIndentedString(isActive)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
