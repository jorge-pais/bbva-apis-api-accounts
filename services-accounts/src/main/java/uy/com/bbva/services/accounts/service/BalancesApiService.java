package uy.com.bbva.services.accounts.service;
import uy.com.bbva.services.accounts.response.BalanceDataList;
import uy.com.bbva.services.commons.exceptions.ServiceException;
import java.util.Date;
public abstract class BalancesApiService {
    public abstract BalanceDataList getBalances(String accountFamilyId, String accountTypeId, String currenciesCurrency, Date periodStartDate, Date periodEndDate, String documento) throws ServiceException;
}