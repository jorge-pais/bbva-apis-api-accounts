package uy.com.bbva.services.accounts.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

public class AnonymousRepresentation20   {
  
  private Account data = null;

  /**
   **/
  public AnonymousRepresentation20 data(Account data) {
    this.data = data;
    return this;
  }

  @JsonProperty("data")
  public Account getData() {
    return data;
  }
  public void setData(Account data) {
    this.data = data;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AnonymousRepresentation20 anonymousRepresentation20 = (AnonymousRepresentation20) o;
    return Objects.equals(data, anonymousRepresentation20.data);
  }

  @Override
  public int hashCode() {
    return Objects.hash(data);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AnonymousRepresentation20 {\n");
    
    sb.append("    data: ").append(toIndentedString(data)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}