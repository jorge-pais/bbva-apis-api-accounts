package uy.com.bbva.services.accounts.model;

import com.fasterxml.jackson.annotation.JsonProperty;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;



public class AnonymousRepresentation7   {
  
  private String id = null;
  private Amount localAmount = null;
  private Amount originAmount = null;
  private MoneyFlow moneyFlow = null;
  private String concept = null;
  private TransactionType transactionType = null;
  private Date operationDate = null;
  private Date accountedDate = null;
  private Date valuationDate = null;
  private FinancingType financingType = null;
  private StatusTransaction status = null;
  private Contract contract = null;
  private List<String> tags = new ArrayList<String>();
  private Category category = null;
  private AnonymousRepresentation7AdditionalInformation additionalInformation = null;

  /**
   * Unique identifier of the transaction.
   **/
  public AnonymousRepresentation7 id(String id) {
    this.id = id;
    return this;
  }

  

  @JsonProperty("id")
  public String getId() {
    return id;
  }
  public void setId(String id) {
    this.id = id;
  }

  /**
   * Amount that is associated to the transaction on the local currency. This amount may not include any charges that the bank can carry with the operation.
   **/
  public AnonymousRepresentation7 localAmount(Amount localAmount) {
    this.localAmount = localAmount;
    return this;
  }

  

  @JsonProperty("localAmount")
  public Amount getLocalAmount() {
    return localAmount;
  }
  public void setLocalAmount(Amount localAmount) {
    this.localAmount = localAmount;
  }

  /**
   * Origin amount that is associated to the transaction. This attribute can only be informed when the transactions has been performed on a currency that is different from the contract\\`s original currency. For example, when a customer performs a purchase in USD using his card that is associated to an account opened in Euros, originAmount attribute will be fulfilled with the original price of the item purchased in UK. This amount may not include any charges that the bank can carry with the operation.
   **/
  public AnonymousRepresentation7 originAmount(Amount originAmount) {
    this.originAmount = originAmount;
    return this;
  }

  

  @JsonProperty("originAmount")
  public Amount getOriginAmount() {
    return originAmount;
  }
  public void setOriginAmount(Amount originAmount) {
    this.originAmount = originAmount;
  }

  /**
   * Indicates whether the operation is an addition or a substraction from the total balance that customer has in the cotract that is being querying.
   **/
  public AnonymousRepresentation7 moneyFlow(MoneyFlow moneyFlow) {
    this.moneyFlow = moneyFlow;
    return this;
  }

  

  @JsonProperty("moneyFlow")
  public MoneyFlow getMoneyFlow() {
    return moneyFlow;
  }
  public void setMoneyFlow(MoneyFlow moneyFlow) {
    this.moneyFlow = moneyFlow;
  }

  /**
   * Brief description of the operative that create the transaction. Backend must provide this information depending on the transaction\\`s type.
   **/
  public AnonymousRepresentation7 concept(String concept) {
    this.concept = concept;
    return this;
  }

  

  @JsonProperty("concept")
  public String getConcept() {
    return concept;
  }
  public void setConcept(String concept) {
    this.concept = concept;
  }

  /**
   * Transaction type. It talks about which kind of operative has created this transaction. DISCLAIMER: Nowsdays some geographies have no possibility of identifying all the types of transaction that are detailed below. This should not become an impediment that in the near future this inconvenience be fixed by the corresponding backends and allow the customers to know and filter those transactions according to their criteria.
   **/
  public AnonymousRepresentation7 transactionType(TransactionType transactionType) {
    this.transactionType = transactionType;
    return this;
  }

  

  @JsonProperty("transactionType")
  public TransactionType getTransactionType() {
    return transactionType;
  }
  public void setTransactionType(TransactionType transactionType) {
    this.transactionType = transactionType;
  }

  /**
   * String based on ISO-8601 date format for providing the last date when the operation was performed by the client.
   **/
  public AnonymousRepresentation7 operationDate(Date operationDate) {
    this.operationDate = operationDate;
    return this;
  }

  

  @JsonProperty("operationDate")
  public Date getOperationDate() {
    return operationDate;
  }
  public void setOperationDate(Date operationDate) {
    this.operationDate = operationDate;
  }

  /**
   * String based on ISO-8601 date format for providing the last date when the transaction was accounted.
   **/
  public AnonymousRepresentation7 accountedDate(Date accountedDate) {
    this.accountedDate = accountedDate;
    return this;
  }

  

  @JsonProperty("accountedDate")
  public Date getAccountedDate() {
    return accountedDate;
  }
  public void setAccountedDate(Date accountedDate) {
    this.accountedDate = accountedDate;
  }

  /**
   * String based on ISO-8601 date format, this value is used when a debit or credit is fully effective and begins to accrue interest. <br/> For example: <br/> - In check income, to take into account the value date, it will be necessary to wait for the secure payment in the account of the entity. <br/> - In transfer income, entities can not apply a date value higher than the day after the credit has been posted.
   **/
  public AnonymousRepresentation7 valuationDate(Date valuationDate) {
    this.valuationDate = valuationDate;
    return this;
  }

  

  @JsonProperty("valuationDate")
  public Date getValuationDate() {
    return valuationDate;
  }
  public void setValuationDate(Date valuationDate) {
    this.valuationDate = valuationDate;
  }

  /**
   * It represents the information about the possibility that certain operations to be financed within the conditions of the product to which they belong. For example, we can perform a purchase in an store that can be financed if the amount of the purchase is greater than 50,00 euros. The product and the transaction type can define which business rules are applied to know whether the operation can be financed or not.
   **/
  public AnonymousRepresentation7 financingType(FinancingType financingType) {
    this.financingType = financingType;
    return this;
  }

  

  @JsonProperty("financingType")
  public FinancingType getFinancingType() {
    return financingType;
  }
  public void setFinancingType(FinancingType financingType) {
    this.financingType = financingType;
  }

  /**
   * Transaction status.
   **/
  public AnonymousRepresentation7 status(StatusTransaction status) {
    this.status = status;
    return this;
  }

  

  @JsonProperty("status")
  public StatusTransaction getStatus() {
    return status;
  }
  public void setStatus(StatusTransaction status) {
    this.status = status;
  }

  /**
   * Customer\\`s contract that is being queried. When customer is performing an account\\`s transactions query in this attribute must be setted the information about that account. It occurs the same for other products which transaction can be queried (i.e. cards, loans, etc...)
   **/
  public AnonymousRepresentation7 contract(Contract contract) {
    this.contract = contract;
    return this;
  }

  

  @JsonProperty("contract")
  public Contract getContract() {
    return contract;
  }
  public void setContract(Contract contract) {
    this.contract = contract;
  }

  /**
   * List of tags that can label a transaction. This tags are customizable by the customers and can be used to help them when the try to locate a transaction.
   **/
  public AnonymousRepresentation7 tags(List<String> tags) {
    this.tags = tags;
    return this;
  }

  

  @JsonProperty("tags")
  public List<String> getTags() {
    return tags;
  }
  public void setTags(List<String> tags) {
    this.tags = tags;
  }

  /**
   * Represents the category that a transaction can fit.
   **/
  public AnonymousRepresentation7 category(Category category) {
    this.category = category;
    return this;
  }

  

  @JsonProperty("category")
  public Category getCategory() {
    return category;
  }
  public void setCategory(Category category) {
    this.category = category;
  }

  /**
   * Additional information related to this transaction.
   **/
  public AnonymousRepresentation7 additionalInformation(AnonymousRepresentation7AdditionalInformation additionalInformation) {
    this.additionalInformation = additionalInformation;
    return this;
  }

  

  @JsonProperty("additionalInformation")
  public AnonymousRepresentation7AdditionalInformation getAdditionalInformation() {
    return additionalInformation;
  }
  public void setAdditionalInformation(AnonymousRepresentation7AdditionalInformation additionalInformation) {
    this.additionalInformation = additionalInformation;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AnonymousRepresentation7 anonymousRepresentation7 = (AnonymousRepresentation7) o;
    return Objects.equals(id, anonymousRepresentation7.id) &&
        Objects.equals(localAmount, anonymousRepresentation7.localAmount) &&
        Objects.equals(originAmount, anonymousRepresentation7.originAmount) &&
        Objects.equals(moneyFlow, anonymousRepresentation7.moneyFlow) &&
        Objects.equals(concept, anonymousRepresentation7.concept) &&
        Objects.equals(transactionType, anonymousRepresentation7.transactionType) &&
        Objects.equals(operationDate, anonymousRepresentation7.operationDate) &&
        Objects.equals(accountedDate, anonymousRepresentation7.accountedDate) &&
        Objects.equals(valuationDate, anonymousRepresentation7.valuationDate) &&
        Objects.equals(financingType, anonymousRepresentation7.financingType) &&
        Objects.equals(status, anonymousRepresentation7.status) &&
        Objects.equals(contract, anonymousRepresentation7.contract) &&
        Objects.equals(tags, anonymousRepresentation7.tags) &&
        Objects.equals(category, anonymousRepresentation7.category) &&
        Objects.equals(additionalInformation, anonymousRepresentation7.additionalInformation);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, localAmount, originAmount, moneyFlow, concept, transactionType, operationDate, accountedDate, valuationDate, financingType, status, contract, tags, category, additionalInformation);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AnonymousRepresentation7 {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    localAmount: ").append(toIndentedString(localAmount)).append("\n");
    sb.append("    originAmount: ").append(toIndentedString(originAmount)).append("\n");
    sb.append("    moneyFlow: ").append(toIndentedString(moneyFlow)).append("\n");
    sb.append("    concept: ").append(toIndentedString(concept)).append("\n");
    sb.append("    transactionType: ").append(toIndentedString(transactionType)).append("\n");
    sb.append("    operationDate: ").append(toIndentedString(operationDate)).append("\n");
    sb.append("    accountedDate: ").append(toIndentedString(accountedDate)).append("\n");
    sb.append("    valuationDate: ").append(toIndentedString(valuationDate)).append("\n");
    sb.append("    financingType: ").append(toIndentedString(financingType)).append("\n");
    sb.append("    status: ").append(toIndentedString(status)).append("\n");
    sb.append("    contract: ").append(toIndentedString(contract)).append("\n");
    sb.append("    tags: ").append(toIndentedString(tags)).append("\n");
    sb.append("    category: ").append(toIndentedString(category)).append("\n");
    sb.append("    additionalInformation: ").append(toIndentedString(additionalInformation)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
