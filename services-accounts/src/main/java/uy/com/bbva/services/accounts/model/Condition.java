package uy.com.bbva.services.accounts.model;

import com.fasterxml.jackson.annotation.JsonProperty;


import java.util.ArrayList;
import java.util.List;
import java.util.Objects;



public class Condition {
  
  private String conditionId = null;
  private String name = null;
  private Period period = null;
  private List<ConditionFacts> facts = new ArrayList<ConditionFacts>();
  private List<ConditionOutcomes> outcomes = new ArrayList<ConditionOutcomes>();

  /**
   * Condition identifier.
   **/
  public Condition conditionId(String conditionId) {
    this.conditionId = conditionId;
    return this;
  }

  

  @JsonProperty("conditionId")
  public String getConditionId() {
    return conditionId;
  }
  public void setConditionId(String conditionId) {
    this.conditionId = conditionId;
  }

  /**
   * Condition name or description.
   **/
  public Condition name(String name) {
    this.name = name;
    return this;
  }

  

  @JsonProperty("name")
  public String getName() {
    return name;
  }
  public void setName(String name) {
    this.name = name;
  }

  /**
   * Condition periodical information.  It includes the date when the condition will be evaluated.
   **/
  public Condition period(Period period) {
    this.period = period;
    return this;
  }

  

  @JsonProperty("period")
  public Period getPeriod() {
    return period;
  }
  public void setPeriod(Period period) {
    this.period = period;
  }

  /**
   * Facts that must be met to apply the condition.  All the facts will need to be fulfilled in order to apply this condition.
   **/
  public Condition facts(List<ConditionFacts> facts) {
    this.facts = facts;
    return this;
  }

  

  @JsonProperty("facts")
  public List<ConditionFacts> getFacts() {
    return facts;
  }
  public void setFacts(List<ConditionFacts> facts) {
    this.facts = facts;
  }

  /**
   * Outcome or result of applying the condition.
   **/
  public Condition outcomes(List<ConditionOutcomes> outcomes) {
    this.outcomes = outcomes;
    return this;
  }

  

  @JsonProperty("outcomes")
  public List<ConditionOutcomes> getOutcomes() {
    return outcomes;
  }
  public void setOutcomes(List<ConditionOutcomes> outcomes) {
    this.outcomes = outcomes;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Condition condition = (Condition) o;
    return Objects.equals(conditionId, condition.conditionId) &&
        Objects.equals(name, condition.name) &&
        Objects.equals(period, condition.period) &&
        Objects.equals(facts, condition.facts) &&
        Objects.equals(outcomes, condition.outcomes);
  }

  @Override
  public int hashCode() {
    return Objects.hash(conditionId, name, period, facts, outcomes);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Condition {\n");
    
    sb.append("    conditionId: ").append(toIndentedString(conditionId)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    period: ").append(toIndentedString(period)).append("\n");
    sb.append("    facts: ").append(toIndentedString(facts)).append("\n");
    sb.append("    outcomes: ").append(toIndentedString(outcomes)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
