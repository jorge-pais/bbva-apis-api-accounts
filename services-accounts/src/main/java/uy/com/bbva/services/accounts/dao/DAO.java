package uy.com.bbva.services.accounts.dao;
import uy.com.bbva.services.accounts.model.Account;
import uy.com.bbva.services.accounts.response.AccountDataList;
import uy.com.bbva.services.accounts.response.AccountTypeDataList;
import uy.com.bbva.services.accounts.response.BalanceDataList;
import uy.com.bbva.services.accounts.response.TransactionDataList;
import uy.com.bbva.services.commons.exceptions.ServiceException;
import java.util.Date;
public interface DAO {
	Account account(String accountId) throws ServiceException;
	AccountDataList accounts(String clientId, String idCuenta) throws ServiceException;
	BalanceDataList balances(String clientId, String accountFamilyId, String accountTypeId, String currenciesCurrency,
			Date periodStartDate, Date periodEndDate) throws ServiceException;
//	TransactionDataList transactions(String accountId) throws ServiceException;
	AccountTypeDataList getAccountsType();
}