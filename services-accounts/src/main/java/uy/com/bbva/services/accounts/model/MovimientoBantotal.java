package uy.com.bbva.services.accounts.model;

public class MovimientoBantotal {

	private int pgcod = 1;
	private String fechaContabilizado;
	private String fechaValor;
	private int sucursal;
	private int modulo;
	private int trnasaccion;
	private int relacion;
	private int ordinal;
	private int subOrdinal;
	private double monto;
	private int moneda;
	private int cuenta;
	private int debOCred; //deb = 1, cred = 2
	private String estado;
	private boolean historico;
	
	
	public int getPgcod() {
		return pgcod;
	}
	public void setPgcod(int pgcod) {
		this.pgcod = pgcod;
	}
	public String getFechaContabilizado() {
		return fechaContabilizado;
	}
	public void setFechaContabilizado(String fechaContabilizado) {
		this.fechaContabilizado = fechaContabilizado;
	}
	public int getSucursal() {
		return sucursal;
	}
	public void setSucursal(int sucursal) {
		this.sucursal = sucursal;
	}
	public int getModulo() {
		return modulo;
	}
	public void setModulo(int modulo) {
		this.modulo = modulo;
	}
	public int getTrnasaccion() {
		return trnasaccion;
	}
	public void setTrnasaccion(int trnasaccion) {
		this.trnasaccion = trnasaccion;
	}
	public int getRelacion() {
		return relacion;
	}
	public void setRelacion(int relacion) {
		this.relacion = relacion;
	}
	public int getOrdinal() {
		return ordinal;
	}
	public void setOrdinal(int ordinal) {
		this.ordinal = ordinal;
	}
	public int getSubOrdinal() {
		return subOrdinal;
	}
	public void setSubOrdinal(int subOrdinal) {
		this.subOrdinal = subOrdinal;
	}
	public boolean isHistorico() {
		return historico;
	}
	public void setHistorico(boolean historico) {
		this.historico = historico;
	}
	public double getMonto() {
		return monto;
	}
	public void setMonto(double monto) {
		this.monto = monto;
	}
	public int getMoneda() {
		return moneda;
	}
	public void setMoneda(int moneda) {
		this.moneda = moneda;
	}
	public int getDebOCred() {
		return debOCred;
	}
	public void setDebOCred(int debOCred) {
		this.debOCred = debOCred;
	}
	public String getFechaValor() {
		return fechaValor;
	}
	public void setFechaValor(String fechaValor) {
		this.fechaValor = fechaValor;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public int getCuenta() {
		return cuenta;
	}
	public void setCuenta(int cuenta) {
		this.cuenta = cuenta;
	}
	

}