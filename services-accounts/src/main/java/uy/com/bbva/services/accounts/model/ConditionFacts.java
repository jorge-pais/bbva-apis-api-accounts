package uy.com.bbva.services.accounts.model;

import com.fasterxml.jackson.annotation.JsonProperty;


import java.util.Objects;



public class ConditionFacts {
  
  private OperatorType factType = null;
  private Amount conditionAmount = null;
  private Apply apply = null;

  /**
   * Fact type.
   **/
  public ConditionFacts factType(OperatorType factType) {
    this.factType = factType;
    return this;
  }

  

  @JsonProperty("factType")
  public OperatorType getFactType() {
    return factType;
  }
  public void setFactType(OperatorType factType) {
    this.factType = factType;
  }

  /**
   * Fact amount.
   **/
  public ConditionFacts conditionAmount(Amount conditionAmount) {
    this.conditionAmount = conditionAmount;
    return this;
  }

  

  @JsonProperty("conditionAmount")
  public Amount getConditionAmount() {
    return conditionAmount;
  }
  public void setConditionAmount(Amount conditionAmount) {
    this.conditionAmount = conditionAmount;
  }

  /**
   * Property or value over which Fact value applies.
   **/
  public ConditionFacts apply(Apply apply) {
    this.apply = apply;
    return this;
  }

  

  @JsonProperty("apply")
  public Apply getApply() {
    return apply;
  }
  public void setApply(Apply apply) {
    this.apply = apply;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ConditionFacts conditionFacts = (ConditionFacts) o;
    return Objects.equals(factType, conditionFacts.factType) &&
        Objects.equals(conditionAmount, conditionFacts.conditionAmount) &&
        Objects.equals(apply, conditionFacts.apply);
  }

  @Override
  public int hashCode() {
    return Objects.hash(factType, conditionAmount, apply);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ConditionFacts {\n");
    
    sb.append("    factType: ").append(toIndentedString(factType)).append("\n");
    sb.append("    conditionAmount: ").append(toIndentedString(conditionAmount)).append("\n");
    sb.append("    apply: ").append(toIndentedString(apply)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
