package uy.com.bbva.services.accounts.model;

import com.fasterxml.jackson.annotation.JsonProperty;


import java.util.Objects;



public class Participant {
  
  private String participantId = null;
  private String firstName = null;
  private String middleName = null;
  private String lastName = null;
  private String suffix = null;
  private ParticipantType participantType = null;

  /**
   * Participant identifier.
   **/
  public Participant participantId(String participantId) {
    this.participantId = participantId;
    return this;
  }

  

  @JsonProperty("participantId")
  public String getParticipantId() {
    return participantId;
  }
  public void setParticipantId(String participantId) {
    this.participantId = participantId;
  }

  /**
   * Participant first name.
   **/
  public Participant firstName(String firstName) {
    this.firstName = firstName;
    return this;
  }

  

  @JsonProperty("firstName")
  public String getFirstName() {
    return firstName;
  }
  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  /**
   * Part of the participant\\'s name occurring between the first and family names, as a second given name or a maternal surname.
   **/
  public Participant middleName(String middleName) {
    this.middleName = middleName;
    return this;
  }

  

  @JsonProperty("middleName")
  public String getMiddleName() {
    return middleName;
  }
  public void setMiddleName(String middleName) {
    this.middleName = middleName;
  }

  /**
   * Participant last name.
   **/
  public Participant lastName(String lastName) {
    this.lastName = lastName;
    return this;
  }

  

  @JsonProperty("lastName")
  public String getLastName() {
    return lastName;
  }
  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  /**
   * Suffix is a part that follows a participant\\'s full name and provides additional information about the person. Post-nominal suffixes indicate that the person holds a position, educational degree, accreditation, office, or honor (e.g. MD\\: Suffix for a Doctor of Medicine, JD\\: Suffix for a Juris Doctor, PHD\\: Suffix for a Doctor of Philosophy, CPA\\: Suffix for a Certified Public Accountant). Other suffixes include generational designations (e.g. JR\\: Suffix for the second generation of a family name, SR\\: Identifier suffix for the first generation of a family name, III\\: Identifier suffix for the third generation of a family name, IV\\: Identifier suffix for the fourth generation of a family name.)
   **/
  public Participant suffix(String suffix) {
    this.suffix = suffix;
    return this;
  }

  

  @JsonProperty("suffix")
  public String getSuffix() {
    return suffix;
  }
  public void setSuffix(String suffix) {
    this.suffix = suffix;
  }

  /**
   * Participation role.
   **/
  public Participant participantType(ParticipantType participantType) {
    this.participantType = participantType;
    return this;
  }

  

  @JsonProperty("participantType")
  public ParticipantType getParticipantType() {
    return participantType;
  }
  public void setParticipantType(ParticipantType participantType) {
    this.participantType = participantType;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Participant participant = (Participant) o;
    return Objects.equals(participantId, participant.participantId) &&
        Objects.equals(firstName, participant.firstName) &&
        Objects.equals(middleName, participant.middleName) &&
        Objects.equals(lastName, participant.lastName) &&
        Objects.equals(suffix, participant.suffix) &&
        Objects.equals(participantType, participant.participantType);
  }

  @Override
  public int hashCode() {
    return Objects.hash(participantId, firstName, middleName, lastName, suffix, participantType);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Participant {\n");
    
    sb.append("    participantId: ").append(toIndentedString(participantId)).append("\n");
    sb.append("    firstName: ").append(toIndentedString(firstName)).append("\n");
    sb.append("    middleName: ").append(toIndentedString(middleName)).append("\n");
    sb.append("    lastName: ").append(toIndentedString(lastName)).append("\n");
    sb.append("    suffix: ").append(toIndentedString(suffix)).append("\n");
    sb.append("    participantType: ").append(toIndentedString(participantType)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
