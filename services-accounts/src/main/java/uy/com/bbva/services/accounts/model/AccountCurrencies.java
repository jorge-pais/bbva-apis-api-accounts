package uy.com.bbva.services.accounts.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;


public class AccountCurrencies   {
  
  private String currency = null;
  private Boolean isMajor = false;

  /**
   * String based on ISO-4217 for specifying the currency.
   **/
  public AccountCurrencies currency(String currency) {
    this.currency = currency;
    return this;
  }

  
  @JsonProperty("currency")
  public String getCurrency() {
    return currency;
  }
  public void setCurrency(String currency) {
    this.currency = currency;
  }

  /**
   * Flag indicating whether this currency corresponds to the main currency.
   **/
  public AccountCurrencies isMajor(Boolean isMajor) {
    this.isMajor = isMajor;
    return this;
  }

  
  @JsonProperty("isMajor")
  public Boolean getIsMajor() {
    return isMajor;
  }
  public void setIsMajor(Boolean isMajor) {
    this.isMajor = isMajor;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AccountCurrencies accountCurrencies = (AccountCurrencies) o;
    return Objects.equals(currency, accountCurrencies.currency) &&
        Objects.equals(isMajor, accountCurrencies.isMajor);
  }

  @Override
  public int hashCode() {
    return Objects.hash(currency, isMajor);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AccountCurrencies {\n");
    
    sb.append("    currency: ").append(toIndentedString(currency)).append("\n");
    sb.append("    isMajor: ").append(toIndentedString(isMajor)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}