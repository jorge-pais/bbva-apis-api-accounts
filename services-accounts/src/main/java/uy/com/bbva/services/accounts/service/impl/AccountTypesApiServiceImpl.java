package uy.com.bbva.services.accounts.service.impl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uy.com.bbva.services.accounts.dao.DAO;
import uy.com.bbva.services.accounts.response.AccountTypeDataList;
import uy.com.bbva.services.accounts.service.AccountTypesApiService;
import uy.com.bbva.services.commons.exceptions.ServiceException;
@Service
public class AccountTypesApiServiceImpl extends AccountTypesApiService {
	
	@Autowired
	private DAO dao;
	
	
    @Override
    public AccountTypeDataList getAccountTypes() throws ServiceException {
        return dao.getAccountsType();
    }
}