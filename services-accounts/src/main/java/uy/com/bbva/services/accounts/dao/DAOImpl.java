package uy.com.bbva.services.accounts.dao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import uy.com.bbva.services.accounts.model.*;
import uy.com.bbva.services.accounts.response.AccountDataList;
import uy.com.bbva.services.accounts.response.AccountTypeDataList;
import uy.com.bbva.services.accounts.response.BalanceDataList;
import uy.com.bbva.services.accounts.response.TransactionDataList;
import uy.com.bbva.services.commons.dao.ManagerDataAccessAs400;
import uy.com.bbva.services.commons.exceptions.ServiceException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
@Component
public class DAOImpl implements DAO {
	@Autowired
	private ManagerDataAccessAs400 manager;
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	private SimpleDateFormat sdfBantotal = new SimpleDateFormat("yyyyMMdd");
	private static HashMap<Integer,Branch> sucursales;
	public void init() throws ServiceException {
		if (sucursales == null){
			sucursales = new HashMap<>();
			String sqlSuc = "select * from fst001";
			try {
				PreparedStatement psSuc = manager.prepareStatement(sqlSuc);
				ResultSet rsScu = manager.executeQuery(psSuc);
				while(rsScu.next()){
					Branch branch = new Branch();
					branch.setBranchId(String.valueOf(rsScu.getString("sucurs")));
					branch.setName(rsScu.getString("scnom"));
					sucursales.put(Integer.parseInt(branch.getBranchId()), branch);
				}
			} catch (SQLException e1) {
				throw new ServiceException("Error en init","Error en init",e1);
			} finally {
				try {
					manager.closeConnection();
				} catch (SQLException e) {
					throw new ServiceException("Error al cerrar la conexion","Error en init",e);
				}
			}
		}
	}
	public Account account(String accountId) throws ServiceException {
		init();
		Account account = new Account();
		SaldoBantotal cta = AccountsUtil.getSaldo(accountId);
		double saldoAnterior;
		try {
			PreparedStatement psCuenta = manager.prepareStatement(SQLStatementsBBVA.GET_CUENTAVISTA);
			PreparedStatement psProducto = manager.prepareStatement(SQLStatementsBBVA.GET_PRODUCTO);
			PreparedStatement psHisto = manager.prepareStatement(SQLStatementsBBVA.GET_SALDO_ANTERIOR);
			psCuenta.setInt(1, cta.getModulo());
			psCuenta.setInt(2, cta.getMoneda());
			psCuenta.setInt(3, cta.getPapel());
			psCuenta.setInt(4, cta.getCuenta());
			psCuenta.setInt(5, cta.getSubOperacion());
			psCuenta.setInt(6, cta.getSucursal());
			ResultSet rsCuenta = manager.executeQuery(psCuenta);
			while(rsCuenta.next()){
				cta.setRubro(rsCuenta.getLong("scrub"));
				cta.setEstado(rsCuenta.getInt("scstat"));
				cta.setSaldo(rsCuenta.getDouble("scsdo"));
				cta.setScfunc(rsCuenta.getInt("scfunc"));
				psProducto.setInt(1, cta.getModulo());
				ResultSet rsProducto = manager.executeQuery(psProducto);
				String nombreProducto = null;
				while (rsProducto.next()) {
					nombreProducto = rsProducto.getString("Mdnom") != null ? rsProducto.getString("Mdnom").trim()
							: "";
				}
				rsProducto.close();
				psHisto.setInt(1, cta.getSucursal());
				psHisto.setLong(2, cta.getRubro());
				psHisto.setInt(3, cta.getMoneda());
				psHisto.setInt(4, cta.getPapel());
				psHisto.setInt(5, cta.getCuenta());
				psHisto.setInt(6, 0);
				psHisto.setInt(7, cta.getSubOperacion());
				psHisto.setInt(8, 0);
				psHisto.setInt(9, Calendar.getInstance().get(Calendar.YEAR));
				ResultSet rsHisto = manager.executeQuery(psHisto);
				// Saldo al cierre del día anterior
				saldoAnterior = 0;
				while (rsHisto.next()) {
					saldoAnterior = rsHisto.getDouble("Hasd13");
				}
				rsHisto.close();
				// Moneda de la cuenta
				AccountCurrencies currency = new AccountCurrencies();
				currency.setCurrency(AccountsUtil.getSimboloMoneda(cta.getMoneda(), cta.getPapel()));
				currency.setIsMajor(true);
				account.setAccountId(AccountsUtil.getId(cta.getSucursal()
						                               ,cta.getCuenta()
						                               ,cta.getMoneda()
						                               ,cta.getPapel()
						                               ,cta.getSubOperacion()
						                               ,cta.getModulo()));
				account.setNumber(AccountsUtil.getId(cta.getSucursal()
                        							,cta.getCuenta()
                        							,cta.getMoneda()
                        							,cta.getPapel()
                        							,cta.getSubOperacion()
                        							,cta.getModulo()));
				account.setNumberType(AccountsUtil.getNumberType());
				account.setAccountFamily(AccountsUtil.getAccountFamily(cta.getModulo()));
				account.setAccountType(AccountsUtil.getAccountType(cta.getModulo()));
				account.setTitle(AccountsUtil.getTitle(cta.getModulo(), nombreProducto));
				account.setAlias(AccountsUtil.getAlias(cta.getSucursal()
													  ,cta.getSubOperacion()
													  ,sucursales));
				account.getCurrencies().add(currency);
				account.setAvailableBalance(AccountsUtil.getAvailableBalance(cta.getSaldo()
																			,saldoAnterior
																			,cta.getMoneda()
																			, cta.getPapel()));
				account.setStatus(AccountsUtil.getStatus(cta.getEstado(), cta.getScfunc()));
			}
			rsCuenta.close();
			psProducto.close();
			psHisto.close();
			psCuenta.close();
			manager.closeConnection();
		} catch (SQLException e1) {
			throw new ServiceException("Error en account","Error en account",e1);
		} finally {
			try {
				manager.closeConnection();
			} catch (SQLException e) {
				throw new ServiceException("Error al cerrar la conexion","Error en account",e);
			}
		}
		return account;
	}
	public AccountDataList accounts(String clientId, String idCuenta) throws ServiceException {
		init();
		AccountDataList accounts = new AccountDataList();
		try {
			PreparedStatement psCuentas = manager.prepareStatement(SQLStatementsBBVA.GET_CUENTAS_BANTOTAL_ASOCIADAS_AL_DOCUMENTO);
			PreparedStatement psSaldos = manager.prepareStatement(SQLStatementsBBVA.GET_LIST_CUENTASVISTAS_POR_CUENTA_BANTOTAL);
			PreparedStatement psHisto = manager.prepareStatement(SQLStatementsBBVA.GET_SALDO_ANTERIOR);
			PreparedStatement psProducto = manager.prepareStatement(SQLStatementsBBVA.GET_PRODUCTO);
			psCuentas.setInt(1, 845);
			psCuentas.setInt(2, 1);
			psCuentas.setString(3, clientId);
			ResultSet rsCuentas = manager.executeQuery(psCuentas);
			ArrayList<Integer> cuentas = new ArrayList<Integer>();
			while (rsCuentas.next()) {
				cuentas.add(rsCuentas.getInt("CTNRO"));
			}
			rsCuentas.close();
			for (int cta : cuentas) {
				psSaldos.setInt(1, cta);
				ResultSet rsSaldos = manager.executeQuery(psSaldos);
				int sucursal = 0;
				int cuenta = 0;
				int subCuenta = 0;
				int modulo = 0;
				int moneda = 0;
				int papel = 0;
				int operacion = 0;
				int tipoOper = 0;
				int estado = 0;
				double saldo = 0;
				double saldoAnterior = 0;
				long rubro = 0;
				int scfunc = 0;
				while (rsSaldos.next()) {
					sucursal = rsSaldos.getInt("scsuc");
					cuenta = rsSaldos.getInt("sccta");
					subCuenta = rsSaldos.getInt("scsbop");
					modulo = rsSaldos.getInt("scmod");
					moneda = rsSaldos.getInt("scmda");
					papel = rsSaldos.getInt("scpap");
					operacion = rsSaldos.getInt("scoper");
					tipoOper = rsSaldos.getInt("sctope");
					rubro = rsSaldos.getLong("scrub");
					estado = rsSaldos.getInt("scstat");
					saldo = rsSaldos.getDouble("scsdo");
					scfunc = rsSaldos.getInt("scfunc");
					psHisto.setInt(1, sucursal);
					psHisto.setLong(2, rubro);
					psHisto.setInt(3, moneda);
					psHisto.setInt(4, papel);
					psHisto.setInt(5, cuenta);
					psHisto.setInt(6, operacion);
					psHisto.setInt(7, subCuenta);
					psHisto.setInt(8, tipoOper);
					psHisto.setInt(9, Calendar.getInstance().get(Calendar.YEAR));
					ResultSet rsHisto = manager.executeQuery(psHisto);
					psProducto.setInt(1, modulo);
					ResultSet rsProducto = manager.executeQuery(psProducto);
					String nombreProducto = null;
					while (rsProducto.next()) {
						nombreProducto = rsProducto.getString("Mdnom") != null ? rsProducto.getString("Mdnom").trim()
								: "";
					}
					rsProducto.close();
					// Saldo al cierre del día anterior
					saldoAnterior = 0;
					while (rsHisto.next()) {
						saldoAnterior = rsHisto.getDouble("Hasd13");
					}
					rsHisto.close();
					// Moneda de la cuenta
					AccountCurrencies currency = new AccountCurrencies();
					currency.setCurrency(AccountsUtil.getSimboloMoneda(moneda, papel));
					currency.setIsMajor(true);
					// Fecha de Alta de la cuenta
					Date fechaAlta = null;
					try {
						fechaAlta = sdfBantotal.parse(rsSaldos.getString("scfcon"));
					} catch (ParseException e1) {
						e1.printStackTrace();
					}
					Account account = new Account();
					account.setAccountId(AccountsUtil.getId(sucursal, cuenta, moneda, papel, subCuenta, modulo));
					account.setNumber(AccountsUtil.getId(sucursal, cuenta, moneda, papel, subCuenta, modulo));
 					account.setNumberType(AccountsUtil.getNumberType());
					account.setAccountFamily(AccountsUtil.getAccountFamily(modulo));
					account.setAccountType(AccountsUtil.getAccountType(modulo));
					account.setTitle(AccountsUtil.getTitle(modulo, nombreProducto));
					account.setAlias(AccountsUtil.getAlias(sucursal, subCuenta, sucursales ));
					account.setOpeningDate(fechaAlta);
					account.getCurrencies().add(currency);
					account.setAvailableBalance(AccountsUtil.getAvailableBalance(saldo, saldoAnterior, moneda, papel));
					account.setStatus(AccountsUtil.getStatus(estado,scfunc));
					account.setRelatedContracts(AccountsUtil.getRelatedContracts(cuenta));
					if (idCuenta == null || idCuenta.length() == 0 || idCuenta.equals(account.getAccountId())){
						accounts.getData().add(account);
					}
				}
				rsSaldos.close();
			}
			psSaldos.close();
			psHisto.close();
			psProducto.close();
			psCuentas.close();
			manager.closeConnection();
		} catch (SQLException e1) {
			throw new ServiceException("Error en accounts","Error en accounts",e1);
		} finally {
			try {
				manager.closeConnection();
			} catch (SQLException e) {
				throw new ServiceException("Error al cerrar la conexion","Error en accounts",e);
			}
		}
		return accounts;
	}
	public BalanceDataList balances(String clientId, String accountFamilyId, String accountTypeId, String currenciesCurrency, Date periodStartDate, Date periodEndDate) throws ServiceException {
		BalanceDataList balances = new BalanceDataList();
		try {
			PreparedStatement psPersona = manager.prepareStatement(SQLStatementsBBVA.GET_PERSONA);
			PreparedStatement psCuentas = manager.prepareStatement(SQLStatementsBBVA.GET_CUENTAS_BANTOTAL_ASOCIADAS_AL_DOCUMENTO);
			PreparedStatement psSaldos = manager.prepareStatement(SQLStatementsBBVA.GET_LIST_CUENTASVISTAS_POR_CUENTA_BANTOTAL);
			PreparedStatement psSaldoAnterior = manager.prepareStatement(SQLStatementsBBVA.GET_SALDO_ANTERIOR);
			psPersona.setString(1, clientId);
			ResultSet rsPersona = manager.executeQuery(psPersona);
			int pais = 0;
			int tipoDoc = 0;
			int cuenta = 0;
			int modulo = 0;
			int moneda = 0;
			int sucursal = 0;
			long rubro = 0;
			int papel = 0;
			int operacion = 0;
			int subCuenta = 0;
			int tipoOper = 0;
			double saldo = 0;
			boolean existeCuentaVista = false;
			while(rsPersona.next()){
				pais = rsPersona.getInt("pepais");
				tipoDoc = rsPersona.getInt("petdoc");
				//-- Verfico si la persona tiene cuentas
				psCuentas.setInt(1, pais);
				psCuentas.setInt(2, tipoDoc);
				psCuentas.setString(3, clientId);
				ResultSet rsCuentas = manager.executeQuery(psCuentas);
				while(rsCuentas.next()){
					cuenta = rsCuentas.getInt("CTNRO");
					//-- Verifico si tiene saldos
					psSaldos.setInt(1, cuenta);
					ResultSet rsSaldos = manager.executeQuery(psSaldos);
					while(rsSaldos.next()){
						existeCuentaVista = true;
						modulo = rsSaldos.getInt("scmod");
						moneda = rsSaldos.getInt("scmda");
						saldo = rsSaldos.getDouble("scsdo");
						sucursal = rsSaldos.getInt("scsuc");
						rubro = rsSaldos.getLong("scrub");
						papel = rsSaldos.getInt("scpap");
						operacion = rsSaldos.getInt("scoper");
						subCuenta = rsSaldos.getInt("scsbop");
						tipoOper = rsSaldos.getInt("sctope");
						AccountFamily af = AccountsUtil.getAccountFamily(modulo);
						boolean existeItem = false;
						boolean existeBreakdown = false;
						String currency = AccountsUtil.getSimboloMoneda(moneda, papel);
						psSaldoAnterior.setInt(1, sucursal);
						psSaldoAnterior.setLong(2, rubro);
						psSaldoAnterior.setInt(3, moneda);
						psSaldoAnterior.setInt(4, papel);
						psSaldoAnterior.setInt(5, cuenta);
						psSaldoAnterior.setInt(6, operacion);
						psSaldoAnterior.setInt(7, subCuenta);
						psSaldoAnterior.setInt(8, tipoOper);
						psSaldoAnterior.setInt(9, Calendar.getInstance().get(Calendar.YEAR));
						ResultSet rsHisto = manager.executeQuery(psSaldoAnterior);
						// Saldo al cierre del día anterior
						double saldoAnterior = 0;
						while (rsHisto.next()) {
							saldoAnterior = rsHisto.getDouble("Hasd13");
						}
						rsHisto.close();
						boolean existeGrouping = false;
						for(Balance balance : balances.getData()){
							if("ACCOUNT_FAMILY".equals(balance.getGroupings().getId())){
								existeGrouping = true;
								for(GroupingItem gi : balance.getGroupings().getItems()){
									//Verifico que existe un item para el account family
									if(gi.getId().equals(af.getId())){
										existeItem = true;
										break;
									}
								}
								if(!existeItem){
									GroupingItem groupingItem = new GroupingItem();
									groupingItem.setId(af.getId());
									groupingItem.setName(af.getName());
									balance.getGroupings().getItems().add(groupingItem);
								}
								//-- Se busca breakdown para la moneda
								for(Breakdown breakdown : balance.getBreakdown()){
									if(breakdown.getCurrency().equals(currency)){
										existeBreakdown = true;
										breakdown.setAvailable(breakdown.getAvailable() + saldo);
										breakdown.setPosted(breakdown.getPosted() + saldoAnterior);
									}
								}
								if(!existeBreakdown){
									Breakdown bkd = new Breakdown();
									bkd.setCurrency(currency);
									bkd.setAvailable(saldo);
									bkd.setPosted(saldoAnterior);
									balance.getBreakdown().add(bkd);
								}
								break;
							}
						}
						if(!existeGrouping){
							Balance nuevoBalance = new Balance();
							Grouping nuevoGrouping = new Grouping();
							nuevoGrouping.setId("ACCOUNT_FAMILY");
							nuevoGrouping.setName("Account Family");
							GroupingItem gi = new GroupingItem();
							gi.setId(af.getId());
							gi.setName(af.getName());
							nuevoGrouping.getItems().add(gi);
							nuevoBalance.setGroupings(nuevoGrouping);
							Breakdown nuevoBkd = new Breakdown();
							nuevoBkd.setCurrency(currency);
							nuevoBkd.setAvailable(saldo);
							nuevoBkd.setPosted(saldoAnterior);
							nuevoBalance.getBreakdown().add(nuevoBkd);
							balances.getData().add(nuevoBalance);
						}
					}
					rsSaldos.close();
					if(existeCuentaVista){
						break;
					}
				}
				if(existeCuentaVista){
					break;
				}
				rsCuentas.close();
			}
			rsPersona.close();
			psPersona.close();
			psCuentas.close();
			psSaldos.close();
			psSaldoAnterior.close();
			manager.closeConnection();
		} catch (SQLException e1) {
			throw new ServiceException("Error en transactions","Error en transactions",e1);
		} finally {
			try {
				manager.closeConnection();
			} catch (SQLException e) {
				throw new ServiceException("Error al cerrar la conexion","Error en transactions",e);
			}
		}
		return balances;
	}
//	public TransactionDataList transactions(String accountId) throws ServiceException {
//		init();
//		TransactionDataList trans = new TransactionDataList();
//		SaldoBantotal saldo = AccountsUtil.getSaldo(accountId);
//
//
//		try {
//			//-- Obtengo el rubro
//			PreparedStatement psCuenta = manager.prepareStatement(SQLStatementsBBVA.GET_CUENTAVISTA);
//			psCuenta.setInt(1, saldo.getModulo());
//			psCuenta.setInt(2, saldo.getMoneda());
//			psCuenta.setInt(3, saldo.getPapel());
//			psCuenta.setInt(4, saldo.getCuenta());
//			psCuenta.setInt(5, saldo.getSubOperacion());
//			psCuenta.setInt(6, saldo.getSucursal());
//			ResultSet rsCuenta = manager.executeQuery(psCuenta);
//			while(rsCuenta.next()){
//				saldo.setRubro(rsCuenta.getLong("scrub"));
//			}
//			rsCuenta.close();
//
//			PreparedStatement psTransaccion = manager.prepareStatement(SQLStatementsBBVA.GET_NOMBRE_TRANSACCION);
//			PreparedStatement psCabezalDiario = manager.prepareStatement(SQLStatementsBBVA.GET_CABEZAL_MOVIMIENTO_DIARIO);
//			PreparedStatement psCabezalHistorico = manager.prepareStatement(SQLStatementsBBVA.GET_CABEZAL_MOVIMIENTO_HISTORICO);
//
//			//-- Movimientos diarios
//			PreparedStatement psMovDiarios = manager.prepareStatement(SQLStatementsBBVA.GET_MOVIMIENTOS_DIARIOS);
//			psMovDiarios.setInt(1, saldo.getPgcod());
//			psMovDiarios.setInt(2, saldo.getSucursal());
//			psMovDiarios.setLong(3, saldo.getRubro());
//			psMovDiarios.setInt(4, saldo.getMoneda());
//			psMovDiarios.setInt(5, saldo.getPapel());
//			psMovDiarios.setInt(6, saldo.getCuenta());
//			psMovDiarios.setInt(7, saldo.getOperacion());
//			psMovDiarios.setInt(8, saldo.getSubOperacion());
//			psMovDiarios.setInt(9, saldo.getTipoOperacion());
//
//			ResultSet rsMovDiarios = manager.executeQuery(psMovDiarios);
//			while(rsMovDiarios.next()){
//				MovimientoBantotal mb = new MovimientoBantotal();
//				mb.setPgcod(rsMovDiarios.getInt("pgcod"));
//				mb.setSucursal(rsMovDiarios.getInt("itsuc"));
//				mb.setModulo(rsMovDiarios.getInt("itmod"));
//				mb.setTrnasaccion(rsMovDiarios.getInt("ittran"));
//				mb.setRelacion(rsMovDiarios.getInt("itnrel"));
//				mb.setOrdinal(rsMovDiarios.getInt("itord"));
//				mb.setMonto(rsMovDiarios.getDouble("itimp1"));
//				mb.setMoneda(rsMovDiarios.getInt("Moneda"));
//				mb.setDebOCred(rsMovDiarios.getInt("itdbha"));
//				mb.setCuenta(rsMovDiarios.getInt("Ctnro"));
//				mb.setHistorico(false);
//
//				Transaction trn = new Transaction();
//				ModelImport mi = new ModelImport();
//				mi.setAmount(mb.getMonto());
//				mi.setCurrency(AccountsUtil.getSimboloMoneda(mb.getMoneda()));
//				trn.setLocalAmount(mi);
//				trn.setMoneyFlow(AccountsUtil.getMoneyFlow(mb.getDebOCred()));
//				psTransaccion.setInt(1, mb.getPgcod());
//				psTransaccion.setInt(2, mb.getModulo());
//				psTransaccion.setInt(3, mb.getTrnasaccion());
//				ResultSet rsNombreTransaccion = manager.executeQuery(psTransaccion);
//				while(rsNombreTransaccion.next()){
//					trn.setConcept(rsNombreTransaccion.getString("trnom").trim());
//				}
//				rsNombreTransaccion.close();
//				trn.setTransactionType(AccountsUtil.getTransactionType(mb.getModulo(), mb.getTrnasaccion()));
//				//-Datos del cabezal
//				psCabezalDiario.setInt(1, mb.getPgcod());
//				psCabezalDiario.setInt(2, mb.getSucursal());
//				psCabezalDiario.setInt(3, mb.getModulo());
//				psCabezalDiario.setInt(4, mb.getTrnasaccion());
//				psCabezalDiario.setInt(5, mb.getRelacion());
//				ResultSet rsCabDiario = manager.executeQuery(psCabezalDiario);
//				while(rsCabDiario.next()){
//					mb.setFechaContabilizado(rsCabDiario.getString("itfcon"));
//					mb.setFechaValor(rsCabDiario.getString("itfvc"));
//					mb.setEstado(rsCabDiario.getString("itcont"));
//					trn.setStatus(AccountsUtil.getStatus(mb.getEstado()));
//					try {
//						trn.setOperationDate(sdfBantotal.parse(mb.getFechaContabilizado()));
//						trn.setAccountedDate(sdfBantotal.parse(mb.getFechaContabilizado()));
//						trn.setValuationDate(sdfBantotal.parse(mb.getFechaValor()));
//					} catch (ParseException e) {
//						e.printStackTrace();
//					}
//				}
//				rsCabDiario.close();
//				trn.setTransactionId(AccountsUtil.getIdTransacction(mb));
//				trn.setFinancingType(AccountsUtil.getFinancingType(mb.getModulo(), mb.getTrnasaccion()));
//				trn.setContract(AccountsUtil.getContract(mb.getCuenta()));
//				if ("S".equals(mb.getEstado()) || "B".equals(mb.getEstado())){
//					trans.getData().add(trn);
//				}
//			}
//
//			rsMovDiarios.close();
//
//			//-- Movimientos Históricos
//			PreparedStatement psMovHistoricios = manager.prepareStatement(SQLStatementsBBVA.GET_MOVIMIENTOS_HISTORICOS);
//			psMovHistoricios.setInt(1, saldo.getPgcod());
//			psMovHistoricios.setString(2, "20170101");
//			psMovHistoricios.setString(3, sdfBantotal.format(new Date()));
//			psMovHistoricios.setInt(4, saldo.getSucursal());
//			psMovHistoricios.setLong(5, saldo.getRubro());
//			psMovHistoricios.setInt(6, saldo.getMoneda());
//			psMovHistoricios.setInt(7, saldo.getPapel());
//			psMovHistoricios.setInt(8, saldo.getCuenta());
//			psMovHistoricios.setInt(9, saldo.getOperacion());
//			psMovHistoricios.setInt(10, saldo.getSubOperacion());
//			//Pgcod, Hfcon, Hsucur, Hrubro, Hmda, Hpap, Hcta, Hoper, Hsubop
//			ResultSet rsMovHistoricos = manager.executeQuery(psMovHistoricios);
//			while(rsMovHistoricos.next()){
//				MovimientoBantotal mb = new MovimientoBantotal();
//				mb.setPgcod(rsMovHistoricos.getInt("pgcod"));
//				mb.setFechaContabilizado(rsMovHistoricos.getString("Hfcon"));
//				mb.setSucursal(rsMovHistoricos.getInt("Hsucor"));
//				mb.setModulo(rsMovHistoricos.getInt("Hcmod"));
//				mb.setTrnasaccion(rsMovHistoricos.getInt("Htran"));
//				mb.setRelacion(rsMovHistoricos.getInt("Hnrel"));
//				mb.setOrdinal(rsMovHistoricos.getInt("Hcord"));
//				mb.setMonto(rsMovHistoricos.getDouble("Hcimp1"));
//				mb.setMoneda(rsMovHistoricos.getInt("Hmda"));
//				mb.setDebOCred(rsMovHistoricos.getInt("Hcodmo"));
//				mb.setCuenta(rsMovHistoricos.getInt("Hcta"));
//				mb.setHistorico(false);
//
//				Transaction trn = new Transaction();
//				ModelImport mi = new ModelImport();
//				mi.setAmount(mb.getMonto());
//				mi.setCurrency(AccountsUtil.getSimboloMoneda(mb.getMoneda()));
//				trn.setLocalAmount(mi);
//				trn.setMoneyFlow(AccountsUtil.getMoneyFlow(mb.getDebOCred()));
//				psTransaccion.setInt(1, mb.getPgcod());
//				psTransaccion.setInt(2, mb.getModulo());
//				psTransaccion.setInt(3, mb.getTrnasaccion());
//				ResultSet rsNombreTransaccion = manager.executeQuery(psTransaccion);
//				while(rsNombreTransaccion.next()){
//					trn.setConcept(rsNombreTransaccion.getString("trnom").trim());
//				}
//				rsNombreTransaccion.close();
//				trn.setTransactionType(AccountsUtil.getTransactionType(mb.getModulo(), mb.getTrnasaccion()));
//				//-Datos del cabezal
//				psCabezalHistorico.setInt(1, mb.getPgcod());
//				psCabezalHistorico.setInt(2, mb.getModulo());
//				psCabezalHistorico.setInt(3, mb.getSucursal());
//				psCabezalHistorico.setInt(4, mb.getTrnasaccion());
//				psCabezalHistorico.setInt(5, mb.getRelacion());
//				psCabezalHistorico.setString(6, mb.getFechaContabilizado());
//				ResultSet rsCabHistorico = manager.executeQuery(psCabezalHistorico);
//				while(rsCabHistorico.next()){
//					mb.setFechaValor(rsCabHistorico.getString("HFvc"));
//					mb.setEstado("S");
//					trn.setStatus(AccountsUtil.getStatus(mb.getEstado()));
//
//					try {
//						trn.setAccountedDate(sdfBantotal.parse(mb.getFechaContabilizado()));
//						trn.setValuationDate(sdfBantotal.parse(mb.getFechaValor()));
//					} catch (ParseException e) {
//						e.printStackTrace();
//					}
//				}
//				rsCabHistorico.close();
//				trn.setTransactionId(AccountsUtil.getIdTransacction(mb));
//				trn.setFinancingType(AccountsUtil.getFinancingType(mb.getModulo(), mb.getTrnasaccion()));
//				trn.setContract(AccountsUtil.getContract(mb.getCuenta()));
//				trans.getData().add(trn);
//			}
//			rsMovHistoricos.close();
//			psCabezalHistorico.close();
//			psCabezalDiario.close();
//			psTransaccion.close();
//			psMovDiarios.close();
//			psMovHistoricios.close();
//			psCuenta.close();
//		} catch (SQLException e1) {
//			throw new ServiceException("Error en transactions","Error en transactions",e1);
//		} finally {
//			try {
//				manager.closeConnection();
//			} catch (SQLException e) {
//				throw new ServiceException("Error al cerrar la conexion","Error en transactions",e);
//			}
//		}
//
//		return trans;
//
//	}
	public AccountTypeDataList getAccountsType(){
		AccountTypeDataList atdl = new AccountTypeDataList();
		AccountType at1 = new AccountType();
		at1.setId("20");
		at1.setName("Cuenta Corriente");
		atdl.getData().add(at1);
		AccountType at2 = new AccountType();
		at2.setId("21");
		at2.setName("Caja de Ahorro");
		atdl.getData().add(at2);
		AccountType at3 = new AccountType();
		at3.setId("23");
		at3.setName("Cuenta Cajero");
		atdl.getData().add(at3);
		return atdl;
	}
}