package uy.com.bbva.services.accounts.model;

import com.fasterxml.jackson.annotation.JsonProperty;


import java.util.Objects;



public class AnonymousRepresentation7AdditionalInformation   {
  
  private String reference = null;
  private String additionalData = null;
  private ModelImport updatedBalance = null;

  /**
   * Brief information that allows customers to know details about the operative that originates the transaction.
   **/
  public AnonymousRepresentation7AdditionalInformation reference(String reference) {
    this.reference = reference;
    return this;
  }

  

  @JsonProperty("reference")
  public String getReference() {
    return reference;
  }
  public void setReference(String reference) {
    this.reference = reference;
  }

  /**
   * Additional data that sometimes complements transaction information.
   **/
  public AnonymousRepresentation7AdditionalInformation additionalData(String additionalData) {
    this.additionalData = additionalData;
    return this;
  }

  

  @JsonProperty("additionalData")
  public String getAdditionalData() {
    return additionalData;
  }
  public void setAdditionalData(String additionalData) {
    this.additionalData = additionalData;
  }

  /**
   * This balance means the maximum consumable amount after the transaction was posted.
   **/
  public AnonymousRepresentation7AdditionalInformation updatedBalance(ModelImport updatedBalance) {
    this.updatedBalance = updatedBalance;
    return this;
  }

  

  @JsonProperty("updatedBalance")
  public ModelImport getUpdatedBalance() {
    return updatedBalance;
  }
  public void setUpdatedBalance(ModelImport updatedBalance) {
    this.updatedBalance = updatedBalance;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AnonymousRepresentation7AdditionalInformation anonymousRepresentation7AdditionalInformation = (AnonymousRepresentation7AdditionalInformation) o;
    return Objects.equals(reference, anonymousRepresentation7AdditionalInformation.reference) &&
        Objects.equals(additionalData, anonymousRepresentation7AdditionalInformation.additionalData) &&
        Objects.equals(updatedBalance, anonymousRepresentation7AdditionalInformation.updatedBalance);
  }

  @Override
  public int hashCode() {
    return Objects.hash(reference, additionalData, updatedBalance);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AnonymousRepresentation7AdditionalInformation {\n");
    
    sb.append("    reference: ").append(toIndentedString(reference)).append("\n");
    sb.append("    additionalData: ").append(toIndentedString(additionalData)).append("\n");
    sb.append("    updatedBalance: ").append(toIndentedString(updatedBalance)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
