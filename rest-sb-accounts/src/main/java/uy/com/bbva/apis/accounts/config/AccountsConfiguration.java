package uy.com.bbva.apis.accounts.config;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import uy.com.bbva.restsbcommons.config.RestSBCommonsConfiguration;

@Configuration
@EnableAutoConfiguration
@Import({RestSBCommonsConfiguration.class})
public class AccountsConfiguration extends WebMvcConfigurerAdapter {

    public static final String AUTHENTICATION_HEADER = "${authentication.header}";

    @Override
    public void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
        configurer.defaultContentType(MediaType.APPLICATION_JSON);
    }
}