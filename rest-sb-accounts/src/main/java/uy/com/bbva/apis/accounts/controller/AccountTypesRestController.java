package uy.com.bbva.apis.accounts.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import uy.com.bbva.services.accounts.response.AccountTypeDataList;
import uy.com.bbva.services.accounts.service.AccountTypesApiService;
import uy.com.bbva.services.commons.exceptions.ServiceException;

@RestController
@RequestMapping("${path.accountsTypes}")
public class AccountTypesRestController {

    @Autowired
    private AccountTypesApiService accountTypesApiService;

    @GetMapping
    public AccountTypeDataList getAccountTypes()
            throws ServiceException {
        return accountTypesApiService.getAccountTypes();
    }
}