package uy.com.bbva.apis.accounts.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import uy.com.bbva.services.accounts.response.BalanceDataList;
import uy.com.bbva.services.accounts.service.BalancesApiService;
import uy.com.bbva.services.commons.exceptions.ServiceException;

import java.util.Date;
import static uy.com.bbva.apis.accounts.config.AccountsConfiguration.AUTHENTICATION_HEADER;

@RestController
@RequestMapping("${path.balances}")
public class BalancesRestController {

    @Autowired
    private BalancesApiService balancesApiService;

    @GetMapping
    public BalanceDataList getBalances(@RequestParam(name = "accountFamily.id", required = false) String accountFamilyId
            , @RequestParam(name = "accountType.id", required = false) String accountTypeId
            , @RequestParam(name = "currencies.currency", required = false) String currenciesCurrency
            , @RequestParam(name = "period.startDate", required = false) Date periodStartDate
            , @RequestParam(name = "period.endDate", required = false) Date periodEndDate
            , @RequestHeader(AUTHENTICATION_HEADER) String documento
            )
            throws ServiceException {
        return balancesApiService.getBalances(accountFamilyId, accountTypeId, currenciesCurrency, periodStartDate, periodEndDate, documento);
    }
}