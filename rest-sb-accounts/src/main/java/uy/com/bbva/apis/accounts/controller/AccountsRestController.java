package uy.com.bbva.apis.accounts.controller;

import java.sql.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uy.com.bbva.services.accounts.response.AccountData;
import uy.com.bbva.services.accounts.response.AccountDataList;
import uy.com.bbva.services.accounts.response.TransactionDataList;
import uy.com.bbva.services.accounts.service.AccountsApiService;
import uy.com.bbva.services.commons.exceptions.ServiceException;

import static uy.com.bbva.apis.accounts.config.AccountsConfiguration.AUTHENTICATION_HEADER;

@RestController
@RequestMapping("${path.accountsRest}")
public class AccountsRestController {

    @Autowired
    private AccountsApiService accountsApiService;

    @GetMapping()
    public AccountDataList getAccounts(@RequestParam(name = "accountId", required = false) String accountId
            , @RequestParam(name = "numberType.id", required = false) String numberTypeId
            , @RequestParam(name = "number", required = false) String number
            , @RequestParam(name = "accountFamily.id", required = false) String accountFamilyId
            , @RequestParam(name = "accountType.id", required = false) String accountTypeId
            , @RequestParam(name = "joint.id", required = false) String jointId
            , @RequestParam(name = "alias", required = false) String alias
            , @RequestParam(name = "status.id", required = false) String statusId
            , @RequestParam(name = "currencies.currency", required = false) String currenciesCurrency
            , @RequestParam(name = "orderBy", required = false) String orderBy
            , @RequestParam(name = "order", required = false) String order
            , @RequestParam(name = "expand", required = false) String expand
            , @RequestParam(name = "pageSize", required = false) Long pageSize
            , @RequestParam(name = "paginationKey", required = false) String paginationKey
            , @RequestParam(name = "fields", required = false) String fields
            , @RequestHeader(AUTHENTICATION_HEADER) String documento
    ) throws ServiceException {
        AccountDataList accountDataList = accountsApiService.getAccounts(accountId, numberTypeId, number, accountFamilyId, accountTypeId, jointId, alias, statusId, currenciesCurrency, orderBy, order, expand, pageSize, paginationKey, fields, documento);
        return accountDataList;
    }

    @GetMapping("/{account-id}")
    public AccountData getAccountsAccountId(@PathVariable(name = "account-id") String accountId
            , @RequestParam(name = "expand", required = false) String expand
            , @RequestParam(name = "fields", required = false) String fields)
            throws ServiceException {
        AccountData accountData =  accountsApiService.getAccountsAccountId(accountId, expand, fields);
        return accountData;
    }

    @GetMapping("/{account-id}/transactions")
    public ResponseEntity<TransactionDataList> getAccountsAccountIdTransactions(@PathVariable(name="account-id", required = false) String accountId
            , @RequestParam(name = "operationDate", required = false) Date operationDate
            , @RequestParam(name = "fromOperationDate", required = false) Date fromOperationDate
            , @RequestParam(name = "toOperationDate", required = false) Date toOperationDate
            , @RequestParam(name = "localAmount.amount", required = false) String localAmountAmount
            , @RequestParam(name = "fromLocalAmount.amount", required = false) String fromLocalAmountAmount
            , @RequestParam(name = "toLocalAmount.amount", required = false) String toLocalAmountAmount
            , @RequestParam(name = "localAmount.currency", required = false) String localAmountCurrency
            , @RequestParam(name = "transactionType.id", required = false) String transactionTypeId
            , @RequestParam(name = "transactionType.internalCode.id", required = false) String transactionTypeInternalCodeId
            , @RequestParam(name = "status.id", required = false) String statusId
            , @RequestParam(name = "pageSize", required = false) Long pageSize
            , @RequestParam(name = "paginationKey", required = false) String paginationKey
            , @RequestParam(name = "orderBy", required = false) String orderBy
            , @RequestParam(name = "order", required = false) String order
            , @RequestParam(name = "fields", required = false) String fields
            )
            throws ServiceException {
    	TransactionDataList tdl = accountsApiService.getAccountsAccountIdTransactions(accountId, operationDate, fromOperationDate, toOperationDate, localAmountAmount, fromLocalAmountAmount, toLocalAmountAmount, localAmountCurrency, transactionTypeId, transactionTypeInternalCodeId, statusId, pageSize, paginationKey, orderBy, order, fields);
        return new ResponseEntity<TransactionDataList>(tdl, HttpStatus.OK); 
    }
}